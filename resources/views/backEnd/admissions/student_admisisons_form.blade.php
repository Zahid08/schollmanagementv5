@extends('backEnd.master')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('public/backEnd/')}}/css/croppie.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/backEnd/')}}/css/custom.css">
@endsection

@section('mainContent')
    <section class="sms-breadcrumb mb-40 up_breadcrumb white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>@lang('lang.admission_form')</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                    <a href="#">@lang('lang.student_information')</a>
                    <a href="#">@lang('lang.admission_form')</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-6">
                    <div class="main-title">
                        <h3 class="mb-0">@lang('lang.add') @lang('lang.admission_form')</h3>
                    </div>
                </div>
                @if(in_array(63, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                    <div class="offset-lg-3 col-lg-3 text-right mb-20">
                        <a href="#" class="primary-btn small fix-gr-bg">
                            <span class="ti-plus pr-2"></span>
                            @lang('lang.import') @lang('lang.admission_form')
                        </a>
                    </div>
                @endif
            </div>

            @if(in_array(65, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                {{ Form::open(['class' => 'form-horizontal studentadmission', 'files' => true, 'route' => 'student_store_admission',
                'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'student_form']) }}
            @endif
            <div class="row">
                <div class="col-lg-12">
                    @if(session()->has('message-success'))
                        <div class="alert alert-success">
                            {{ session()->get('message-success') }}
                        </div>
                    @elseif(session()->has('message-danger'))
                        <div class="alert alert-danger">
                            {{ session()->get('message-danger') }}
                        </div>
                    @endif
                    <div class="white-box">
                        <div class="">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    @if ($errors->any())
                                        <div class="error text-danger ">{{ 'Something went wrong, please try again' }}</div>
                                    @endif
                                </div>
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">@lang('lang.personal') @lang('lang.info')</h4>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="url" id="url" value="{{URL::to('/')}}">
                            <div class="row mb-40 mt-30">
                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <input class="primary-input read-only-input form-control{{ $errors->has('admission_number') ? ' is-invalid' : '' }}" type="text" name="admission_number" value="{{$max_admission_id != ''? $max_admission_id + 1 : 1}}" readonly>
                                        <label>Form No</label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('admission_number'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('admission_number') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <select class="niceSelect w-100 bb form-control{{ $errors->has('session') ? ' is-invalid' : '' }}" name="session">
                                            <option data-display="@lang('lang.acadimic') @lang('lang.year') *" value="">@lang('lang.acadimic') @lang('lang.year') *</option>
                                            @foreach($sessions as $session)
                                                <option value="{{$session->id}}" {{old('session') == $session->id? 'selected': ''}}>{{$session->session}}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('session'))
                                            <span class="invalid-feedback invalid-select" role="alert">
									<strong>{{ $errors->first('session') }}</strong>
								</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <input class="primary-input form-control{{ $errors->has('full_name') ? ' is-invalid' : '' }}" type="text" name="full_name" value="{{old('full_name')}}">
                                        <label>Full Name<span>*</span> </label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('full_name'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('full_name') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-lg-3">
                                    <div class="input-effect">

                                        <select class="niceSelect w-100 bb form-control{{ $errors->has('admited_to_class') ? ' is-invalid' : '' }}" name="admited_to_class" id="classSelectStudent">
                                            <option data-display="Admitted to class *" value="">Admitted to class *</option>
                                            @foreach($classes as $class)
                                                <option value="{{$class->id}}" {{old('admited_to_class') == $class->id? 'selected': ''}}>{{$class->class_name}}</option>
                                            @endforeach
                                        </select>

                                        <span class="focus-border"></span>
                                        @if ($errors->has('admited_to_class'))
                                            <span class="invalid-feedback invalid-select" role="alert">
                                        <strong>{{ $errors->first('admited_to_class') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40">

                                @if(!empty(old('class')))
                                    @php
                                        $old_sections = DB::table('sm_class_sections')->where('class_id', '=', old('class'))
                                        ->join('sm_sections','sm_class_sections.section_id','=','sm_sections.id')
                                        ->get();
                                    @endphp
                                    <div class="col-lg-3">
                                        <div class="input-effect" id="sectionStudentDiv">
                                            <select class="niceSelect w-100 bb form-control {{ $errors->has('section') ? ' is-invalid' : '' }}" name="section"
                                                    id="sectionSelectStudent" >
                                                <option data-display="Division *" value="">Division *</option>
                                                @foreach ($old_sections as $old_section)
                                                    <option value="{{ $old_section->id }}" {{ old('section')==$old_section->id ? 'selected' : '' }} >
                                                        {{ $old_section->section_name }}</option>
                                                @endforeach

                                            </select>
                                            <span class="focus-border"></span>
                                            @if ($errors->has('section'))
                                                <span class="invalid-feedback invalid-select" role="alert">
                                        <strong>{{ $errors->first('section') }}</strong>
									</span>
                                            @endif
                                        </div>
                                    </div>
                                @else
                                    <div class="col-lg-3">
                                        <div class="input-effect" id="sectionStudentDiv">
                                            <select class="niceSelect w-100 bb form-control{{ $errors->has('section') ? ' is-invalid' : '' }}" name="section" id="sectionSelectStudent">
                                                <option data-display="Division *" value="">Division *</option>
                                            </select>
                                            <span class="focus-border"></span>
                                            @if ($errors->has('section'))
                                                <span class="invalid-feedback invalid-select" role="alert">
                                        <strong>{{ $errors->first('section') }}</strong>
									</span>
                                            @endif
                                        </div>
                                    </div>
                                @endif

                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <input class="primary-input form-control{{ $errors->has('roll_no') ? ' is-invalid' : '' }}" type="number" name="roll_no" value="{{old('roll_no')}}">
                                        <label>Roll Number <span>*</span></label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('roll_no'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('roll_no') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="no-gutters input-right-icon">
                                        <div class="col">
                                            <div class="input-effect">
                                                <input class="primary-input date" id="admissionDate" type="text"
                                                       name="admission_date" value="{{old('admission_date') != ""? old('admission_date'):date('m/d/Y')}}" autocomplete="off">
                                                <label>Admission Date <span>*</span></label>
                                                <span class="focus-border"></span>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <button class="" type="button">
                                                <i class="ti-calendar" id="admission-date-icon"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <select class="niceSelect w-100 bb form-control{{ $errors->has('fees_package_id') ? ' is-invalid' : '' }}" name="fees_package_id">
                                            <option data-display="Fees Package *" value="">Fees Package *</option>
                                            @foreach($feesList as $feesPackage)
                                                <option value="{{$feesPackage->id}}" >{{$feesPackage->feesGroups->name}} {{ '₹'. $feesPackage->amount}}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('fees_package_id'))
                                            <span class="invalid-feedback invalid-select" role="alert">
									<strong>{{ $errors->first('fees_package_id') }}</strong>
								</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">

                            <div class="col-lg-3">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input date" id="date_of_birth" type="text"
                                                   name="date_of_birth" value="{{old('date_of_birth') != ""? old('date_of_birth'):date('m/d/Y')}}" autocomplete="off">
                                            <label>DOB <span>*</span></label>
                                            <span class="focus-border"></span>
                                            @if ($errors->has('af_date_of_birth'))
                                                <span class="date_of_birth-feedback" role="alert">
                                                <strong>{{ $errors->first('date_of_birth') }}</strong>
											</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="" type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input form-control{{ $errors->has('age') ? ' is-invalid' : '' }}" type="text" name="age" value="{{old('age')}}" id="ageCalculated">
                                    <label>AGe <span>*</span></label>
                                    <span class="focus-border"></span>
                                    @if ($errors->has('age'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('age') }}</strong>
									</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input read-only-input form-control{{ $errors->has('date_of_place') ? ' is-invalid' : '' }}" type="text" name="date_of_place" value="{{ old('date_of_place') }}" >
                                    <label>Birth of Place<span>*</span></label>
                                    <span class="focus-border"></span>
                                    @if ($errors->has('date_of_place'))
                                        <span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('date_of_place') }}</strong>
								</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('gender_id') ? ' is-invalid' : '' }}" name="gender_id">
                                        <option data-display="@lang('lang.gender') *" value="">@lang('lang.gender') *</option>
                                        @foreach($genders as $gender)
                                            <option value="{{$gender->id}}" {{old('gender_id') == $gender->id? 'selected': ''}}>{{$gender->base_setup_name}}</option>
                                        @endforeach

                                    </select>
                                    <span class="focus-border"></span>
                                    @if ($errors->has('gender_id'))
                                        <span class="invalid-feedback invalid-select" role="alert">
                                        <strong>{{ $errors->first('gender_id') }}</strong>
									</span>
                                    @endif
                                </div>
                            </div>
                            {{--//End Line--}}
                        </div>

                        <div class="row mb-40">

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input read-only-input form-control{{ $errors->has('no_of_sibling') ? ' is-invalid' : '' }}" type="text" name="no_of_sibling" value="{{ old('no_of_sibling') }}" >
                                    <label>No of Siblings <span>*</span></label>
                                    <span class="focus-border"></span>
                                    @if ($errors->has('no_of_sibling'))
                                        <span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('no_of_sibling') }}</strong>
								</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('bloodgroup_id') ? ' is-invalid' : '' }}" name="bloodgroup_id">
                                        <option data-display="@lang('lang.blood_group') *" value="">@lang('lang.blood_group')<span>*</span></option>
                                        @foreach($blood_groups as $blood_group)
                                            <option value="{{$blood_group->id}}" {{old('blood_group') == $blood_group->id? 'selected': '' }}>{{$blood_group->base_setup_name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="focus-border"></span>
                                    @if ($errors->has('bloodgroup_id'))
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong>{{ $errors->first('bloodgroup_id') }}</strong>
								</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('nationality') ? ' is-invalid' : '' }}" name="nationality">
                                        <option data-display="Nationality *" value="">Nationality *</option>
                                            <option value="Indian">Indian</option>
                                            <option value="NRI">NRI</option>
                                    </select>
                                    <span class="focus-border"></span>
                                    @if ($errors->has('nationality'))
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong>{{ $errors->first('nationality') }}</strong>
								</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('religion_id') ? ' is-invalid' : '' }}" name="religion_id">
                                        <option data-display="@lang('lang.religion') *" value="">@lang('lang.religion')<span>*</span></option>
                                        @foreach($religions as $religion)
                                            <option value="{{$religion->id}}" {{old('religion_id') == $religion->id? 'selected': '' }}>{{$religion->base_setup_name}}</option>
                                        @endforeach

                                    </select>
                                    <span class="focus-border"></span>
                                    @if ($errors->has('religion_id'))
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong>{{ $errors->first('religion_id') }}</strong>
								</span>
                                    @endif
                                </div>
                            </div>

                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-2">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('caste') ? ' is-invalid' : '' }}" name="caste">
                                        <option data-display="Caste *" value="">Caste *</option>
                                        <option value="General">General</option>
                                        <option value="OBC">OBC</option>
                                        <option value="EBC">EBC</option>
                                        <option value="SC">SC</option>
                                        <option value="ST">ST</option>
                                        <option value="other">other</option>
                                    </select>
                                    <span class="focus-border"></span>
                                    @if ($errors->has('caste'))
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong>{{ $errors->first('caste') }}</strong>
								</span>
                                    @endif
                                </div>
                            </div>


                            <div class="col-lg-2">
                                <div class="input-effect">
                                    <input class="primary-input read-only-input form-control{{ $errors->has('sub_caste') ? ' is-invalid' : '' }}" type="text" name="sub_caste" value="{{ old('sub_caste') }}" >
                                    <label>Sub-Caste <span>*</span></label>
                                    <span class="focus-border"></span>
                                    @if ($errors->has('sub_caste'))
                                        <span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('sub_caste') }}</strong>
								</span>
                                    @endif
                                </div>
                            </div>
                        </div>


                        {{--Image Area--}}
                        <div class="row mb-40">
                            <div class="col-lg-6">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input" type="text" id="placeholderPhoto" placeholder="@lang('lang.student_photo')"
                                                   readonly="">
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg" for="photo">@lang('lang.browse')</label>
                                            <input type="file" class="d-none" value="{{ old('photo') }}" name="photo" id="photo">
                                        </button>
                                    </div>
                                    <div class="col-lg-3 webcam">
                                        <div class="col-auto">
                                            <button class="primary-btn-small-input open" type="button">
                                                <label class="primary-btn small fix-gr-bg" for="webcam">@lang('lang.web_cam')</label>
                                            </button>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="results">Your captured image will appear here...</div>
                                <input type="hidden" name="webcam_photo" class="image-tag" value="">
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('student_group') ? ' is-invalid' : '' }}" name="student_group">
                                        <option data-display="Students Group *" value="">Students Group <span>*</span></option>
                                        <option value="Open">Open</option>
                                        <option value="RTE">RTE</option>
                                        <option value="other">other</option>
                                    </select>
                                    <span class="focus-border"></span>
                                    @if ($errors->has('student_group'))
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong>{{ $errors->first('student_group') }}</strong>
								</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="last_school_attend" value="{{old('last_school_attend')}}">
                                    <label>Last School Attended</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="last_class_attend" value="{{old('last_class_attend')}}">
                                    <label>Last Class Attended</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="student_uid_no" value="{{old('student_uid_no')}}">
                                    <label>Student’s UID No</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="student_rfid_no" value="{{old('student_rfid_no')}}">
                                    <label>RFID No</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control{{ $errors->has('student_email') ? ' is-invalid' : '' }}" type="email" name="student_email" value="{{old('student_email')}}">
                                    <label>@lang('lang.email') @lang('lang.address') <span></span> </label>
                                    <span class="focus-border"></span>
                                    @if ($errors->has('student_email'))
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong>{{ $errors->first('student_email') }}</strong>
								</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-2">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="student_height" value="{{old('student_height')}}">
                                    <label>@lang('lang.height') (@lang('lang.in')) <span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="student_weight" value="{{old('student_weight')}}">
                                    <label>@lang('lang.Weight') (@lang('lang.kg')) <span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="student_details_handicapped" value="{{old('student_details_handicapped')}}">
                                    <label>Details, If Handicapped</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="student_medical_issue" value="{{old('student_medical_issue')}}">
                                    <label>Details, If Any Medical Issue</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                        </div>

                        {{--Parent Area Start--}}
                        <div class="parent_details" id="parent_details">
                            <div class="row mt-40">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">Parent Details</h4>
                                    </div>
                                </div>
                            </div>

                            {{--//Start father info--}}
                            <div class="row mb-40 mt-30">
                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control{{ $errors->has('fathers_name') ? ' is-invalid' : '' }}" type="text" name="fathers_name" id="fathers_name" value="{{old('fathers_name')}}">
                                        <label>Father Name <span>*</span> </label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('fathers_name'))
                                            <span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('fathers_name') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <select class="niceSelect w-100 bb form-control{{ $errors->has('fathers_occupation') ? ' is-invalid' : '' }}" name="fathers_occupation">
                                            <option data-display="Father’s Occupation *" value="">Father’s Occupation *</option>
                                            <option value="self-employed">Self Employed</option>
                                            <option value="private-service">Private Service</option>
                                            <option value="government-service">Government Service</option>
                                            <option value="other">other</option>
                                        </select>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('fathers_occupation'))
                                            <span class="invalid-feedback invalid-select" role="alert">
									<strong>{{ $errors->first('fathers_occupation') }}</strong>
								</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control{{ $errors->has('fathers_email_id') ? ' is-invalid' : '' }}" type="email" name="fathers_email_id" value="{{old('fathers_email_id')}}">
                                        <label>Father’s Email Id <span>*</span> </label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('fathers_email_id'))
                                            <span class="invalid-feedback invalid-select" role="alert">
									<strong>{{ $errors->first('fathers_email_id') }}</strong>
								</span>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="row mb-30">
                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <select class="niceSelect w-100 bb form-control{{ $errors->has('fathers_educations') ? ' is-invalid' : '' }}" name="fathers_educations">
                                            <option data-display="Educations *" value="">Educations *</option>
                                            <option value="10thssc">10th/SSC</option>
                                            <option value="12hsc"> 12th HSC</option>
                                            <option value="graducations">Graduation</option>
                                            <option value="PostGraduation">Post Graduation</option>
                                            <option value="doctorate">Doctorate</option>
                                        </select>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('fathers_educations'))
                                            <span class="invalid-feedback invalid-select" role="alert">
									    <strong>{{ $errors->first('fathers_educations') }}</strong>
								    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input" type="text" name="fathers_anual_income" id="fathers_anual_income" value="{{old('fathers_anual_income')}}">
                                        <label>Annual Income (INR)</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control{{ $errors->has('fathers_office_no') ? ' is-invalid' : '' }}"  type="number" name="fathers_office_no" id="fathers_office_no" value="{{old('fathers_office_no')}}">
                                        <label>Father’s Office No</label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('fathers_office_no'))
                                            <span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('fathers_office_no') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-30">
                                <div class="col-lg-8">
                                    <div class="input-effect">
                                        <textarea class="primary-input form-control" cols="0" rows="3" name="fathers_office_address" id="fathers_office_address">{{old('fathers_office_address')}}</textarea>
                                        <label>Father’s Office Address</label>
                                        <span class="focus-border textarea"></span>
                                        @if ($errors->has('fathers_office_address'))
                                            <span class="invalid-feedback">
                                    <strong>{{ $errors->first('fathers_office_address') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-4" style="margin-top: 30px;">
                                    <div class="input-effect">
                                        <input class="primary-input form-control{{ $errors->has('fathers_contact_no') ? ' is-invalid' : '' }}" type="number" name="fathers_contact_no" id="fathers_contact_no" value="{{old('fathers_contact_no')}}">
                                        <label>Contact No<span>*</span> </label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('fathers_contact_no'))
                                            <span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('fathers_contact_no') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{--End Father Informations--}}

                            {{--Start Mother Informations--}}
                            <div class="row mb-40 mt-30">

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control{{ $errors->has('mothers_name') ? ' is-invalid' : '' }}" type="text" name="mothers_name" id="mothers_name" value="{{old('mothers_name')}}">
                                        <label>Mother's Name <span>*</span><span></span> </label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('mothers_name'))
                                            <span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('mothers_name') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <select class="niceSelect w-100 bb form-control{{ $errors->has('mothers_occupation') ? ' is-invalid' : '' }}" name="mothers_occupation">
                                            <option data-display="Mother’s Occupation *" value="">Mother’s Occupation *</option>
                                            <option value="housewife">Housewife</option>
                                            <option value="self-employed">Self Employed</option>
                                            <option value="private-service">Private Service</option>
                                            <option value="government-service">Government Service</option>
                                            <option value="other">other</option>
                                        </select>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('mothers_occupation'))
                                            <span class="invalid-feedback invalid-select" role="alert">
									<strong>{{ $errors->first('mothers_occupation') }}</strong>
								</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control{{ $errors->has('mothers_email_id') ? ' is-invalid' : '' }}" type="email" name="mothers_email_id" value="{{old('mothers_email_id')}}">
                                        <label>Mother’s Email Id <span>*</span></label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('mothers_email_id'))
                                            <span class="invalid-feedback invalid-select" role="alert">
									<strong>{{ $errors->first('mothers_email_id') }}</strong>
								</span>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="row mb-30">

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <select class="niceSelect w-100 bb form-control{{ $errors->has('mothers_educations') ? ' is-invalid' : '' }}" name="mothers_educations">
                                            <option data-display="Educations *" value="">Educations *</option>
                                            <option value="10thssc">10th/SSC</option>
                                            <option value="12hsc"> 12th HSC</option>
                                            <option value="graducations">Graduation</option>
                                            <option value="PostGraduation">Post Graduation</option>
                                            <option value="doctorate">Doctorate</option>
                                        </select>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('mothers_educations'))
                                            <span class="invalid-feedback invalid-select" role="alert">
									    <strong>{{ $errors->first('mothers_educations') }}</strong>
								    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input" type="text" name="mothers_anual_income" id="mothers_anual_income" value="{{old('mothers_anual_income')}}">
                                        <label>Annual Income (INR)</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control{{ $errors->has('mothers_office_no') ? ' is-invalid' : '' }}" type="number" name="mothers_office_no" id="mothers_office_no" value="{{old('mothers_office_no')}}">
                                        <label>Mother’s Office No <span></span> </label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('mothers_office_no'))
                                            <span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('mothers_office_no') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-30">
                                <div class="col-lg-8">
                                    <div class="input-effect">
                                        <textarea class="primary-input form-control" cols="0" rows="3" name="mothers_office_address" id="mothers_office_address">{{old('mothers_office_address')}}</textarea>
                                        <label>Mother’s Office Address <span></span> </label>
                                        <span class="focus-border textarea"></span>
                                        @if ($errors->has('mothers_office_address'))
                                            <span class="invalid-feedback">
                                    <strong>{{ $errors->first('mothers_office_address') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-4" style="margin-top: 30px;">
                                    <div class="input-effect">
                                        <input class="primary-input form-control{{ $errors->has('mothers_contact_no') ? ' is-invalid' : '' }}" type="number" name="mothers_contact_no" id="mothers_contact_no" value="{{old('mothers_contact_no')}}">
                                        <label>Contact No<span>*</span> </label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('mothers_contact_no'))
                                            <span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('mothers_contact_no') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{--End Mother Informations--}}


                            <div class="row mb-30">
                                <div class="col-lg-12">
                                    <div class="input-effect">
                                        <textarea class="primary-input form-control {{ $errors->has('recedentials_address') ? ' is-invalid' : '' }}" cols="0" rows="3" name="recedentials_address" id="recedentials_address">{{old('recedentials_address')}}</textarea>
                                        <label>Residentials Address <span>*</span> </label>
                                        <span class="focus-border textarea"></span>
                                        @if ($errors->has('recedentials_address'))
                                            <span class="invalid-feedback">
										<strong>{{ $errors->first('recedentials_address') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-30">

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" type="text" name="city" id="city" value="{{old('city')}}">
                                        <label>City <span>*</span> </label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('city'))
                                            <span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('city') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" type="text" name="state" id="state" value="{{old('state')}}">
                                        <label>State <span>*</span> </label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('state'))
                                            <span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('state') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control{{ $errors->has('district') ? ' is-invalid' : '' }}" type="text" name="district" id="district" value="{{old('district')}}">
                                        <label>District <span>*</span> </label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('district'))
                                            <span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('district') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="row mb-30">
                                <div class="col-lg-12">
                                    <div class="input-effect">
                                        <textarea class="primary-input form-control {{ $errors->has('permanent_address') ? ' is-invalid' : '' }}" cols="0" rows="3" name="permanent_address" id="permanent_address">{{old('permanent_address')}}</textarea>
                                        <label>Permanent Address <span id="validationsColorStar">*</span> </label>
                                        <span class="focus-border textarea"></span>
                                        @if ($errors->has('permanent_address'))
                                            <span class="invalid-feedback">
										<strong>{{ $errors->first('permanent_address') }}</strong>
									</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row mb-30">
                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('trnasport') ? ' is-invalid' : '' }}" name="trnasport" id="trnasport">
                                        <option data-display="Transport *" value="">Transport *</option>
                                        <option value="yes">Yes</option>
                                        <option value="no"> No</option>
                                    </select>
                                    <span class="focus-border"></span>
                                    @if ($errors->has('trnasport'))
                                        <span class="invalid-feedback invalid-select" role="alert">
									    <strong>{{ $errors->first('trnasport') }}</strong>
								    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-4 disbaledItem" id="disbaledItem">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="route" id="route" value="{{old('route')}}">
                                    <label>Route No</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4 disbaledItem" id="disbaledItem">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="bus_no" id="bus_no" value="{{old('bus_no')}}">
                                    <label>Bus No</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                        </div>

                        <div class="row mt-40">
                            <div class="col-lg-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">Upload Documents</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40 mt-30">
                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="document_title_1" value="{{old('document_title_1')}}">
                                    <label>Birth Certificate</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="document_title_2" value="{{old('document_title_2')}}">
                                    <label>Transfer Certificate</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="document_title_3" value="{{old('document_title_3')}}">
                                    <label>Mark Sheet/Report Card</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="document_title_4" value="{{old('document_title_4')}}">
                                    <label>UID</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-30">
                            <div class="col-lg-3">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input" type="text" id="placeholderFileOneName" placeholder="01"
                                                   readonly="">
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg" for="document_file_1">@lang('lang.browse')</label>
                                            <input type="file" class="d-none" name="document_file_1" id="document_file_1">
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input" type="text" id="placeholderFileTwoName" placeholder="02"
                                                   readonly="">
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg" for="document_file_2">@lang('lang.browse')</label>
                                            <input type="file" class="d-none" name="document_file_2" id="document_file_2">
                                        </button>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input" type="text" id="placeholderFileThreeName" placeholder="03"
                                                   readonly="">
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg" for="document_file_3">@lang('lang.browse')</label>
                                            <input type="file" class="d-none" name="document_file_3" id="document_file_3">
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input" type="text" id="placeholderFileFourName" placeholder="04"
                                                   readonly="">
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg" for="document_file_4">@lang('lang.browse')</label>
                                            <input type="file" class="d-none" name="document_file_4" id="document_file_4">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @php
                            $tooltip = "";
                            if(in_array(65, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ){
                            $tooltip = "";
                            }else{
                            $tooltip = "You have no permission to add";
                            }
                        @endphp


                        <div class="row mt-40">
                            <div class="col-lg-12 text-center">
                                <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="{{$tooltip}}">
                                    <span class="ti-check"></span>
                                    @lang('lang.save') @lang('lang.admission_form')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
        </div>
    </section>
    {{-- student photo --}}
    <input type="text" id="STurl" value="{{ route('student_admission_pic')}}" hidden>
    <div class="modal" id="LogoPic">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Crop Image And Upload</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div id="resize"></div>
                    <button class="btn rotate float-lef" data-deg="90" >
                        <i class="ti-back-right"></i></button>
                    <button class="btn rotate float-right" data-deg="-90" >
                        <i class="ti-back-left"></i></button>
                    <hr>

                    <button class="primary-btn fix-gr-bg pull-right" id="upload_logo">Crop</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end student photo --}}

    {{-- father photo --}}

    <div class="modal" id="FatherPic">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Crop Image And Upload</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div id="fa_resize"></div>
                    <button class="btn rotate float-lef" data-deg="90" >
                        <i class="ti-back-right"></i></button>
                    <button class="btn rotate float-right" data-deg="-90" >
                        <i class="ti-back-left"></i></button>
                    <hr>

                    <button class="primary-btn fix-gr-bg pull-right" id="FatherPic_logo">Crop</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end father photo --}}
    {{-- mother photo --}}

    <div class="modal" id="MotherPic">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Crop Image And Upload</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div id="ma_resize"></div>
                    <button class="btn rotate float-lef" data-deg="90" >
                        <i class="ti-back-right"></i></button>
                    <button class="btn rotate float-right" data-deg="-90" >
                        <i class="ti-back-left"></i></button>
                    <hr>

                    <button class="primary-btn fix-gr-bg pull-right" id="Mother_logo">Crop</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end mother photo --}}
    {{-- mother photo --}}

    <div class="modal" id="GurdianPic">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Crop Image And Upload</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div id="Gu_resize"></div>
                    <button class="btn rotate float-lef" data-deg="90" >
                        <i class="ti-back-right"></i></button>
                    <button class="btn rotate float-right" data-deg="-90" >
                        <i class="ti-back-left"></i></button>
                    <hr>

                    <button class="primary-btn fix-gr-bg pull-right" id="Gurdian_logo">Crop</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end mother photo --}}
    <div class="pop-outer" style="display:none">
        <div class="pop-inner">
            <div id="my_camera"></div>
            <span class="buttonwebcam">
			<input class="buttonsnap" type=button value="Take Snapshot" onClick="take_snapshot()">
			<input type=button class="closebutton" value="Close">

		</span>
        </div>
    </div>
@endsection


@section('script')
    <script src="{{asset('public/backEnd/')}}/js/croppie.js"></script>
    <script src="{{asset('public/backEnd/')}}/js/st_addmision.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <script>
        $(document).ready(function (){
            $(".open").click(function (){
                Webcam.set({
                    width: 414,
                    height: 205,
                    image_format: 'jpeg',
                    jpeg_quality: 90
                });
                Webcam.attach( '#my_camera' );
                $(".pop-outer").show();
            });

            $(".closebutton").click(function (){
                Webcam.reset()
                $(".pop-outer").hide();
            });
        });

        var data_uri = '/student-store';
        function take_snapshot() {
            $(".pop-outer").hide();
            Webcam.snap( function(data_uri) {
                $(".image-tag").val(data_uri);
                document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
            } );
            Webcam.reset()
        }

        $('div#disbaledItem').css("display", "none");

        $(document).on('change', '#trnasport', function() {
            var value=$(this).val();
            if (value=='yes'){
                $('div#disbaledItem').css("display", "block");
            }else {
                $('div#disbaledItem').css("display", "none");
            }
        });

        $(document).on('keypress','#fathers_office_no, #fathers_contact_no , #mothers_office_no , #mothers_contact_no',function(e){
            if($(e.target).prop('value').length>=12){
                if(e.keyCode!=32)
                {return false}
            }});



        function getAge(dateString) {
            var now = new Date();
            var today = new Date(now.getYear(),now.getMonth(),now.getDate());

            var yearNow = now.getYear();
            var monthNow = now.getMonth();
            var dateNow = now.getDate();

            var dob = new Date(dateString.substring(6,10),
                dateString.substring(0,2)-1,
                dateString.substring(3,5)
            );

            var yearDob = dob.getYear();
            var monthDob = dob.getMonth();
            var dateDob = dob.getDate();
            var age = {};
            var ageString = "";
            var yearString = "";
            var monthString = "";
            var dayString = "";


            yearAge = yearNow - yearDob;

            if (monthNow >= monthDob)
                var monthAge = monthNow - monthDob;
            else {
                yearAge--;
                var monthAge = 12 + monthNow -monthDob;
            }

            if (dateNow >= dateDob)
                var dateAge = dateNow - dateDob;
            else {
                monthAge--;
                var dateAge = 31 + dateNow - dateDob;

                if (monthAge < 0) {
                    monthAge = 11;
                    yearAge--;
                }
            }

            age = {
                years: yearAge,
                months: monthAge,
                days: dateAge
            };

            if ( age.years > 1 ) yearString = " years";
            else yearString = " year";
            if ( age.months> 1 ) monthString = " months";
            else monthString = " month";
            if ( age.days > 1 ) dayString = " days";
            else dayString = " day";


            if ( (age.years > 0) && (age.months > 0) && (age.days > 0) )
                ageString = age.years + yearString + ", " + age.months + monthString + ", and " + age.days + dayString + " old.";
            else if ( (age.years == 0) && (age.months == 0) && (age.days > 0) )
                ageString = "Only " + age.days + dayString + " old!";
            else if ( (age.years > 0) && (age.months == 0) && (age.days == 0) )
                ageString = age.years + yearString + " old. Happy Birthday!!";
            else if ( (age.years > 0) && (age.months > 0) && (age.days == 0) )
                ageString = age.years + yearString + " and " + age.months + monthString + " old.";
            else if ( (age.years == 0) && (age.months > 0) && (age.days > 0) )
                ageString = age.months + monthString + " and " + age.days + dayString + " old.";
            else if ( (age.years > 0) && (age.months == 0) && (age.days > 0) )
                ageString = age.years + yearString + " and " + age.days + dayString + " old.";
            else if ( (age.years == 0) && (age.months > 0) && (age.days == 0) )
                ageString = age.months + monthString + " old.";
            else ageString = "Oops! Could not calculate age!";

            return ageString;
        }


        //mm/dd/year
        $("#date_of_birth").change(function(){
            var dataItem=$(this).val();
            var TodayDate = new Date();
            var endDate= new Date(Date.parse(dataItem));

            if (endDate> TodayDate) {
                alert("Should be less than today date");
                $('#ageCalculated').val(0);
                $('#ageCalculated').addClass('has-content');
                $(this).val('');
            }

            var ageCalculated =getAge($('#date_of_birth').val());

            $('#ageCalculated').val(ageCalculated);
            $('#ageCalculated').addClass('has-content');
        });

    </script>
@endsection