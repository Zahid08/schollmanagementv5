@extends('backEnd.master')
@section('mainContent')

    @php
        function showPicName($data){
            $name = explode('/', $data);
            $number = count($name);
            return $name[$number-1];
        }
        function showTimelineDocName($data){
            $name = explode('/', $data);
            $number = count($name);
            return $name[$number-1];
        }
        function showDocumentName($data){
            $name = explode('/', $data);
            $number = count($name);
            return $name[$number-1];
        }
    @endphp
    @php  $setting = App\SmGeneralSettings::find(1);  if(!empty($setting->currency_symbol)){ $currency = $setting->currency_symbol; }else{ $currency = '$'; }   @endphp

    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Student Admission Details</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                    <a href="{{route('student_admission_form_list')}}">Student Admission List</a>
                    <a href="#">Student Admission Details</a>
                </div>
            </div>
        </div>
    </section>

    <section class="student-details">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-3">
                    <!-- Start Student Meta Information -->
                    <div class="main-title">
                        <h3 class="mb-20">Student Admission Details</h3>
                    </div>
                    <div class="student-meta-box">
                        <div class="student-meta-top"></div>
                        <img class="student-meta-img img-100" src="{{ file_exists(@$student_detail->student_photo) ? asset($student_detail->student_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"                            alt="">

                        <div class="white-box radius-t-y-0">
                            <div class="single-meta mt-10">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                      Student Full Name:
                                    </div>
                                    <div class="value">
                                        {{@$student_detail->full_name}}
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                      Form No :
                                    </div>
                                    <div class="value">
                                        {{@$student_detail->admission_number}}
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                       Roll No :
                                    </div>
                                    <div class="value">
                                        {{@$student_detail->roll_no}}
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                       Admitted To Class:
                                    </div>
                                    <div class="value">
                                        @if($student_detail->className!="")
                                            {{@$student_detail->className->class_name}}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Division :
                                    </div>
                                    <div class="value">
                                        {{@$student_detail->divison}}
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        @lang('lang.gender')
                                    </div>
                                    <div class="value">
                                        {{@$student_detail->gender_id !=""?$student_detail->gender->base_setup_name:""}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Student Meta Information -->
                </div>

                <!-- Start Student Details -->
                <div class="col-lg-9 student-details up_admin_visitor">
                    <ul class="nav nav-tabs tabs_scroll_nav" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#studentProfile" role="tab" data-toggle="tab">@lang('lang.profile')</a>
                        </li>
                        {{--<li class="nav-item">--}}
                            {{--<a class="nav-link" href="#studentFees" role="tab" data-toggle="tab">@lang('lang.fees')</a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item">--}}
                            {{--<a class="nav-link" href="#studentDocuments1" role="tab" data-toggle="tab">@lang('lang.document')</a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item">--}}
                            {{--<a class="nav-link" href="#studentTimeline" role="tab" data-toggle="tab">@lang('lang.timeline')</a>--}}
                        {{--</li>--}}
                        <li class="nav-item edit-button">
                            @if(@in_array(66, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                                <a href="{{route('student_admission_form_edit', [@$student_detail->id])}}"
                                   class="primary-btn small fix-gr-bg">@lang('lang.edit')
                                </a>
                            @endif
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <!-- Start Profile Tab -->
                        <div role="tabpanel" class="tab-pane fade  show active" id="studentProfile">
                            <div class="white-box">
                                <h4 class="stu-sub-head">@lang('lang.personal') @lang('lang.info')</h4>
                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="">
                                                <h6>Admission Fees : </h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="">
                                               10
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="">
                                                <h6>Admission Date:</h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="">
                                                {{ !empty($student_detail->admission_date)? App\SmGeneralSettings::DateConvater($student_detail->admission_date):''}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6> @lang('lang.date_of_birth') :</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                {{ !empty($student_detail->date_of_birth)? App\SmGeneralSettings::DateConvater($student_detail->date_of_birth):''}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6>Age :</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                {{$student_detail->age}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <h6> Birth of Place:</h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                {{$student_detail->date_of_place}}
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <h6>  No of Siblings:</h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                {{$student_detail->no_of_sibling}}
                                            </div>
                                        </div>


                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <h6>Blood Group:</h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                {{isset($student_detail->bloodgroup_id)? @$student_detail->bloodGroup->base_setup_name: ''}}
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <h6> Nationality:</h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                {{$student_detail->nationality}}
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <h6> Religion:</h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                {{$student_detail->religion_id != ""? $student_detail->religion->base_setup_name:""}}
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <h6> Caste:</h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                {{$student_detail->caste}}
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6>Sub-Caste :</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                {{$student_detail->sub_caste}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6>Last School Attended :</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                {{$student_detail->last_school_attend}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6> Last Class Attended:</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                {{$student_detail->last_class_attend}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6> RFID No:</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                {{@$student_detail->student_rfid_no}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6>Student’s Email:</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                {{@$student_detail->student_email}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6> Student’s UID No:</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                {{@$student_detail->student_uid_no}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-6">
                                            <div class="">
                                                <h6> Height :</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-md-6">
                                            <div class="">
                                                {{@$student_detail->student_height}}
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-md-6">
                                            <div class="">
                                                <h6>Wieght :</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-md-6">
                                            <div class="">
                                                {{@$student_detail->student_weight}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6>@lang('lang.present') @lang('lang.address')</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                {{@$student_detail->current_address}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6>  Details, If Handicapped :</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                {{@$student_detail->student_details_handicapped}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6>Details, If Any Medical Issue:</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                {{@$student_detail->student_medical_issue}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Start Parent Part -->
                                <h4 class="stu-sub-head mt-40">@lang('lang.Parent_Guardian_Details')</h4>
                                <div class="d-flex">
                                    <div class="w-100">
                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6>Father Name:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->fathers_name}}
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6>Father’s Occupation:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->fathers_occupation}}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6> Father’s Email Id:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->fathers_email_id}}
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6>Education:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->fathers_educations}}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6>Annual Income:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->fathers_anual_income}}
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6> Father’s Office No:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->fathers_office_no}}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6">
                                                    <div class="">
                                                        <h6>Father’s Office Address:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-8 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->fathers_office_address}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6">
                                                    <div class="">
                                                        <h6> Contact No</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-8 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->fathers_contact_no}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        {{--//Mother Information Start--}}
                                    </div>
                                </div>

                                <div class="d-flex">
                                    <div class="w-100">
                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6>Mother's Name:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->mothers_name}}
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6>Mothers’s Occupation:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->mothers_occupation}}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6>  Mothers’s Email Id:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->mothers_email_id}}
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6>  Education:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->mothers_educations}}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6>Annual Income:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->mothers_anual_income}}
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6>Mothers’s Office No:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->mothers_office_no}}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6">
                                                    <div class="">
                                                        <h6> Mothers’s Office Address:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-8 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->mothers_office_address}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6">
                                                    <div class="">
                                                        <h6>Contact No</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-8 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->mothers_contact_no}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6">
                                                    <div class="">
                                                        <h6>  Recedentials Address</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-8 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->recedentials_address}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-2 col-md-6">
                                                    <div class="">
                                                        <h6> City</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-2 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->city}}
                                                    </div>
                                                </div>

                                                <div class="col-lg-2 col-md-6">
                                                    <div class="">
                                                        <h6>State</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-2 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->state}}
                                                    </div>
                                                </div>

                                                <div class="col-lg-2 col-md-6">
                                                    <div class="">
                                                        <h6>District</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-2 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->district}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6">
                                                    <div class="">
                                                        <h6>Permanent Address</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-8 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->permanent_address}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-2 col-md-6">
                                                    <div class="">
                                                        <h6>Transport</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-2 col-md-7">
                                                    <div class="">
                                                       <?php
                                                          if ($student_detail->city=='yes'){
                                                              echo "Yes";
                                                          }else{
                                                              echo "No";
                                                          }
                                                        ?>
                                                    </div>
                                                </div>

                                                <div class="col-lg-2 col-md-6">
                                                    <div class="">
                                                        <h6>Route No</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-2 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->route}}
                                                    </div>
                                                </div>

                                                <div class="col-lg-2 col-md-6">
                                                    <div class="">
                                                        <h6>Bus No</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-2 col-md-7">
                                                    <div class="">
                                                        {{@$student_detail->bus_no}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        {{--DOcument FIle Location Start--}}
                                        <table id="" class="table simple-table table-responsive school-table"
                                               cellspacing="0">
                                            <thead class="d-block">
                                            <tr class="d-flex">
                                                <th class="col-6">@lang('lang.document') @lang('lang.title')</th>
                                                <th class="col-6">@lang('lang.action')</th>
                                            </tr>
                                            </thead>

                                            <tbody class="d-block">
                                            @if($student_detail->document_file_1 != "")
                                                <tr class="d-flex">
                                                    <td class="col-6">{{$student_detail->document_title_1}}</td>
                                                    <td class="col-6">
                                                        @if (file_exists($student_detail->document_file_1))
                                                            <a class="primary-btn tr-bg text-uppercase bord-rad"
                                                               href="{{url('download-document/'.showDocumentName($student_detail->document_file_1))}}">
                                                                @lang('lang.download')<span class="pl ti-download"></span>
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                            @if($student_detail->document_file_2 != "")
                                                <tr class="d-flex">
                                                    <td class="col-6">{{$student_detail->document_title_2}}</td>
                                                    <td class="col-6">
                                                        @if (file_exists($student_detail->document_file_2))
                                                            <a class="primary-btn tr-bg text-uppercase bord-rad"
                                                               href="{{url('download-document/'.showDocumentName($student_detail->document_file_2))}}">
                                                                @lang('lang.download')<span class="pl ti-download"></span>
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                            @if($student_detail->document_file_3 != "")
                                                <tr class="d-flex">
                                                    <td class="col-6">{{$student_detail->document_title_3}}</td>
                                                    <td class="col-6">
                                                        @if (file_exists($student_detail->document_file_3))
                                                            <a class="primary-btn tr-bg text-uppercase bord-rad"
                                                               href="{{url('download-document/'.showDocumentName($student_detail->document_file_3))}}">
                                                                @lang('lang.download')<span class="pl ti-download"></span>
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                            @if($student_detail->document_file_4 != "")
                                                <tr class="d-flex">
                                                    <td class="col-6">{{$student_detail->document_title_4}}</td>
                                                    <td class="col-6">
                                                        @if (file_exists($student_detail->document_file_4))
                                                            <a class="primary-btn tr-bg text-uppercase bord-rad"
                                                               href="{{url('download-document/'.showDocumentName($student_detail->document_file_4))}}">
                                                                @lang('lang.download')<span class="pl ti-download"></span>
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif

                                            @foreach($documents as $document)
                                                @php
                                                @endphp
                                                <tr class="d-flex">
                                                    <td class="col-6">{{$document->title}}</td>
                                                    <td class="col-6">
                                                        @if (file_exists($document->file))
                                                            <a class="primary-btn tr-bg text-uppercase bord-rad"
                                                               href="{{url('download-document/'.showDocumentName($document->file))}}">
                                                                @lang('lang.download')<span class="pl ti-download"></span>
                                                            </a>
                                                        @endif
                                                        <a class="primary-btn icon-only fix-gr-bg" data-toggle="modal"
                                                           data-target="#deleteDocumentModal{{$document->id}}" href="#">
                                                            <span class="ti-trash"></span>
                                                        </a>

                                                    </td>
                                                </tr>
                                                <div class="modal fade admin-query" id="deleteDocumentModal{{$document->id}}">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">@lang('lang.delete')</h4>
                                                                <button type="button" class="close" data-dismiss="modal">
                                                                    &times;
                                                                </button>
                                                            </div>

                                                            <div class="modal-body">
                                                                <div class="text-center">
                                                                    <h4>@lang('lang.are_you_sure_to_delete')</h4>
                                                                </div>

                                                                <div class="mt-40 d-flex justify-content-between">
                                                                    <button type="button" class="primary-btn tr-bg"
                                                                            data-dismiss="modal">@lang('lang.cancel')
                                                                    </button>
                                                                    <a class="primary-btn fix-gr-bg"
                                                                       href="{{route('delete-student-document', [$document->id])}}">
                                                                        @lang('lang.delete')</a>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                        <!-- End Documents Tab -->
                        <!-- Add Document modal form start-->
                        <div class="modal fade admin-query" id="add_document_madal">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title"> @lang('lang.upload') @lang('lang.document')</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    <div class="modal-body">
                                        <div class="container-fluid">
                                            {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => 'upload_document',
                                                                'method' => 'POST', 'enctype' => 'multipart/form-data', 'name' => 'document_upload']) }}
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <input type="hidden" name="student_id"
                                                           value="{{$student_detail->id}}">
                                                    <div class="row mt-25">
                                                        <div class="col-lg-12">
                                                            <div class="input-effect">
                                                                <input class="primary-input form-control{" type="text"
                                                                       name="title" value="" id="title">
                                                                <label> @lang('lang.title')<span>*</span> </label>
                                                                <span class="focus-border"></span>

                                                                <span class=" text-danger" role="alert"
                                                                      id="amount_error">

                                                                </span>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 mt-30">
                                                    <div class="row no-gutters input-right-icon">
                                                        <div class="col">
                                                            <div class="input-effect">
                                                                <input class="primary-input" type="text"
                                                                       id="placeholderPhoto" placeholder="Document"
                                                                       disabled>
                                                                <span class="focus-border"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <button class="primary-btn-small-input" type="button">
                                                                <label class="primary-btn small fix-gr-bg" for="photo"> @lang('lang.browse')</label>
                                                                <input type="file" class="d-none" name="photo"
                                                                       id="photo">
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>


                                                <!-- <div class="col-lg-12 text-center mt-40">
                                                    <button class="primary-btn fix-gr-bg" id="save_button_sibling" type="button">
                                                        <span class="ti-check"></span>
                                                        save information
                                                    </button>
                                                </div> -->
                                                <div class="col-lg-12 text-center mt-40">
                                                    <div class="mt-40 d-flex justify-content-between">
                                                        <button type="button" class="primary-btn tr-bg"
                                                                data-dismiss="modal">@lang('lang.cancel')
                                                        </button>

                                                        <button class="primary-btn fix-gr-bg" type="submit">@lang('lang.save')
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            {{ Form::close() }}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- Add Document modal form end-->
                        <!-- delete document modal -->

                        <!-- delete document modal -->
                        <!-- Start Timeline Tab -->
                        <div role="tabpanel" class="tab-pane fade" id="studentTimeline">
                            <div class="white-box">
                                <div class="text-right mb-20">
                                    <button type="button" data-toggle="modal" data-target="#add_timeline_madal"
                                            class="primary-btn tr-bg text-uppercase bord-rad">
                                        @lang('lang.add')
                                        <span class="pl ti-plus"></span>
                                    </button>

                                </div>
                                @foreach($timelines as $timeline)
                                    <div class="student-activities">
                                        <div class="single-activity">
                                            <h4 class="title text-uppercase">

                                                {{$timeline->date != ""? App\SmGeneralSettings::DateConvater($timeline->date):''}}

                                            </h4>
                                            <div class="sub-activity-box d-flex">
                                                <h6 class="time text-uppercase">10.30 pm</h6>
                                                <div class="sub-activity">
                                                    <h5 class="subtitle text-uppercase"> {{$timeline->title}}</h5>
                                                    <p>
                                                        {{$timeline->description}}
                                                    </p>
                                                </div>

                                                <div class="close-activity">

                                                    <a class="primary-btn icon-only fix-gr-bg" data-toggle="modal"
                                                       data-target="#deleteTimelineModal{{$timeline->id}}" href="#">
                                                        <span class="ti-trash text-white"></span>
                                                    </a>

                                                    @if (file_exists($timeline->file))
                                                        <a href="{{url('staff-download-timeline-doc/'.showTimelineDocName($timeline->file))}}"
                                                           class="primary-btn tr-bg text-uppercase bord-rad">
                                                            @lang('lang.download')<span class="pl ti-download"></span>
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal fade admin-query" id="deleteTimelineModal{{$timeline->id}}">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">@lang('lang.delete')</h4>
                                                        <button type="button" class="close" data-dismiss="modal">
                                                            &times;
                                                        </button>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class="text-center">
                                                            <h4>@lang('lang.are_you_sure_to_delete')</h4>
                                                        </div>

                                                        <div class="mt-40 d-flex justify-content-between">
                                                            <button type="button" class="primary-btn tr-bg"
                                                                    data-dismiss="modal">@lang('lang.cancel')
                                                            </button>
                                                            <a class="primary-btn fix-gr-bg"
                                                               href="{{route('delete_timeline', [$timeline->id])}}">
                                                                @lang('lang.delete')</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                            </div>
                        </div>
                        <!-- End Timeline Tab -->
                    </div>
                </div>
                <!-- End Student Details -->
            </div>


        </div>
    </section>

    <!-- timeline form modal start-->
    <div class="modal fade admin-query" id="add_timeline_madal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">@lang('lang.add') @lang('lang.timeline')</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="container-fluid">
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => 'student_timeline_store',
                                            'method' => 'POST', 'enctype' => 'multipart/form-data', 'name' => 'document_upload']) }}
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="hidden" name="student_id" value="{{$student_detail->id}}">
                                <div class="row mt-25">
                                    <div class="col-lg-12">
                                        <div class="input-effect">
                                            <input class="primary-input form-control{" type="text" name="title" value=""
                                                   id="title" maxlength="200">
                                            <label>@lang('lang.title') <span>*</span> </label>
                                            <span class="focus-border"></span>

                                            <span class=" text-danger" role="alert" id="amount_error">

                                            </span>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 mt-30">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input date form-control" readonly id="startDate" type="text"
                                                   name="date">
                                            <label>@lang('lang.date')</label>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="" type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 mt-30">
                                <div class="input-effect">
                                    <textarea class="primary-input form-control" cols="0" rows="3" name="description"
                                              id="Description"></textarea>
                                    <label>@lang('lang.description')<span></span> </label>
                                    <span class="focus-border textarea"></span>
                                </div>
                            </div>

                            <div class="col-lg-12 mt-40">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input" type="text" id="placeholderFileFourName"
                                                   placeholder="Document"
                                                   disabled>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg"
                                                   for="document_file_4">@lang('lang.browse')</label>
                                            <input type="file" class="d-none" name="document_file_4"
                                                   id="document_file_4">
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 mt-30">

                                <input type="checkbox" id="currentAddressCheck" class="common-checkbox"
                                       name="visible_to_student" value="1">
                                <label for="currentAddressCheck">@lang('lang.visible_to_this_person')</label>
                            </div>


                            <!-- <div class="col-lg-12 text-center mt-40">
                                <button class="primary-btn fix-gr-bg" id="save_button_sibling" type="button">
                                    <span class="ti-check"></span>
                                    save information
                                </button>
                            </div> -->
                            <div class="col-lg-12 text-center mt-40">
                                <div class="mt-40 d-flex justify-content-between">
                                    <button type="button" class="primary-btn tr-bg" data-dismiss="modal">@lang('lang.cancel')</button>

                                    <button class="primary-btn fix-gr-bg" type="submit">@lang('lang.save')</button>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- timeline form modal end-->




@endsection
