<style>
    #visitors {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #visitors td, #visitors th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #visitors tr:nth-child(even){background-color: #f2f2f2;}

    #visitors tr:hover {background-color: #ddd;}

    #visitors th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
    }
</style>

<table  id="visitors">
    <tr>
        <td>Name : </td>
        <td colspan="4">{{$visitor->name}}</td>
        <td>Image</td>
    </tr>
    <tr>
        <td>Purpose of Visit:</td>
        <td colspan="4">{{$visitor->purpose}}</td>
        <td rowspan="3"><img src="{{asset($visitor->file)}}" alt="" class="logoimage" width="100px" height="100px"></td>
    </tr>
    <tr>
        <td>No of Person:</td>
        <td colspan="4">{{$visitor->no_of_person}}</td>
    </tr>
    <tr>
        <td>Phone No :</td>
        <td colspan="2">{{$visitor->phone}}</td>
        <td>Place :</td>
        <td >{{$visitor->place}}</td>
    </tr>
    <tr>
        <td>In Time :</td>
        <td >{{$visitor->in_time}}</td>
        <td>Out Time :</td>
        <td>{{$visitor->out_time}}</td>
        <td>Meeting With :</td>
        <td>{{$visitor->meeting_with}}</td>
    </tr>
</table>