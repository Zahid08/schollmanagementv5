
<?php $__env->startSection('mainContent'); ?>

    <?php
        function showPicName($data){
            $name = explode('/', $data);
            $number = count($name);
            return $name[$number-1];
        }
        function showTimelineDocName($data){
            $name = explode('/', $data);
            $number = count($name);
            return $name[$number-1];
        }
        function showDocumentName($data){
            $name = explode('/', $data);
            $number = count($name);
            return $name[$number-1];
        }
    ?>
    <?php  $setting = App\SmGeneralSettings::find(1);  if(!empty($setting->currency_symbol)){ $currency = $setting->currency_symbol; }else{ $currency = '$'; }   ?>

    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Student Admission Details</h1>
                <div class="bc-pages">
                    <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
                    <a href="<?php echo e(route('student_admission_form_list')); ?>">Student Admission List</a>
                    <a href="#">Student Admission Details</a>
                </div>
            </div>
        </div>
    </section>

    <section class="student-details">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-3">
                    <!-- Start Student Meta Information -->
                    <div class="main-title">
                        <h3 class="mb-20">Student Admission Details</h3>
                    </div>
                    <div class="student-meta-box">
                        <div class="student-meta-top"></div>
                        <img class="student-meta-img img-100" src="<?php echo e(file_exists(@$student_detail->student_photo) ? asset($student_detail->student_photo) : asset('public/uploads/staff/demo/staff.jpg')); ?>"                            alt="">

                        <div class="white-box radius-t-y-0">
                            <div class="single-meta mt-10">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Student Full Name:
                                    </div>
                                    <div class="value">
                                        <?php echo e(@$student_detail->full_name); ?>

                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Form No :
                                    </div>
                                    <div class="value">
                                        <?php echo e(@$student_detail->admission_no); ?>

                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Roll No :
                                    </div>
                                    <div class="value">
                                        <?php echo e(@$student_detail->roll_no); ?>

                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Admitted To Class:
                                    </div>
                                    <div class="value">
                                        <?php if($student_detail->className!=""): ?>
                                            <?php echo e(@$student_detail->className->class_name); ?>

                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        Division :
                                    </div>
                                    <div class="value">
                                        <?php echo e(@$student_detail->section->section_name); ?>

                                    </div>
                                </div>
                            </div>
                            <div class="single-meta">
                                <div class="d-flex justify-content-between">
                                    <div class="name">
                                        <?php echo app('translator')->get('lang.gender'); ?>
                                    </div>
                                    <div class="value">
                                        <?php echo e(@$student_detail->gender_id !=""?$student_detail->gender->base_setup_name:""); ?>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- Start Student Details -->
                <div class="col-lg-9 student-details up_admin_visitor">
                    <ul class="nav nav-tabs tabs_scroll_nav" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#studentProfile" role="tab" data-toggle="tab"><?php echo app('translator')->get('lang.profile'); ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#studentFees" role="tab" data-toggle="tab"><?php echo app('translator')->get('lang.fees'); ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#studentDocuments" role="tab" data-toggle="tab"><?php echo app('translator')->get('lang.document'); ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#studentTimeline" role="tab" data-toggle="tab"><?php echo app('translator')->get('lang.timeline'); ?></a>
                        </li>
                        <li class="nav-item edit-button">
                            <?php if(@in_array(66, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1): ?>
                                <a href="<?php echo e(route('student_admission_form_edit', [@$student_detail->id])); ?>"
                                   class="primary-btn small fix-gr-bg"><?php echo app('translator')->get('lang.edit'); ?>
                                </a>
                            <?php endif; ?>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <!-- Start Profile Tab -->
                        <div role="tabpanel" class="tab-pane fade  show active" id="studentProfile">
                            <div class="white-box">
                                <h4 class="stu-sub-head"><?php echo app('translator')->get('lang.personal'); ?> <?php echo app('translator')->get('lang.info'); ?></h4>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="">
                                                <h6>Admission Fees : </h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="">
                                                10
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="">
                                                <h6>Admission Date:</h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="">
                                                <?php echo e(!empty($student_detail->admission_date)? App\SmGeneralSettings::DateConvater($student_detail->admission_date):''); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6> <?php echo app('translator')->get('lang.date_of_birth'); ?> :</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                <?php echo e(!empty($student_detail->date_of_birth)? App\SmGeneralSettings::DateConvater($student_detail->date_of_birth):''); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6>Age :</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                <?php echo e($student_detail->age); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <h6> Birth of Place:</h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <?php echo e($student_detail->date_of_place); ?>

                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <h6>  No of Siblings:</h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <?php echo e($student_detail->no_of_sibling); ?>

                                            </div>
                                        </div>


                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <h6>Blood Group:</h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <?php echo e(isset($student_detail->bloodgroup_id)? @$student_detail->bloodGroup->base_setup_name: ''); ?>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <h6> Nationality:</h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <?php echo e($student_detail->nationality); ?>

                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <h6> Religion:</h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <?php echo e($student_detail->religion_id != ""? $student_detail->religion->base_setup_name:""); ?>

                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <h6> Caste:</h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="">
                                                <?php echo e($student_detail->caste); ?>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6>Sub-Caste :</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                <?php echo e($student_detail->sub_caste); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6>Last School Attended :</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                <?php echo e($student_detail->last_school_attend); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6> Last Class Attended:</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                <?php echo e($student_detail->last_class_attend); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6> RFID No:</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                <?php echo e(@$student_detail->student_rfid_no); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6>Student’s Email:</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                <?php echo e(@$student_detail->email); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6> Student’s UID No:</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                <?php echo e(@$student_detail->student_uid_no); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-6">
                                            <div class="">
                                                <h6> Height :</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-md-6">
                                            <div class="">
                                                <?php echo e(@$student_detail->height); ?>

                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-md-6">
                                            <div class="">
                                                <h6>Wieght :</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-md-6">
                                            <div class="">
                                                <?php echo e(@$student_detail->weight); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6><?php echo app('translator')->get('lang.present'); ?> <?php echo app('translator')->get('lang.address'); ?></h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                <?php echo e(@$student_detail->current_address); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6>  Details, If Handicapped :</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                <?php echo e(@$student_detail->student_details_handicapped); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-info">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="">
                                                <h6>Details, If Any Medical Issue:</h6>
                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7">
                                            <div class="">
                                                <?php echo e(@$student_detail->student_medical_issue); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Start Parent Part -->
                                <h4 class="stu-sub-head mt-40"><?php echo app('translator')->get('lang.Parent_Guardian_Details'); ?></h4>

                                <div class="d-flex">
                                    <div class="w-100">
                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6>Father Name:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        <?php echo e(@$student_detail->fathers_name); ?>

                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6>Father’s Occupation:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        <?php echo e(@$student_detail->fathers_occupation); ?>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6> Father’s Email Id:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        <?php echo e(@$student_detail->fathers_email_id); ?>

                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6>Education:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        <?php echo e(@$student_detail->fathers_educations); ?>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6>Annual Income:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        <?php echo e(@$student_detail->fathers_anual_income); ?>

                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-6">
                                                    <div class="">
                                                        <h6> Father’s Office No:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-md-7">
                                                    <div class="">
                                                        <?php echo e(@$student_detail->fathers_office_no); ?>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6">
                                                    <div class="">
                                                        <h6>Father’s Office Address:</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-8 col-md-7">
                                                    <div class="">
                                                        <?php echo e(@$student_detail->fathers_office_address); ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="single-info">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6">
                                                    <div class="">
                                                        <h6> Contact No</h6>
                                                    </div>
                                                </div>

                                                <div class="col-lg-8 col-md-7">
                                                    <div class="">
                                                        <?php echo e(@$student_detail->fathers_contact_no); ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Profile Tab -->

                        <!-- Start Fees Tab -->
                        <div role="tabpanel" class="tab-pane fade" id="studentFees">
                            <table class="display school-table school-table-style res_scrol" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th><?php echo app('translator')->get('lang.fees_group'); ?></th>
                                    <th><?php echo app('translator')->get('lang.fees_code'); ?></th>
                                    <th><?php echo app('translator')->get('lang.due_date'); ?></th>
                                    <th><?php echo app('translator')->get('lang.Status'); ?></th>
                                    <th><?php echo app('translator')->get('lang.amount'); ?> (<?php echo e(@$currency); ?>)</th>
                                    <th><?php echo app('translator')->get('lang.payment_ID'); ?></th>
                                    <th><?php echo app('translator')->get('lang.mode'); ?></th>
                                    <th><?php echo app('translator')->get('lang.date'); ?></th>
                                    <th><?php echo app('translator')->get('lang.discount'); ?> (<?php echo e(@$currency); ?>)</th>
                                    <th><?php echo app('translator')->get('lang.fine'); ?> (<?php echo e(@$currency); ?>)</th>
                                    <th><?php echo app('translator')->get('lang.paid'); ?> (<?php echo e(@$currency); ?>)</th>
                                    <th><?php echo app('translator')->get('lang.balance'); ?> (<?php echo e(@$currency); ?>)</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php
                                    $grand_total = 0;
                                    $total_fine = 0;
                                    $total_discount = 0;
                                    $total_paid = 0;
                                    $total_grand_paid = 0;
                                    $total_balance = 0;
                                ?>
                                <?php $__currentLoopData = $fees_assigneds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fees_assigned): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php
                                        $grand_total += $fees_assigned->feesGroupMaster->amount;


                                    ?>

                                    <?php
                                        $discount_amount = App\SmFeesAssign::discountSum($fees_assigned->student_id, $fees_assigned->feesGroupMaster->feesTypes->id, 'discount_amount');
                                        $total_discount += $discount_amount;
                                        $student_id = $fees_assigned->student_id;
                                    ?>
                                    <?php
                                        $paid = App\SmFeesAssign::discountSum($fees_assigned->student_id, $fees_assigned->feesGroupMaster->feesTypes->id, 'amount');
                                        $total_grand_paid += $paid;
                                    ?>
                                    <?php
                                        $fine = App\SmFeesAssign::discountSum($fees_assigned->student_id, $fees_assigned->feesGroupMaster->feesTypes->id, 'fine');
                                        $total_fine += $fine;
                                    ?>

                                    <?php
                                        $total_paid = $discount_amount + $paid;
                                    ?>
                                    <tr>
                                        <td><?php echo e($fees_assigned->feesGroupMaster->feesGroups !=""?$fees_assigned->feesGroupMaster->feesGroups->name:""); ?></td>
                                        <td><?php echo e($fees_assigned->feesGroupMaster->feesTypes !=""?$fees_assigned->feesGroupMaster->feesTypes->name:""); ?></td>
                                        <td>
                                            <?php if(!empty($fees_assigned->feesGroupMaster)): ?>
                                                <?php echo e($fees_assigned->feesGroupMaster->date != ""? App\SmGeneralSettings::DateConvater($fees_assigned->feesGroupMaster->date):''); ?>

                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if($fees_assigned->feesGroupMaster->amount == $total_paid): ?>
                                                <button class="primary-btn small bg-success text-white border-0">
                                                    <?php echo app('translator')->get('lang.paid'); ?>
                                                </button>
                                            <?php elseif($total_paid != 0): ?>
                                                <button class="primary-btn small bg-warning text-white border-0">
                                                    <?php echo app('translator')->get('lang.partial'); ?>
                                                </button>
                                            <?php elseif($total_paid == 0): ?>
                                                <button class="primary-btn small bg-danger text-white border-0">
                                                    <?php echo app('translator')->get('lang.unpaid'); ?>
                                                </button>
                                            <?php endif; ?>

                                        </td>
                                        <td>
                                            <?php
                                                echo $fees_assigned->feesGroupMaster->amount;


                                            ?>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td> <?php echo e($discount_amount); ?> </td>
                                        <td><?php echo e($fine); ?></td>
                                        <td><?php echo e($paid); ?></td>
                                        <td>
                                            <?php

                                                $rest_amount = $fees_assigned->feesGroupMaster->amount - $total_paid;


                                              $total_balance +=  $rest_amount;
                                              echo $rest_amount;
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                        $payments = App\SmFeesAssign::feesPayment($fees_assigned->feesGroupMaster->feesTypes->id, $fees_assigned->student_id);
                                        $i = 0;
                                    ?>

                                    <?php $__currentLoopData = $payments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="text-right"><img
                                                        src="<?php echo e(asset('public/backEnd/img/table-arrow.png')); ?>"></td>
                                            <td>
                                                <?php
                                                    $created_by = App\User::find($payment->created_by);
                                                ?>
                                                
                                            </td>
                                            <td>
                                                <?php if($payment->payment_mode == "C"): ?>
                                                    <?php echo e('Cash'); ?>

                                                <?php elseif($payment->payment_mode == "Cq"): ?>
                                                    <?php echo e('Cheque'); ?>

                                                <?php elseif('DD'): ?>
                                                    <?php echo e('DD'); ?>

                                                <?php elseif('PS'): ?>
                                                    <?php echo e('Paystack'); ?>

                                                <?php endif; ?>
                                            </td>
                                            <td> <?php echo e($payment->payment_date != ""? App\SmGeneralSettings::DateConvater($payment->payment_date):''); ?>


                                            </td>
                                            <td><?php echo e(@$payment->discount_amount); ?></td>
                                            <td><?php echo e(@$payment->fine); ?></td>
                                            <td><?php echo e(@$payment->amount); ?></td>
                                            <td></td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                </tbody>
                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th><?php echo app('translator')->get('lang.grand_total'); ?> (<?php echo e(@$currency); ?>)</th>
                                    <th></th>
                                    <th><?php echo e(@$grand_total); ?></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><?php echo e(@$total_discount); ?></th>
                                    <th><?php echo e(@$total_fine); ?></th>
                                    <th><?php echo e(@$total_grand_paid); ?></th>
                                    <th><?php echo e(@$total_balance); ?></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- End Profile Tab -->

                        <!-- Start Documents Tab -->
                        <div role="tabpanel" class="tab-pane fade" id="studentDocuments">
                            <div class="white-box">
                                <div class="text-right mb-20">
                                    <button type="button" data-toggle="modal" data-target="#add_document_madal"
                                            class="primary-btn tr-bg text-uppercase bord-rad">
                                        <?php echo app('translator')->get('lang.upload'); ?> <?php echo app('translator')->get('lang.document'); ?>
                                        <span class="pl ti-upload"></span>
                                    </button>
                                </div>
                                <table id="" class="table simple-table table-responsive school-table"
                                       cellspacing="0">
                                    <thead class="d-block">
                                    <tr class="d-flex">
                                        <th class="col-6"><?php echo app('translator')->get('lang.document'); ?> <?php echo app('translator')->get('lang.title'); ?></th>
                                        <th class="col-6"><?php echo app('translator')->get('lang.action'); ?></th>
                                    </tr>
                                    </thead>

                                    <tbody class="d-block">
                                    <?php if($student_detail->document_file_1 != ""): ?>
                                        <tr class="d-flex">
                                            <td class="col-6"><?php echo e($student_detail->document_title_1); ?></td>
                                            <td class="col-6">
                                                <?php if(file_exists($student_detail->document_file_1)): ?>
                                                    <a class="primary-btn tr-bg text-uppercase bord-rad"
                                                       href="<?php echo e(url('download-document/'.showDocumentName($student_detail->document_file_1))); ?>">
                                                        <?php echo app('translator')->get('lang.download'); ?><span class="pl ti-download"></span>
                                                    </a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                    <?php if($student_detail->document_file_2 != ""): ?>
                                        <tr class="d-flex">
                                            <td class="col-6"><?php echo e($student_detail->document_title_2); ?></td>
                                            <td class="col-6">
                                                <?php if(file_exists($student_detail->document_file_2)): ?>
                                                    <a class="primary-btn tr-bg text-uppercase bord-rad"
                                                       href="<?php echo e(url('download-document/'.showDocumentName($student_detail->document_file_2))); ?>">
                                                        <?php echo app('translator')->get('lang.download'); ?><span class="pl ti-download"></span>
                                                    </a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                    <?php if($student_detail->document_file_3 != ""): ?>
                                        <tr class="d-flex">
                                            <td class="col-6"><?php echo e($student_detail->document_title_3); ?></td>
                                            <td class="col-6">
                                                <?php if(file_exists($student_detail->document_file_3)): ?>
                                                    <a class="primary-btn tr-bg text-uppercase bord-rad"
                                                       href="<?php echo e(url('download-document/'.showDocumentName($student_detail->document_file_3))); ?>">
                                                        <?php echo app('translator')->get('lang.download'); ?><span class="pl ti-download"></span>
                                                    </a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                    <?php if($student_detail->document_file_4 != ""): ?>
                                        <tr class="d-flex">
                                            <td class="col-6"><?php echo e($student_detail->document_title_4); ?></td>
                                            <td class="col-6">
                                                <?php if(file_exists($student_detail->document_file_4)): ?>
                                                    <a class="primary-btn tr-bg text-uppercase bord-rad"
                                                       href="<?php echo e(url('download-document/'.showDocumentName($student_detail->document_file_4))); ?>">
                                                        <?php echo app('translator')->get('lang.download'); ?><span class="pl ti-download"></span>
                                                    </a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endif; ?>

                                    <?php $__currentLoopData = $documents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $document): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <?php
                                            //    $name = explode('/', $document->file);
                                            //    dd($name);

                                                // if(!function_exists('showDocumentName')){
                                                //     function showDocumentName($data){
                                                //     $name = explode('/', $data);
                                                //     return $name[4];
                                                // }
                                                // }

                                        ?>
                                        <tr class="d-flex">
                                            <td class="col-6"><?php echo e($document->title); ?></td>
                                            <td class="col-6">
                                                <?php if(file_exists($document->file)): ?>
                                                    <a class="primary-btn tr-bg text-uppercase bord-rad"
                                                       href="<?php echo e(url('download-document/'.showDocumentName($document->file))); ?>">
                                                        <?php echo app('translator')->get('lang.download'); ?><span class="pl ti-download"></span>
                                                    </a>
                                                <?php endif; ?>
                                                <a class="primary-btn icon-only fix-gr-bg" data-toggle="modal"
                                                   data-target="#deleteDocumentModal<?php echo e($document->id); ?>" href="#">
                                                    <span class="ti-trash"></span>
                                                </a>

                                            </td>
                                        </tr>
                                        <div class="modal fade admin-query" id="deleteDocumentModal<?php echo e($document->id); ?>">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title"><?php echo app('translator')->get('lang.delete'); ?></h4>
                                                        <button type="button" class="close" data-dismiss="modal">
                                                            &times;
                                                        </button>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class="text-center">
                                                            <h4><?php echo app('translator')->get('lang.are_you_sure_to_delete'); ?></h4>
                                                        </div>

                                                        <div class="mt-40 d-flex justify-content-between">
                                                            <button type="button" class="primary-btn tr-bg"
                                                                    data-dismiss="modal"><?php echo app('translator')->get('lang.cancel'); ?>
                                                            </button>
                                                            <a class="primary-btn fix-gr-bg"
                                                               href="<?php echo e(route('delete-student-document', [$document->id])); ?>">
                                                                <?php echo app('translator')->get('lang.delete'); ?></a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- End Documents Tab -->
                        <!-- Add Document modal form start-->
                        <div class="modal fade admin-query" id="add_document_madal">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title"> <?php echo app('translator')->get('lang.upload'); ?> <?php echo app('translator')->get('lang.document'); ?></h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    <div class="modal-body">
                                        <div class="container-fluid">
                                            <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => 'upload_document',
                                                                'method' => 'POST', 'enctype' => 'multipart/form-data', 'name' => 'document_upload'])); ?>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <input type="hidden" name="student_id"
                                                           value="<?php echo e($student_detail->id); ?>">
                                                    <div class="row mt-25">
                                                        <div class="col-lg-12">
                                                            <div class="input-effect">
                                                                <input class="primary-input form-control{" type="text"
                                                                       name="title" value="" id="title">
                                                                <label> <?php echo app('translator')->get('lang.title'); ?><span>*</span> </label>
                                                                <span class="focus-border"></span>

                                                                <span class=" text-danger" role="alert"
                                                                      id="amount_error">

                                                                </span>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 mt-30">
                                                    <div class="row no-gutters input-right-icon">
                                                        <div class="col">
                                                            <div class="input-effect">
                                                                <input class="primary-input" type="text"
                                                                       id="placeholderPhoto" placeholder="Document"
                                                                       disabled>
                                                                <span class="focus-border"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <button class="primary-btn-small-input" type="button">
                                                                <label class="primary-btn small fix-gr-bg" for="photo"> <?php echo app('translator')->get('lang.browse'); ?></label>
                                                                <input type="file" class="d-none" name="photo"
                                                                       id="photo">
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>


                                                <!-- <div class="col-lg-12 text-center mt-40">
                                                    <button class="primary-btn fix-gr-bg" id="save_button_sibling" type="button">
                                                        <span class="ti-check"></span>
                                                        save information
                                                    </button>
                                                </div> -->
                                                <div class="col-lg-12 text-center mt-40">
                                                    <div class="mt-40 d-flex justify-content-between">
                                                        <button type="button" class="primary-btn tr-bg"
                                                                data-dismiss="modal"><?php echo app('translator')->get('lang.cancel'); ?>
                                                        </button>

                                                        <button class="primary-btn fix-gr-bg" type="submit"><?php echo app('translator')->get('lang.save'); ?>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php echo e(Form::close()); ?>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- Add Document modal form end-->
                        <!-- delete document modal -->

                        <!-- delete document modal -->
                        <!-- Start Timeline Tab -->
                        <div role="tabpanel" class="tab-pane fade" id="studentTimeline">
                            <div class="white-box">
                                <div class="text-right mb-20">
                                    <button type="button" data-toggle="modal" data-target="#add_timeline_madal"
                                            class="primary-btn tr-bg text-uppercase bord-rad">
                                        <?php echo app('translator')->get('lang.add'); ?>
                                        <span class="pl ti-plus"></span>
                                    </button>

                                </div>
                                <?php $__currentLoopData = $timelines; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $timeline): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="student-activities">
                                        <div class="single-activity">
                                            <h4 class="title text-uppercase">

                                                <?php echo e($timeline->date != ""? App\SmGeneralSettings::DateConvater($timeline->date):''); ?>


                                            </h4>
                                            <div class="sub-activity-box d-flex">
                                                <h6 class="time text-uppercase">10.30 pm</h6>
                                                <div class="sub-activity">
                                                    <h5 class="subtitle text-uppercase"> <?php echo e($timeline->title); ?></h5>
                                                    <p>
                                                        <?php echo e($timeline->description); ?>

                                                    </p>
                                                </div>

                                                <div class="close-activity">

                                                    <a class="primary-btn icon-only fix-gr-bg" data-toggle="modal"
                                                       data-target="#deleteTimelineModal<?php echo e($timeline->id); ?>" href="#">
                                                        <span class="ti-trash text-white"></span>
                                                    </a>

                                                    <?php if(file_exists($timeline->file)): ?>
                                                        <a href="<?php echo e(url('staff-download-timeline-doc/'.showTimelineDocName($timeline->file))); ?>"
                                                           class="primary-btn tr-bg text-uppercase bord-rad">
                                                            <?php echo app('translator')->get('lang.download'); ?><span class="pl ti-download"></span>
                                                        </a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal fade admin-query" id="deleteTimelineModal<?php echo e($timeline->id); ?>">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title"><?php echo app('translator')->get('lang.delete'); ?></h4>
                                                        <button type="button" class="close" data-dismiss="modal">
                                                            &times;
                                                        </button>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class="text-center">
                                                            <h4><?php echo app('translator')->get('lang.are_you_sure_to_delete'); ?></h4>
                                                        </div>

                                                        <div class="mt-40 d-flex justify-content-between">
                                                            <button type="button" class="primary-btn tr-bg"
                                                                    data-dismiss="modal"><?php echo app('translator')->get('lang.cancel'); ?>
                                                            </button>
                                                            <a class="primary-btn fix-gr-bg"
                                                               href="<?php echo e(route('delete_timeline', [$timeline->id])); ?>">
                                                                <?php echo app('translator')->get('lang.delete'); ?></a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                            </div>
                        </div>
                        <!-- End Timeline Tab -->
                    </div>
                </div>
                <!-- End Student Details -->
            </div>


        </div>
    </section>

    <!-- timeline form modal start-->
    <div class="modal fade admin-query" id="add_timeline_madal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo app('translator')->get('lang.add'); ?> <?php echo app('translator')->get('lang.timeline'); ?></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="container-fluid">
                        <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => 'student_timeline_store',
                                            'method' => 'POST', 'enctype' => 'multipart/form-data', 'name' => 'document_upload'])); ?>

                        <div class="row">
                            <div class="col-lg-12">
                                <input type="hidden" name="student_id" value="<?php echo e($student_detail->id); ?>">
                                <div class="row mt-25">
                                    <div class="col-lg-12">
                                        <div class="input-effect">
                                            <input class="primary-input form-control{" type="text" name="title" value=""
                                                   id="title" maxlength="200">
                                            <label><?php echo app('translator')->get('lang.title'); ?> <span>*</span> </label>
                                            <span class="focus-border"></span>

                                            <span class=" text-danger" role="alert" id="amount_error">

                                            </span>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 mt-30">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input date form-control" readonly id="startDate" type="text"
                                                   name="date">
                                            <label><?php echo app('translator')->get('lang.date'); ?></label>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="" type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 mt-30">
                                <div class="input-effect">
                                    <textarea class="primary-input form-control" cols="0" rows="3" name="description"
                                              id="Description"></textarea>
                                    <label><?php echo app('translator')->get('lang.description'); ?><span></span> </label>
                                    <span class="focus-border textarea"></span>
                                </div>
                            </div>

                            <div class="col-lg-12 mt-40">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input" type="text" id="placeholderFileFourName"
                                                   placeholder="Document"
                                                   disabled>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg"
                                                   for="document_file_4"><?php echo app('translator')->get('lang.browse'); ?></label>
                                            <input type="file" class="d-none" name="document_file_4"
                                                   id="document_file_4">
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 mt-30">

                                <input type="checkbox" id="currentAddressCheck" class="common-checkbox"
                                       name="visible_to_student" value="1">
                                <label for="currentAddressCheck"><?php echo app('translator')->get('lang.visible_to_this_person'); ?></label>
                            </div>


                            <!-- <div class="col-lg-12 text-center mt-40">
                                <button class="primary-btn fix-gr-bg" id="save_button_sibling" type="button">
                                    <span class="ti-check"></span>
                                    save information
                                </button>
                            </div> -->
                            <div class="col-lg-12 text-center mt-40">
                                <div class="mt-40 d-flex justify-content-between">
                                    <button type="button" class="primary-btn tr-bg" data-dismiss="modal"><?php echo app('translator')->get('lang.cancel'); ?></button>

                                    <button class="primary-btn fix-gr-bg" type="submit"><?php echo app('translator')->get('lang.save'); ?></button>
                                </div>
                            </div>
                        </div>
                        <?php echo e(Form::close()); ?>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- timeline form modal end-->




<?php $__env->stopSection(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\schollmanagementv5\resources\views/backEnd/Admissions/student_admission_form_view.blade.php ENDPATH**/ ?>