
<?php $__env->startSection('mainContent'); ?>
<?php
    function showPicName($data){
        $name = explode('/', $data);
        return $name[3];
    }
?>
<section class="sms-breadcrumb mb-40 white-box up_breadcrumb">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1><?php echo app('translator')->get('lang.sms'); ?> <?php echo app('translator')->get('lang.template'); ?></h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
                <a href="#"><?php echo app('translator')->get('lang.communicate'); ?></a>
                <a href="#"><?php echo app('translator')->get('lang.sms'); ?> <?php echo app('translator')->get('lang.template'); ?></a>
            </div>
        </div>
    </div>
</section>

<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
        <?php if(isset($certificate)): ?>
        <div class="row">
            <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                <a href="<?php echo e(url('student-certificate')); ?>" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                    <?php echo app('translator')->get('lang.add'); ?>
                </a>
            </div>
        </div>
        <?php endif; ?>
        <div class="row">
           
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">
                                    <?php echo app('translator')->get('lang.update'); ?>
                                <?php echo app('translator')->get('lang.sms'); ?> <?php echo app('translator')->get('lang.template'); ?>
                            </h3>
                        </div>
                          
                            <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'sms-template-new', 'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?>

                        
                        <div class="white-box">
                            <div class="add-visitor">
                                <div class="row mt-25">
                                    <div class="col-lg-12">
                                        <?php if(session()->has('message-success')): ?>
                                        <div class="alert alert-success">
                                            <?php echo e(session()->get('message-success')); ?>

                                        </div>
                                        <?php elseif(session()->has('message-danger')): ?>
                                        <div class="alert alert-danger">
                                            <?php echo e(session()->get('message-danger')); ?>

                                        </div>
                                        <?php endif; ?>
                                        
                                    </div>
                                </div>

                                <div class="row mt-25">
                                    <div class="col-lg-12 mb-20">

                                        <span class="text-primary">[student_name] [student_email] [student_password] [guardian_name] [guardian_email] [guardian_password] [admission_number] [school_name]</span>

                                    </div>
                                    <div class="col-lg-8">

                                        <div class="input-effect">
                                            <textarea class="primary-input form-control<?php echo e($errors->has('student_approve_message_sms') ? ' is-invalid' : ''); ?>" cols="0" rows="2" name="student_approve_message_sms" maxlength="500"><?php echo e(isset($template)? $template->student_approve_message_sms: old('student_approve_message_sms')); ?></textarea>
                                            <label><?php echo app('translator')->get('lang.student_approve_message'); ?> (<?php echo app('translator')->get('lang.sms'); ?>) <span></span></label>
                                            <span class="focus-border textarea"></span>

                                            <?php if($errors->has('student_approve_message_sms')): ?>
                                                <span class="error text-danger"><strong><?php echo e($errors->first('student_approve_message_sms')); ?></strong></span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="row">
                                                <div class="col-lg-5 d-flex">
                                                    <p class="text-uppercase fw-500 mb-10"><?php echo app('translator')->get('lang.status'); ?></p>
                                                </div>
                                                <div class="col-lg-7">
                                                    
                                                        <div class=" radio-btn-flex ml-20">
                                                            <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="">
                                                                    <input type="radio" name="student_approve_message_sms_status" id="student_approve_message_sms_statusF" value="1" class="common-radio relationButton"  <?php echo e(@$template->student_approve_message_sms_status == 1? 'checked': ''); ?>>
                                                                    <label for="student_approve_message_sms_statusF"><?php echo app('translator')->get('lang.enable'); ?></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="">
                                                                    <input type="radio" name="student_approve_message_sms_status" id="student_approve_message_sms_statusM" value="2" class="common-radio relationButton"  <?php echo e(@$template->student_approve_message_sms_status == 2? 'checked': ''); ?>>
                                                                    <label for="student_approve_message_sms_statusM"><?php echo app('translator')->get('lang.disable'); ?></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>

                                <div class="row mt-25">
                                    <div class="col-lg-12 mb-20">

                                        <span class="text-primary">[student_name] [student_email] [student_password] [guardian_name] [guardian_email] [guardian_password] [admission_number] [school_name]</span>

                                    </div>
                                    <div class="col-lg-8">

                                        <div class="input-effect">
                                            <textarea class="primary-input form-control<?php echo e($errors->has('student_registration_message_sms') ? ' is-invalid' : ''); ?>" cols="0" rows="2" name="student_registration_message_sms" maxlength="500"><?php echo e(isset($template)? $template->student_registration_message_sms: old('student_registration_message_sms')); ?></textarea>
                                            <label><?php echo app('translator')->get('lang.student_registration_message'); ?> (<?php echo app('translator')->get('lang.sms'); ?>) <span></span></label>
                                            <span class="focus-border textarea"></span>

                                            <?php if($errors->has('student_registration_message_sms')): ?>
                                                <span class="error text-danger"><strong><?php echo e($errors->first('student_registration_message_sms')); ?></strong></span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="row">
                                                <div class="col-lg-5 d-flex">
                                                    <p class="text-uppercase fw-500 mb-10"><?php echo app('translator')->get('lang.status'); ?></p>
                                                </div>
                                                <div class="col-lg-7">
                                                    
                                                        <div class=" radio-btn-flex ml-20">
                                                            <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="">
                                                                    <input type="radio" name="student_registration_message_sms_status" id="student_registration_message_sms_statusF" value="1" class="common-radio relationButton"  <?php echo e(@$template->student_registration_message_sms_status == 1? 'checked': ''); ?>>
                                                                    <label for="student_registration_message_sms_statusF"><?php echo app('translator')->get('lang.enable'); ?></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="">
                                                                    <input type="radio" name="student_registration_message_sms_status" id="student_registration_message_sms_statusM" value="2" class="common-radio relationButton"  <?php echo e(@$template->student_registration_message_sms_status == 2? 'checked': ''); ?>>
                                                                    <label for="student_registration_message_sms_statusM"><?php echo app('translator')->get('lang.disable'); ?></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="row mt-25">
                                    <div class="col-lg-12 mb-20">

                                        <span class="text-primary">[student_name] [student_username] [student_email] [student_password] [guardian_name] [guardian_email] [guardian_password] [admission_number] [school_name]</span>

                                    </div>
                                    <div class="col-lg-8">

                                        <div class="input-effect">
                                            <textarea class="primary-input form-control<?php echo e($errors->has('student_admission_message_sms') ? ' is-invalid' : ''); ?>" cols="0" rows="2" name="student_admission_message_sms" maxlength="500"><?php echo e(isset($template)? $template->student_admission_message_sms: old('student_admission_message_sms')); ?></textarea>
                                            <label><?php echo app('translator')->get('lang.student_admission_message'); ?> (<?php echo app('translator')->get('lang.sms'); ?>) <span></span></label>
                                            <span class="focus-border textarea"></span>

                                            <?php if($errors->has('student_admission_message_sms')): ?>
                                                <span class="error text-danger"><strong><?php echo e($errors->first('student_admission_message_sms')); ?></strong></span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="row">
                                                <div class="col-lg-5 d-flex">
                                                    <p class="text-uppercase fw-500 mb-10"><?php echo app('translator')->get('lang.status'); ?></p>
                                                </div>
                                                <div class="col-lg-7">
                                                    
                                                        <div class=" radio-btn-flex ml-20">
                                                            <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="">
                                                                    <input type="radio" name="student_admission_message_sms_status" id="student_admission_message_sms_statusF" value="1" class="common-radio relationButton"  <?php echo e(@$template->student_admission_message_sms_status == 1? 'checked': ''); ?>>
                                                                    <label for="student_admission_message_sms_statusF"><?php echo app('translator')->get('lang.enable'); ?></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="">
                                                                    <input type="radio" name="student_admission_message_sms_status" id="student_admission_message_sms_statusM" value="2" class="common-radio relationButton"  <?php echo e(@$template->student_admission_message_sms_status == 2? 'checked': ''); ?>>
                                                                    <label for="student_admission_message_sms_statusM"><?php echo app('translator')->get('lang.disable'); ?></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>

                                <div class="row mt-25">
                                    <div class="col-lg-12 mb-20">

                                        <span class="text-primary">[student_name] [parent_name] [admission_number] [exam_term] [subject] [date] [period] [school_name]</span>

                                    </div>
                                    <div class="col-lg-8">

                                        <div class="input-effect">
                                            <textarea class="primary-input form-control<?php echo e($errors->has('exam_schedule_message_sms') ? ' is-invalid' : ''); ?>" cols="0" rows="2" name="exam_schedule_message_sms" maxlength="500"><?php echo e(isset($template)? $template->exam_schedule_message_sms: old('admission_pro')); ?></textarea>
                                            <label><?php echo app('translator')->get('lang.exam_schedule_message'); ?> (<?php echo app('translator')->get('lang.sms'); ?>) <span></span></label>
                                            <span class="focus-border textarea"></span>

                                            <?php if($errors->has('exam_schedule_message_sms')): ?>
                                                <span class="error text-danger"><strong><?php echo e($errors->first('exam_schedule_message_sms')); ?></strong></span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="row">
                                                <div class="col-lg-5 d-flex">
                                                    <p class="text-uppercase fw-500 mb-10"><?php echo app('translator')->get('lang.status'); ?></p>
                                                </div>
                                                <div class="col-lg-7">
                                                    
                                                        <div class=" radio-btn-flex ml-20">
                                                            <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="">
                                                                    <input type="radio" name="exam_schedule_message_sms_status" id="exam_schedule_message_smsF" value="1" class="common-radio relationButton"  <?php echo e(@$template->exam_schedule_message_sms_status == 1? 'checked': ''); ?>>
                                                                    <label for="exam_schedule_message_smsF"><?php echo app('translator')->get('lang.enable'); ?></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="">
                                                                    <input type="radio" name="exam_schedule_message_sms_status" id="exam_schedule_message_smsM" value="2" class="common-radio relationButton"  <?php echo e(@$template->exam_schedule_message_sms_status == 2? 'checked': ''); ?>>
                                                                    <label for="exam_schedule_message_smsM"><?php echo app('translator')->get('lang.disable'); ?></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>

                                <div class="row mt-25">
                                    <div class="col-lg-12 mb-20">

                                        <span class="text-primary">[student_name] [parent_name] [due_amount] [fees_name] [due_date] [school_name]</span>

                                    </div>
                                    <div class="col-lg-8">

                                        <div class="input-effect">
                                            <textarea class="primary-input form-control<?php echo e($errors->has('dues_fees_message') ? ' is-invalid' : ''); ?>" cols="0" rows="2" name="dues_fees_message_sms" maxlength="500"><?php echo e(isset($template)? $template->dues_fees_message_sms: old('dues_fees_message_sms')); ?></textarea>
                                            <label><?php echo app('translator')->get('lang.dues_fees_message'); ?> (<?php echo app('translator')->get('lang.sms'); ?>) <span></span></label>
                                            <span class="focus-border textarea"></span>

                                            <?php if($errors->has('dues_fees_message_sms')): ?>
                                                <span class="error text-danger"><strong><?php echo e($errors->first('dues_fees_message_sms')); ?></strong></span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="row">
                                                <div class="col-lg-5 d-flex">
                                                    <p class="text-uppercase fw-500 mb-10"><?php echo app('translator')->get('lang.status'); ?></p>
                                                </div>
                                                <div class="col-lg-7">
                                                    
                                                        <div class=" radio-btn-flex ml-20">
                                                            <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="">
                                                                    <input type="radio" name="dues_fees_message_sms_status" id="dues_fees_message_smsF" value="1" class="common-radio relationButton"  <?php echo e(@$template->dues_fees_message_sms_status == 1? 'checked': ''); ?>>
                                                                    <label for="dues_fees_message_smsF"><?php echo app('translator')->get('lang.enable'); ?></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="">
                                                                    <input type="radio" name="dues_fees_message_sms_status" id="dues_fees_message_smsM" value="2" class="common-radio relationButton"  <?php echo e(@$template->dues_fees_message_sms_status == 2? 'checked': ''); ?>>
                                                                    <label for="dues_fees_message_smsM"><?php echo app('translator')->get('lang.disable'); ?></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>


                                

                                

                               
	                           <?php 
                                  $tooltip = "";
                                  if(in_array(50, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ){
                                        $tooltip = "";
                                    }else{
                                        $tooltip = "You have no permission to add";
                                    }
                                ?>
                                
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                        <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="<?php echo e($tooltip); ?>">
                                            <span class="ti-check"></span>
                                            
                                                <?php echo app('translator')->get('lang.update'); ?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo e(Form::close()); ?>

                    </div>
                </div>
            </div>
           
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/cambrid1/public_html/erp/resources/views/backEnd/communicate/sms_template.blade.php ENDPATH**/ ?>