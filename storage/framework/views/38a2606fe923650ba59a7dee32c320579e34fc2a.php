
<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/backEnd/')); ?>/css/croppie.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/backEnd/')); ?>/css/custom.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('mainContent'); ?>
    <section class="sms-breadcrumb mb-40 up_breadcrumb white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Pre Admission Form</h1>
                <div class="bc-pages">
                    <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
                    <a href="#"><?php echo app('translator')->get('lang.student_information'); ?></a>
                    <a href="#">Pre Admission Form</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-6">
                    <div class="main-title" style="margin-bottom: 20px;">
                        <h3 class="mb-0"><?php echo app('translator')->get('lang.add'); ?> Pre Admission Form</h3>
                    </div>
                </div>
            </div>

            <?php if(in_array(65, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ): ?>
                <?php echo e(Form::open(['class' => 'form-horizontal studentadmission', 'files' => true, 'route' => 'student_store__pre_admission',
                'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'student_form'])); ?>

            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12">
                    <?php if(session()->has('message-success')): ?>
                        <div class="alert alert-success">
                            <?php echo e(session()->get('message-success')); ?>

                        </div>
                    <?php elseif(session()->has('message-danger')): ?>
                        <div class="alert alert-danger">
                            <?php echo e(session()->get('message-danger')); ?>

                        </div>
                    <?php endif; ?>
                    <div class="white-box">
                        <div class="">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <?php if($errors->any()): ?>
                                        <div class="error text-danger "><?php echo e('Something went wrong, please try again'); ?></div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head"><?php echo app('translator')->get('lang.personal'); ?> <?php echo app('translator')->get('lang.info'); ?></h4>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="url" id="url" value="<?php echo e(URL::to('/')); ?>">
                            <div class="row mb-40 mt-30">
                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <input class="primary-input read-only-input form-control<?php echo e($errors->has('admission_number') ? ' is-invalid' : ''); ?>" type="text" name="admission_number" value="<?php echo e($max_admission_id != ''? $max_admission_id + 1 : 1); ?>" readonly>
                                        <label>Form No</label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('admission_number')): ?>
                                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('admission_number')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="no-gutters input-right-icon">
                                        <div class="col">
                                            <div class="input-effect">
                                                <input class="primary-input date" id="admissionDate" type="text"
                                                       name="date" value="<?php echo e(old('date') != ""? old('date'):date('m/d/Y')); ?>" autocomplete="off">
                                                <label>Date <span>*</span></label>
                                                <span class="focus-border"></span>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <button class="" type="button">
                                                <i class="ti-calendar" id="admission-date-icon"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('session') ? ' is-invalid' : ''); ?>" name="session">
                                            <option data-display="<?php echo app('translator')->get('lang.acadimic'); ?> <?php echo app('translator')->get('lang.year'); ?> *" value=""><?php echo app('translator')->get('lang.acadimic'); ?> <?php echo app('translator')->get('lang.year'); ?> *</option>
                                            <?php $__currentLoopData = $sessions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $session): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($session->id); ?>" <?php echo e(old('session') == $session->id? 'selected': ''); ?>><?php echo e($session->session); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('session')): ?>
                                            <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('session')); ?></strong>
								</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('student_name') ? ' is-invalid' : ''); ?>" type="text" name="student_name" id="student_name" value="<?php echo e(old('student_name')); ?>">
                                        <label>Student's Name <span>*</span> </label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('student_name')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('student_name')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>


                            </div>

                            <div class="row mb-40">

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('fathers_name') ? ' is-invalid' : ''); ?>" type="text" name="fathers_name" id="fathers_name" value="<?php echo e(old('fathers_name')); ?>">
                                        <label>Father Name <span>*</span> </label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('fathers_name')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('fathers_name')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('mothers_name') ? ' is-invalid' : ''); ?>" type="text" name="mothers_name" id="mothers_name" value="<?php echo e(old('mothers_name')); ?>">
                                        <label>Mother's Name <span>*</span><span></span> </label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('mothers_name')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('mothers_name')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('surname') ? ' is-invalid' : ''); ?>" type="text" name="surname" id="surname" value="<?php echo e(old('surname')); ?>">
                                        <label>SURNAME</label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('surname')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('surname')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                
                            </div>
                        </div>

                        <div class="row mb-40">

                            <div class="col-lg-3" style="margin-top: 30px;">
                                <div class="input-effect">
                                    <input class="primary-input form-control<?php echo e($errors->has('contact_no_1') ? ' is-invalid' : ''); ?>" type="number" name="contact_no_1" id="contact_no_1" value="<?php echo e(old('contact_no_1')); ?>">
                                    <label>Contact No 1:<span>*</span> </label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('contact_no_1')): ?>
                                        <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('contact_no_1')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3" style="margin-top: 30px;">
                                <div class="input-effect">
                                    <input class="primary-input form-control<?php echo e($errors->has('contact_no_2') ? ' is-invalid' : ''); ?>" type="number" name="contact_no_2" id="contact_no_2" value="<?php echo e(old('contact_no_2')); ?>">
                                    <label>Contact No 2:</label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('contact_no_2')): ?>
                                        <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('contact_no_2')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3" style="margin-top: 30px;">
                                <div class="input-effect">
                                    <input class="primary-input form-control<?php echo e($errors->has('student_email') ? ' is-invalid' : ''); ?>" type="email" name="student_email" value="<?php echo e(old('student_email')); ?>">
                                    <label><?php echo app('translator')->get('lang.email'); ?> <?php echo app('translator')->get('lang.address'); ?> <span>*</span> </label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('student_email')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('student_email')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3" style="margin-top: 30px;">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input date" id="date_of_birth" type="text"
                                                   name="date_of_birth" value="<?php echo e(old('date_of_birth') != ""? old('date_of_birth'):date('m/d/Y')); ?>" autocomplete="off">
                                            <label>DOB <span>*</span></label>
                                            <span class="focus-border"></span>
                                            <?php if($errors->has('af_date_of_birth')): ?>
                                                <span class="date_of_birth-feedback" role="alert">
                                                <strong><?php echo e($errors->first('date_of_birth')); ?></strong>
											</span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="" type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input form-control<?php echo e($errors->has('age') ? ' is-invalid' : ''); ?>" type="text" name="age" value="<?php echo e(old('age')); ?>" id="ageCalculated">
                                    <label>AGE <span>*</span></label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('age')): ?>
                                        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('age')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">

                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('admited_to_class') ? ' is-invalid' : ''); ?>" name="admited_to_class" id="classSelectStudent">
                                        <option data-display="Admitted to class *" value="">Admitted to class *</option>
                                        <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $class): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($class->id); ?>" <?php echo e(old('admited_to_class') == $class->id? 'selected': ''); ?>><?php echo e($class->class_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>

                                    <span class="focus-border"></span>
                                    <?php if($errors->has('admited_to_class')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
                                        <strong><?php echo e($errors->first('admited_to_class')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('gender_id') ? ' is-invalid' : ''); ?>" name="gender_id">
                                        <option data-display="<?php echo app('translator')->get('lang.gender'); ?> *" value=""><?php echo app('translator')->get('lang.gender'); ?> *</option>
                                        <?php $__currentLoopData = $genders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gender): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($gender->id); ?>" <?php echo e(old('gender_id') == $gender->id? 'selected': ''); ?>><?php echo e($gender->base_setup_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('gender_id')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
                                        <strong><?php echo e($errors->first('gender_id')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input read-only-input form-control<?php echo e($errors->has('no_of_sibling') ? ' is-invalid' : ''); ?>" type="text" name="no_of_sibling" value="<?php echo e(old('no_of_sibling')); ?>" >
                                    <label>No of Siblings <span>*</span></label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('no_of_sibling')): ?>
                                        <span class="invalid-feedback" role="alert">
									<strong><?php echo e($errors->first('no_of_sibling')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                        </div>

                        
                        <div class="row mb-40">
                            <div class="col-lg-6">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input" type="text" id="placeholderPhoto" placeholder="<?php echo app('translator')->get('lang.student_photo'); ?>"
                                                   readonly="">
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg" for="photo"><?php echo app('translator')->get('lang.browse'); ?></label>
                                            <input type="file" class="d-none" value="<?php echo e(old('photo')); ?>" name="photo" id="photo">
                                        </button>
                                    </div>
                                    <div class="col-lg-3 webcam">
                                        <div class="col-auto">
                                            <button class="primary-btn-small-input open" type="button">
                                                <label class="primary-btn small fix-gr-bg" for="webcam"><?php echo app('translator')->get('lang.web_cam'); ?></label>
                                            </button>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="results">Your captured image will appear here...</div>
                                <input type="hidden" name="webcam_photo" class="image-tag" value="">
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-12">
                                <div class="input-effect">
                                    <textarea class="primary-input form-control <?php echo e($errors->has('permanent_address') ? ' is-invalid' : ''); ?>" cols="0" rows="3" name="permanent_address" id="permanent_address"><?php echo e(old('permanent_address')); ?></textarea>
                                    <label>Address <span id="validationsColorStar">*</span> </label>
                                    <span class="focus-border textarea"></span>
                                    <?php if($errors->has('permanent_address')): ?>
                                        <span class="invalid-feedback">
										<strong><?php echo e($errors->first('permanent_address')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">

                            <div class="col-lg-2">
                                <div class="input-effect">
                                    <input class="primary-input read-only-input form-control<?php echo e($errors->has('pin_code') ? ' is-invalid' : ''); ?>" type="text" name="pin_code" value="<?php echo e(old('pin_code')); ?>" id="pin_code">
                                    <label>Pin Code<span>*</span></label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('pin_code')): ?>
                                        <span class="invalid-feedback" role="alert">
									<strong><?php echo e($errors->first('pin_code')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>


                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('fathers_occupation') ? ' is-invalid' : ''); ?>" name="fathers_occupation">
                                        <option data-display="Father’s Occupation *" value="">Father’s Occupation *</option>
                                        <option value="self-employed">Self Employed</option>
                                        <option value="private-service">Private Service</option>
                                        <option value="government-service">Government Service</option>
                                        <option value="other">other</option>
                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('fathers_occupation')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('fathers_occupation')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>


                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('mothers_occupation') ? ' is-invalid' : ''); ?>" name="mothers_occupation">
                                        <option data-display="Mother’s Occupation *" value="">Mother’s Occupation *</option>
                                        <option value="housewife">Housewife</option>
                                        <option value="self-employed">Self Employed</option>
                                        <option value="private-service">Private Service</option>
                                        <option value="government-service">Government Service</option>
                                        <option value="other">other</option>
                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('mothers_occupation')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('mothers_occupation')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="reference" value="<?php echo e(old('reference')); ?>">
                                    <label>Reference<span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="source" value="<?php echo e(old('source')); ?>">
                                    <label>Source<span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>


                        <div class="row mb-40">
                            <div class="col-lg-12">
                                <div class="input-effect">
                                    <textarea class="primary-input form-control <?php echo e($errors->has('previous_school_details') ? ' is-invalid' : ''); ?>" cols="0" rows="3" name="previous_school_details" id="previous_school_details"><?php echo e(old('previous_school_details')); ?></textarea>
                                    <label>Previous School Details <span id="validationsColorStar"></span> </label>
                                    <span class="focus-border textarea"></span>
                                    <?php if($errors->has('previous_school_details')): ?>
                                        <span class="invalid-feedback">
										<strong><?php echo e($errors->first('previous_school_details')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-12">
                                <div class="input-effect">
                                    <textarea class="primary-input form-control <?php echo e($errors->has('descriptions') ? ' is-invalid' : ''); ?>" cols="0" rows="3" name="descriptions" id="descriptions"><?php echo e(old('descriptions')); ?></textarea>
                                    <label>Description<span id="validationsColorStar"></span> </label>
                                    <span class="focus-border textarea"></span>
                                    <?php if($errors->has('descriptions')): ?>
                                        <span class="invalid-feedback">
										<strong><?php echo e($errors->first('descriptions')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input read-only-input form-control<?php echo e($errors->has('full_name') ? ' is-invalid' : ''); ?>" type="text" name="full_name" value="<?php echo e(old('full_name')); ?>" id="full_name">
                                    <label>Child Full Name<span>*</span></label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('full_name')): ?>
                                        <span class="invalid-feedback" role="alert">
									<strong><?php echo e($errors->first('full_name')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php
                            $tooltip = "";
                            if(in_array(65, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ){
                            $tooltip = "";
                            }else{
                            $tooltip = "You have no permission to add";
                            }
                        ?>

                        <div class="row mt-40">
                            <div class="col-lg-12 text-center">
                                
                                <input type="submit" class="primary-btn fix-gr-bg" value="Save" name="pre_admission">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo e(Form::close()); ?>

        </div>
    </section>
    
    <input type="text" id="STurl" value="<?php echo e(route('student_pre_admission_pic')); ?>" hidden>
    <div class="modal" id="LogoPic">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Crop Image And Upload</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div id="resize"></div>
                    <button class="btn rotate float-lef" data-deg="90" >
                        <i class="ti-back-right"></i></button>
                    <button class="btn rotate float-right" data-deg="-90" >
                        <i class="ti-back-left"></i></button>
                    <hr>

                    <button class="primary-btn fix-gr-bg pull-right" id="upload_logo">Crop</button>
                </div>
            </div>
        </div>
    </div>
    

    <div class="pop-outer" style="display:none">
        <div class="pop-inner">
            <div id="my_camera"></div>
            <span class="buttonwebcam">
			<input class="buttonsnap" type=button value="Take Snapshot" onClick="take_snapshot()">
			<input type=button class="closebutton" value="Close">

		</span>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(asset('public/backEnd/')); ?>/js/croppie.js"></script>
    <script src="<?php echo e(asset('public/backEnd/')); ?>/js/st_addmision.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <script>
        $(document).ready(function (){
            $(".open").click(function (){
                Webcam.set({
                    width: 414,
                    height: 205,
                    image_format: 'jpeg',
                    jpeg_quality: 90
                });
                Webcam.attach( '#my_camera' );
                $(".pop-outer").show();
            });

            $(".closebutton").click(function (){
                Webcam.reset()
                $(".pop-outer").hide();
            });
        });

        var data_uri = '/student-store';
        function take_snapshot() {
            $(".pop-outer").hide();
            Webcam.snap( function(data_uri) {
                $(".image-tag").val(data_uri);
                document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
            } );
            Webcam.reset()
        }

        $('div#disbaledItem').css("display", "none");

        $(document).on('change', '#trnasport', function() {
            var value=$(this).val();
            if (value=='yes'){
                $('div#disbaledItem').css("display", "block");
            }else {
                $('div#disbaledItem').css("display", "none");
            }
        });

        $(document).on('keypress','#contact_no_1, #contact_no_2 , #mothers_office_no , #mothers_contact_no',function(e){
            if($(e.target).prop('value').length>=12){
                if(e.keyCode!=32)
                {return false}
            }});


        $(document).on('keyup','#student_name ,#fathers_name ,#surname',function(e){
            fillupFullName();
        });

        function fillupFullName(data){
            var student_name=$('#student_name').val();
            var fathers_name=$('#fathers_name').val();
            var surname=$('#surname').val();
            var ConcatName=student_name +' '+ fathers_name +' '+ surname;
            $('#full_name').val(ConcatName);
            $('#full_name').addClass('has-content');
        }

        $(document).on('keypress','#pin_code',function(e){
            if($(e.target).prop('value').length>=6){
                if(e.keyCode!=32)
                {return false}
            }});


        function Person(dob) {
            // [1] new Date(dateString)
            this.birthday = new Date(dob); // transform birthday in date-object

            this.calculateAge = function() {
                // diff = now (in ms) - birthday (in ms)
                // diff = age in ms
                const diff = Date.now() - this.birthday.getTime();

                // [2] new Date(value); -> value = ms since 1970
                // = do as if person was born in 1970
                // this works cause we are interested in age, not a year
                const ageDate = new Date(diff);

                // check: 1989 = 1970 + 19
                console.log(ageDate.getUTCFullYear()); // 1989

                // age = year if person was born in 1970 (= 1989) - 1970 = 19
                return Math.abs(ageDate.getUTCFullYear() - 1970);
            };
        }


        $("#date_of_birth").change(function(){
            var dataItem=$(this).val();
            var TodayDate = new Date();
            var endDate= new Date(Date.parse(dataItem));

            if (endDate> TodayDate) {
                alert("Should be less than today date");
                $('#ageCalculated').val(0);
                $('#ageCalculated').addClass('has-content');
                $(this).val('');
            }


            var date = new Date($('#date_of_birth').val());
            var dateString = new Date(date.getTime() - (date.getTimezoneOffset() * 60000 ))
                .toISOString()
                .split("T")[0];

            var ageCalculated =new Person(dateString).calculateAge();

            $('#ageCalculated').val(ageCalculated);
            $('#ageCalculated').addClass('has-content');
        });

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\schollmanagementv5\resources\views/backEnd/preadmissions/student_pre_admisisons_form.blade.php ENDPATH**/ ?>