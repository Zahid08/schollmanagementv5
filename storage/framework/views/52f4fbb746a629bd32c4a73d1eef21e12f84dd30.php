

<style>
    .single_item {
        border: 1px solid #e8e3fa;
        border-radius: 10px;
        background: #fff;
        box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);
    }
    .for_items {
        margin-bottom: 40px;
        width: 100%;
    }
    a.dashboard_links {
        text-decoration: none !important;
    }
    .item_logo {
        padding: 17px 0px;
    }
    .text-center {
        text-align: center;
    }
</style>
<?php $__env->startSection('mainContent'); ?>
    <section class="mb-40">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-title">
                        <h3 class="mb-0"><?php echo app('translator')->get('lang.welcome'); ?> - <?php echo e(@Auth::user()->school->school_name); ?> | <?php echo e(@Auth::user()->roles->name); ?></h3>

                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 20px">
                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links" target="_blank">
                            <div class="single_item">
                                <div class="item_logo">
                                    <img src="http://erp.cambridgejuniorcollege.in/public/icons/adm_add.png">
                                </div>
                                <div class="item_text">
                                    <h5>Enquiry</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo e(route('student_pre_admission_form')); ?>" class="dashboard_links" target="_blank">
                            <div class="single_item">
                                <div class="item_logo">
                                    <img src="http://erp.cambridgejuniorcollege.in/public/icons/adm_add.png">
                                </div>
                                <div class="item_text">
                                    <h5>Pre-Admission</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo e(route('student_admission_form')); ?>" class="dashboard_links" target="_blank">
                            <div class="single_item">
                                <div class="item_logo">
                                    <img src="http://erp.cambridgejuniorcollege.in/public/icons/adm_add.png">
                                </div>
                                <div class="item_text">
                                    <h5>Admission Form</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo e(route('student_attendance')); ?>" class="dashboard_links" target="_blank">
                            <div class="single_item">
                                <div class="item_logo">
                                    <img src="http://erp.cambridgejuniorcollege.in/public/icons/attend.png">
                                </div>
                                <div class="item_text">
                                    <h5>Student Attendence</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo e(route('staff_attendance')); ?>" class="dashboard_links" target="_blank">
                            <div class="single_item">
                                <div class="item_logo">
                                    <img src="http://erp.cambridgejuniorcollege.in/public/icons/attend.png">
                                </div>
                                <div class="item_text">
                                    <h5>Staff Attendence</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="text-center for_items">
                        <a href="" class="dashboard_links" target="_blank">
                            <div class="single_item">
                                <div class="item_logo">
                                    <img src="http://erp.cambridgejuniorcollege.in/public/icons/fees.png">
                                </div>
                                <div class="item_text">
                                    <h5>Fees Collection</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="text-center for_items">
                        <a href="#" class="dashboard_links" target="_blank">
                            <div class="single_item">
                                <div class="item_logo">
                                    <img src="http://erp.cambridgejuniorcollege.in/public/icons/reportsicon.png">
                                </div>
                                <div class="item_text">
                                    <h5>Reports</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo e(route('addStaff')); ?>" class="dashboard_links" target="_blank">
                            <div class="single_item">
                                <div class="item_logo">
                                    <img src="http://erp.cambridgejuniorcollege.in/public/icons/hrm.png">
                                </div>
                                <div class="item_text">
                                    <h5>HRMS</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo e(route('class_routine_new')); ?>" class="dashboard_links" target="_blank">
                            <div class="single_item">
                                <div class="item_logo">
                                    <img src="http://erp.cambridgejuniorcollege.in/public/icons/timetable.png">
                                </div>
                                <div class="item_text">
                                    <h5>Time Table</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo e(url('upload-content')); ?>" class="dashboard_links" target="_blank">
                            <div class="single_item">
                                <div class="item_logo">
                                    <img src="http://erp.cambridgejuniorcollege.in/public/icons/assignments.png">
                                </div>
                                <div class="item_text">
                                    <h5>Assignment</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo e(url('add-homeworks')); ?>" class="dashboard_links" target="_blank">
                            <div class="single_item">
                                <div class="item_logo">
                                    <img src="http://erp.cambridgejuniorcollege.in/public/icons/homeworkicon1.jpg">
                                </div>
                                <div class="item_text">
                                    <h5>Home Work</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo e(url('add-book')); ?>" class="dashboard_links" target="_blank">
                            <div class="single_item">
                                <div class="item_logo">
                                    <img src="http://erp.cambridgejuniorcollege.in/public/icons/library.png">
                                </div>
                                <div class="item_text">
                                    <h5>Library</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo e(url('add-notice')); ?>" class="dashboard_links" target="_blank">
                            <div class="single_item">
                                <div class="item_logo">
                                    <img src="http://erp.cambridgejuniorcollege.in/public/icons/notifcenter.png">
                                </div>
                                <div class="item_text">
                                    <h5>Notificaitons</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo e(url('send-email-sms-view')); ?>" class="dashboard_links" target="_blank">
                            <div class="single_item">
                                <div class="item_logo">
                                    <img src="http://erp.cambridgejuniorcollege.in/public/icons/email1.png">
                                </div>
                                <div class="item_text">
                                    <h5>Email & SMS</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="text-center for_items">
                        <a href="<?php echo e(url('login-access-control')); ?>" class="dashboard_links" target="_blank">
                            <div class="single_item">
                                <div class="item_logo">
                                    <img src="http://erp.cambridgejuniorcollege.in/public/icons/login1.png">
                                </div>
                                <div class="item_text">
                                    <h5>Login Permission</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>


            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\schollmanagementv5\resources\views/backEnd/commonDashboard.blade.php ENDPATH**/ ?>