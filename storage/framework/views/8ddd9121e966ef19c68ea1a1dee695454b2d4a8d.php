
<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/backEnd/')); ?>/css/croppie.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/backEnd/')); ?>/css/custom.css">
<?php $__env->stopSection(); ?>
<?php
    function showPicName($data){
        $name = explode('/', $data);
        return $name[3];
    }
?>

<?php
    function showPicNameDoc($data){
        $name = explode('/', $data);
        return $name[4];
    }
?>


<?php $__env->startSection('mainContent'); ?>
    <section class="sms-breadcrumb mb-40 up_breadcrumb white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Pre Admission Form</h1>
                <div class="bc-pages">
                    <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
                    <a href="#"><?php echo app('translator')->get('lang.student_information'); ?></a>
                    <a href="#">Pre Admission Form</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-6">
                    <div class="main-title" style="margin-bottom: 20px;">
                        <h3 class="mb-0">Update Pre Admission Form</h3>
                    </div>
                </div>
            </div>

            <?php if(in_array(65, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ): ?>
                <?php echo e(Form::open(['class' => 'form-horizontal studentadmission', 'files' => true, 'route' => 'student_pre_admission_form_update',
                'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'student_form'])); ?>

            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12">
                    <?php if(session()->has('message-success')): ?>
                        <div class="alert alert-success">
                            <?php echo e(session()->get('message-success')); ?>

                        </div>
                    <?php elseif(session()->has('message-danger')): ?>
                        <div class="alert alert-danger">
                            <?php echo e(session()->get('message-danger')); ?>

                        </div>
                    <?php endif; ?>

                    <input type="hidden" name="url" id="url" value="<?php echo e(URL::to('/')); ?>">
                    <input type="hidden" name="id" id="id" value="<?php echo e($student->id); ?>">


                    <div class="white-box">
                        <div class="">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <?php if($errors->any()): ?>
                                        <div class="error text-danger "><?php echo e('Something went wrong, please try again'); ?></div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head"><?php echo app('translator')->get('lang.personal'); ?> <?php echo app('translator')->get('lang.info'); ?></h4>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="url" id="url" value="<?php echo e(URL::to('/')); ?>">
                            <div class="row mb-40 mt-30">
                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('admission_number') ? ' is-invalid' : ''); ?>" type="text" name="admission_number" value="<?php echo e($student->admission_number); ?>" onkeyup="GetAdminUpdate(this.value,<?php echo e($student->id); ?>)">
                                        <label>Form No</label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('admission_number')): ?>
                                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('admission_number')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="no-gutters input-right-icon">
                                        <div class="col">
                                            <div class="input-effect">
                                                <input class="primary-input date" id="admissionDate" type="text"
                                                       name="date" value="<?php echo e($student->date != ""? date('m/d/Y', strtotime($student->date)): date('m/d/Y')); ?>" autocomplete="off">
                                                <label>Date <span>*</span></label>
                                                <span class="focus-border"></span>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <button class="" type="button">
                                                <i class="ti-calendar" id="admission-date-icon"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('session') ? ' is-invalid' : ''); ?>" name="session" id="session">
                                            <option data-display="<?php echo app('translator')->get('lang.acadimic'); ?> <?php echo app('translator')->get('lang.year'); ?> *" value=""><?php echo app('translator')->get('lang.acadimic'); ?> <?php echo app('translator')->get('lang.year'); ?> *</option>
                                            <?php $__currentLoopData = $sessions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $session): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if(isset($student->session)): ?>
                                                    <option value="<?php echo e($session->id); ?>" <?php echo e($student->session == $session->id? 'selected':''); ?>><?php echo e($session->year); ?> [<?php echo e($session->title); ?>]</option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($session->id); ?>"><?php echo e($session->year); ?>[<?php echo e($session->title); ?>]</option>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('session')): ?>
                                            <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('session')); ?></strong>
								</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('student_name') ? ' is-invalid' : ''); ?>" type="text" name="student_name" id="student_name" value="<?php echo e($student->student_name); ?>">
                                        <label>Student's Name <span>*</span> </label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('student_name')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('student_name')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>


                            </div>

                            <div class="row mb-40">

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('fathers_name') ? ' is-invalid' : ''); ?>" type="text" name="fathers_name" id="fathers_name" value="<?php echo e($student->fathers_name); ?>">
                                        <label>Father Name <span>*</span> </label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('fathers_name')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('fathers_name')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('mothers_name') ? ' is-invalid' : ''); ?>" type="text" name="mothers_name" id="mothers_name" value="<?php echo e($student->mothers_name); ?>">
                                        <label>Mother's Name <span>*</span><span></span> </label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('mothers_name')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('mothers_name')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('surname') ? ' is-invalid' : ''); ?>" type="text" name="surname" id="surname" value="<?php echo e($student->surname); ?>">
                                        <label>Sure Name</label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('surname')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('surname')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                
                            </div>
                        </div>

                        <div class="row mb-40">

                            <div class="col-lg-3" style="margin-top: 30px;">
                                <div class="input-effect">
                                    <input class="primary-input form-control<?php echo e($errors->has('contact_no_1') ? ' is-invalid' : ''); ?>" type="number" name="contact_no_1" id="contact_no_1" value="<?php echo e($student->contact_no_1); ?>">
                                    <label>Contact No 1:<span>*</span> </label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('contact_no_1')): ?>
                                        <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('contact_no_1')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3" style="margin-top: 30px;">
                                <div class="input-effect">
                                    <input class="primary-input form-control<?php echo e($errors->has('contact_no_2') ? ' is-invalid' : ''); ?>" type="number" name="contact_no_2" id="contact_no_2" value="<?php echo e($student->contact_no_2); ?>">
                                    <label>Contact No 2:</label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('contact_no_2')): ?>
                                        <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('contact_no_2')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3" style="margin-top: 30px;">
                                <div class="input-effect">
                                    <input class="primary-input form-control<?php echo e($errors->has('student_email') ? ' is-invalid' : ''); ?>" type="email" name="student_email" value="<?php echo e($student->student_email); ?>">
                                    <label><?php echo app('translator')->get('lang.email'); ?> <?php echo app('translator')->get('lang.address'); ?> <span>*</span> </label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('student_email')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('student_email')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3" style="margin-top: 30px;">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input date" id="date_of_birth" type="text"
                                                   name="date_of_birth" value="<?php echo e($student->date_of_birth != ""? date('m/d/Y', strtotime($student->date_of_birth)): date('m/d/Y')); ?>" autocomplete="off">
                                            <label>DOB <span>*</span></label>
                                            <span class="focus-border"></span>
                                            <?php if($errors->has('af_date_of_birth')): ?>
                                                <span class="date_of_birth-feedback" role="alert">
                                                <strong><?php echo e($errors->first('date_of_birth')); ?></strong>
											</span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="" type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input form-control<?php echo e($errors->has('age') ? ' is-invalid' : ''); ?>" type="text" name="age" value="<?php echo e($student->age); ?>" id="ageCalculated">
                                    <label>AGe <span>*</span></label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('age')): ?>
                                        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('age')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('admited_to_class') ? ' is-invalid' : ''); ?>" name="admited_to_class" id="classSelectStudent">
                                        <option data-display="Admitted to class *" value="">Admitted to class *</option>
                                        <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $class): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($class->id); ?>" <?php echo e($student->admited_to_class == $class->id? 'selected':''); ?>><?php echo e($class->class_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>

                                    <span class="focus-border"></span>
                                    <?php if($errors->has('admited_to_class')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
                                        <strong><?php echo e($errors->first('admited_to_class')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('gender_id') ? ' is-invalid' : ''); ?>" name="gender_id">
                                        <option data-display="<?php echo app('translator')->get('lang.gender'); ?> *" value=""><?php echo app('translator')->get('lang.gender'); ?> *</option>
                                        <?php $__currentLoopData = $genders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gender): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(isset($student->gender_id)): ?>
                                                <option value="<?php echo e($gender->id); ?>" <?php echo e($student->gender_id == $gender->id? 'selected': ''); ?>><?php echo e($gender->base_setup_name); ?></option>
                                            <?php else: ?>
                                                <option value="<?php echo e($gender->id); ?>"><?php echo e($gender->base_setup_name); ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('gender_id')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
                                        <strong><?php echo e($errors->first('gender_id')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input read-only-input form-control<?php echo e($errors->has('no_of_sibling') ? ' is-invalid' : ''); ?>" type="text" name="no_of_sibling" value="<?php echo e($student->no_of_sibling); ?>" >
                                    <label>No of Siblings <span>*</span></label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('no_of_sibling')): ?>
                                        <span class="invalid-feedback" role="alert">
									<strong><?php echo e($errors->first('no_of_sibling')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                        </div>


                        
                        <div class="row mb-40">
                            <div class="col-lg-6">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input" type="text" id="placeholderPhoto" value="<?php echo e($student->student_photo != ""? showPicName($student->student_photo):''); ?>" placeholder="<?php echo e($student->student_photo != ""? showPicName($student->student_photo):'Student Photo'); ?> "
                                                   readonly="">
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg" for="photo"><?php echo app('translator')->get('lang.browse'); ?></label>
                                            <input type="file" class="d-none" value="<?php echo e(old('photo')); ?>" name="photo" id="photo">
                                        </button>
                                    </div>
                                    <div class="col-lg-3 webcam">
                                        <div class="col-auto">
                                            <button class="primary-btn-small-input open" type="button">
                                                <label class="primary-btn small fix-gr-bg" for="webcam"><?php echo app('translator')->get('lang.web_cam'); ?></label>
                                            </button>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="results">Your captured image will appear here...</div>
                                <input type="hidden" name="webcam_photo" class="image-tag" value="">
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-12">
                                <div class="input-effect">
                                    <textarea class="primary-input form-control <?php echo e($errors->has('permanent_address') ? ' is-invalid' : ''); ?>" cols="0" rows="3" name="permanent_address" id="permanent_address"><?php echo e($student->permanent_address); ?></textarea>
                                    <label>Address <span id="validationsColorStar">*</span> </label>
                                    <span class="focus-border textarea"></span>
                                    <?php if($errors->has('permanent_address')): ?>
                                        <span class="invalid-feedback">
										<strong><?php echo e($errors->first('permanent_address')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">

                            <div class="col-lg-2">
                                <div class="input-effect">
                                    <input class="primary-input read-only-input form-control<?php echo e($errors->has('pin_code') ? ' is-invalid' : ''); ?>" type="text" name="pin_code" value="<?php echo e($student->pin_code); ?>" id="pin_code">
                                    <label>Pin Code<span>*</span></label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('pin_code')): ?>
                                        <span class="invalid-feedback" role="alert">
									<strong><?php echo e($errors->first('pin_code')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>


                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('fathers_occupation') ? ' is-invalid' : ''); ?>" name="fathers_occupation">
                                        <option data-display="Father’s Occupation *" value="">Father’s Occupation *</option>
                                        <option value="self-employed" <?php echo e($student->fathers_occupation =='self-employed'? 'selected': ''); ?>>Self Employed</option>
                                        <option value="private-service" <?php echo e($student->fathers_occupation =='private-service'? 'selected': ''); ?>>Private Service</option>
                                        <option value="government-service" <?php echo e($student->fathers_occupation =='government-service'? 'selected': ''); ?>>Government Service</option>
                                        <option value="other" <?php echo e($student->fathers_occupation =='other'? 'selected': ''); ?>>other</option>
                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('fathers_occupation')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('fathers_occupation')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('mothers_occupation') ? ' is-invalid' : ''); ?>" name="mothers_occupation">
                                        <option data-display="Mother’s Occupation *" value="">Mother’s Occupation *</option>
                                        <option value="housewife" <?php echo e($student->mothers_occupation =='housewife'? 'selected': ''); ?>>Housewife</option>
                                        <option value="self-employed" <?php echo e($student->mothers_occupation =='self-employed'? 'selected': ''); ?>>Self Employed</option>
                                        <option value="private-service" <?php echo e($student->mothers_occupation =='private-service'? 'selected': ''); ?>>Private Service</option>
                                        <option value="government-service" <?php echo e($student->mothers_occupation =='government-service'? 'selected': ''); ?>>Government Service</option>
                                        <option value="other" <?php echo e($student->mothers_occupation =='other'? 'selected': ''); ?>>other</option>
                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('mothers_occupation')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('mothers_occupation')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="reference" value="<?php echo e($student->reference); ?>">
                                    <label>Reference<span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="source" value="<?php echo e($student->source); ?>">
                                    <label>Source<span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>


                        <div class="row mb-40">
                            <div class="col-lg-12">
                                <div class="input-effect">
                                    <textarea class="primary-input form-control <?php echo e($errors->has('previous_school_details') ? ' is-invalid' : ''); ?>" cols="0" rows="3" name="previous_school_details" id="previous_school_details"><?php echo e($student->previous_school_details); ?></textarea>
                                    <label>Previous School Details <span id="validationsColorStar"></span> </label>
                                    <span class="focus-border textarea"></span>
                                    <?php if($errors->has('previous_school_details')): ?>
                                        <span class="invalid-feedback">
										<strong><?php echo e($errors->first('previous_school_details')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-12">
                                <div class="input-effect">
                                    <textarea class="primary-input form-control <?php echo e($errors->has('descriptions') ? ' is-invalid' : ''); ?>" cols="0" rows="3" name="descriptions" id="descriptions"><?php echo e($student->descriptions); ?></textarea>
                                    <label>Description<span id="validationsColorStar"></span> </label>
                                    <span class="focus-border textarea"></span>
                                    <?php if($errors->has('descriptions')): ?>
                                        <span class="invalid-feedback">
										<strong><?php echo e($errors->first('descriptions')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input read-only-input form-control<?php echo e($errors->has('full_name') ? ' is-invalid' : ''); ?>" type="text" name="full_name" value="<?php echo e($student->full_name); ?>" id="full_name">
                                    <label>Full Name<span>*</span></label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('full_name')): ?>
                                        <span class="invalid-feedback" role="alert">
									<strong><?php echo e($errors->first('full_name')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php
                            $tooltip = "";
                            if(in_array(65, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ){
                            $tooltip = "";
                            }else{
                            $tooltip = "You have no permission to add";
                            }
                        ?>

                        <div class="row mt-40">
                            <div class="col-lg-12 text-center">
                                <input type="submit" class="primary-btn fix-gr-bg" value="Admission Confirmed" name="pre_admission">
                                <input type="submit" class="primary-btn fix-gr-bg" value="Update" name="pre_admission">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo e(Form::close()); ?>

        </div>
    </section>
    
    <input type="text" id="STurl" value="<?php echo e(route('student_admission_pic')); ?>" hidden>
    <div class="modal" id="LogoPic">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Crop Image And Upload</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div id="resize"></div>
                    <button class="btn rotate float-lef" data-deg="90" >
                        <i class="ti-back-right"></i></button>
                    <button class="btn rotate float-right" data-deg="-90" >
                        <i class="ti-back-left"></i></button>
                    <hr>

                    <button class="primary-btn fix-gr-bg pull-right" id="upload_logo">Crop</button>
                </div>
            </div>
        </div>
    </div>
    

    <div class="pop-outer" style="display:none">
        <div class="pop-inner">
            <div id="my_camera"></div>
            <span class="buttonwebcam">
			<input class="buttonsnap" type=button value="Take Snapshot" onClick="take_snapshot()">
			<input type=button class="closebutton" value="Close">

		</span>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(asset('public/backEnd/')); ?>/js/croppie.js"></script>
    <script src="<?php echo e(asset('public/backEnd/')); ?>/js/st_addmision.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <script>
        $(document).ready(function (){
            $(".open").click(function (){
                Webcam.set({
                    width: 414,
                    height: 205,
                    image_format: 'jpeg',
                    jpeg_quality: 90
                });
                Webcam.attach( '#my_camera' );
                $(".pop-outer").show();
            });

            $(".closebutton").click(function (){
                Webcam.reset()
                $(".pop-outer").hide();
            });
        });

        var data_uri = '/student-store';
        function take_snapshot() {
            $(".pop-outer").hide();
            Webcam.snap( function(data_uri) {
                $(".image-tag").val(data_uri);
                document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
            } );
            Webcam.reset()
        }

        $('div#disbaledItem').css("display", "none");

        $(document).on('change', '#trnasport', function() {
            var value=$(this).val();
            if (value=='yes'){
                $('div#disbaledItem').css("display", "block");
            }else {
                $('div#disbaledItem').css("display", "none");
            }
        });

        $(document).on('keypress','#contact_no_1, #contact_no_2 , #mothers_office_no , #mothers_contact_no',function(e){
            if($(e.target).prop('value').length>=12){
                if(e.keyCode!=32)
                {return false}
            }});


        $(document).on('keyup','#student_name ,#fathers_name ,#surname',function(e){
            fillupFullName();
        });

        function fillupFullName(data){
            var student_name=$('#student_name').val();
            var fathers_name=$('#fathers_name').val();
            var surname=$('#surname').val();
            var ConcatName=student_name +' '+ fathers_name +' '+ surname;
            $('#full_name').val(ConcatName);
            $('#full_name').addClass('has-content');
        }

        $(document).on('keypress','#pin_code',function(e){
            if($(e.target).prop('value').length>=6){
                if(e.keyCode!=32)
                {return false}
            }});


        function getAge(dateString) {
            var now = new Date();
            var today = new Date(now.getYear(),now.getMonth(),now.getDate());

            var yearNow = now.getYear();
            var monthNow = now.getMonth();
            var dateNow = now.getDate();

            var dob = new Date(dateString.substring(6,10),
                dateString.substring(0,2)-1,
                dateString.substring(3,5)
            );

            var yearDob = dob.getYear();
            var monthDob = dob.getMonth();
            var dateDob = dob.getDate();
            var age = {};
            var ageString = "";
            var yearString = "";
            var monthString = "";
            var dayString = "";


            yearAge = yearNow - yearDob;

            if (monthNow >= monthDob)
                var monthAge = monthNow - monthDob;
            else {
                yearAge--;
                var monthAge = 12 + monthNow -monthDob;
            }

            if (dateNow >= dateDob)
                var dateAge = dateNow - dateDob;
            else {
                monthAge--;
                var dateAge = 31 + dateNow - dateDob;

                if (monthAge < 0) {
                    monthAge = 11;
                    yearAge--;
                }
            }

            age = {
                years: yearAge,
                months: monthAge,
                days: dateAge
            };

            if ( age.years > 1 ) yearString = " years";
            else yearString = " year";
            if ( age.months> 1 ) monthString = " months";
            else monthString = " month";
            if ( age.days > 1 ) dayString = " days";
            else dayString = " day";


            if ( (age.years > 0) && (age.months > 0) && (age.days > 0) )
                ageString = age.years + yearString + ", " + age.months + monthString + ", and " + age.days + dayString + " old.";
            else if ( (age.years == 0) && (age.months == 0) && (age.days > 0) )
                ageString = "Only " + age.days + dayString + " old!";
            else if ( (age.years > 0) && (age.months == 0) && (age.days == 0) )
                ageString = age.years + yearString + " old. Happy Birthday!!";
            else if ( (age.years > 0) && (age.months > 0) && (age.days == 0) )
                ageString = age.years + yearString + " and " + age.months + monthString + " old.";
            else if ( (age.years == 0) && (age.months > 0) && (age.days > 0) )
                ageString = age.months + monthString + " and " + age.days + dayString + " old.";
            else if ( (age.years > 0) && (age.months == 0) && (age.days > 0) )
                ageString = age.years + yearString + " and " + age.days + dayString + " old.";
            else if ( (age.years == 0) && (age.months > 0) && (age.days == 0) )
                ageString = age.months + monthString + " old.";
            else ageString = "Oops! Could not calculate age!";

            return ageString;
        }


        //mm/dd/year
        $("#date_of_birth").change(function(){
            var dataItem=$(this).val();
            var TodayDate = new Date();
            var endDate= new Date(Date.parse(dataItem));

            if (endDate> TodayDate) {
                alert("Should be less than today date");
                $('#ageCalculated').val(0);
                $('#ageCalculated').addClass('has-content');
                $(this).val('');
            }

            var ageCalculated =getAge($('#date_of_birth').val());

            $('#ageCalculated').val(ageCalculated);
            $('#ageCalculated').addClass('has-content');
        });

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\schollmanagementv5\resources\views/backEnd/preadmissions/student_pre_admission_form_edit.blade.php ENDPATH**/ ?>