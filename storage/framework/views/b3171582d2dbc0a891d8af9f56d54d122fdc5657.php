<script src="<?php echo e(asset('public/backEnd/')); ?>/js/main.js"></script>

<?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => 'admission_query_update', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'admission-query-store'])); ?>

<input type="hidden" name="id" value="<?php echo e(@$admission_query->id); ?>">
<div class="modal-body">
    <div class="container-fluid">
        <form action="">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="input-effect">
                                <input class="primary-input read-only-input form-control" type="text" name="name" id="name" value="<?php echo e(@$admission_query->name); ?>" required>
                                <label><?php echo app('translator')->get('lang.name'); ?> <span>*</span></label>
                                <span class="text-danger" role="alert" id="nameError">
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="input-effect">
                                <input class="primary-input read-only-input form-control" type="text" name="phone" id="phone" value="<?php echo e(@$admission_query->phone); ?>" onkeypress="return isNumberKey(event)" required>
                                <label><?php echo app('translator')->get('lang.phone'); ?> <span>*</span></label>
                                <span class="text-danger" role="alert" id="phoneError">
                                   
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="input-effect">
                                <input class="primary-input read-only-input form-control" type="text" name="email" value="<?php echo e(@$admission_query->email); ?>">
                                <label><?php echo app('translator')->get('lang.email'); ?> <span></span></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 mt-25">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-effect">
                                <textarea class="primary-input form-control" cols="0" rows="3" name="address" id="address"><?php echo e(@$admission_query->address); ?></textarea>
                                <label><?php echo app('translator')->get('lang.address'); ?> <span></span> </label>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-effect">
                                <textarea class="primary-input form-control" cols="0" rows="3" name="description" id="description"><?php echo e(@$admission_query->description); ?></textarea>
                                <label><?php echo app('translator')->get('lang.description'); ?> <span></span> </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 mt-25">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="no-gutters input-right-icon">
                                <div class="col">
                                    <div class="input-effect">
                                        <input class="primary-input date form-control" id="startDate" type="text"
                                             name="date" readonly="true" value="<?php echo e(@$admission_query->date != ""? date('m/d/Y', strtotime(@$admission_query->date)) : date('m/d/Y')); ?>">
                                        <label><?php echo app('translator')->get('lang.date'); ?></label>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <button class="" type="button">
                                        <i class="ti-calendar" id="start-date-icon"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="no-gutters input-right-icon">
                                <div class="col">
                                    <div class="input-effect">
                                        <input class="primary-input date form-control" id="endDate" type="text"
                                             name="next_follow_up_date" autocomplete="off" readonly="true"  value="<?php echo e(@$admission_query->next_follow_up_date != ""? date('m/d/Y', strtotime(@$admission_query->next_follow_up_date)) : date('m/d/Y')); ?>">
                                        <label><?php echo app('translator')->get('lang.next_follow_up_date'); ?></label>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <button class="" type="button">
                                        <i class="ti-calendar" id="end-date-icon"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="input-effect">
                                <input class="primary-input read-only-input form-control" type="text" name="assigned" value="<?php echo e(@$admission_query->assigned); ?>" id="assigned" required>
                                <label><?php echo app('translator')->get('lang.assigned'); ?> <span></span></label>
                                <span class="text-danger" role="alert" id="assignedError"> </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 mt-25">
                    <div class="row">
                        <div class="col-lg-3">
                            <select class="niceSelect w-100 bb" name="reference" id="reference" required>
                                <option data-display="<?php echo app('translator')->get('lang.reference'); ?>" value=""><?php echo app('translator')->get('lang.reference'); ?></option>
                                <?php $__currentLoopData = $references; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reference): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e(@$reference->id); ?>" <?php echo e(@$reference->id == @$admission_query->reference? 'selected':''); ?>><?php echo e(@$reference->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <span class="text-danger" role="alert" id="referenceError"></span>
                        </div>
                        <div class="col-lg-3">
                            <select class="niceSelect w-100 bb" name="source" id="source" required>
                                <option data-display="<?php echo app('translator')->get('lang.source'); ?> *" value=""><?php echo app('translator')->get('lang.source'); ?> *</option>
                                <?php $__currentLoopData = $sources; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $source): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e(@$source->id); ?>" <?php echo e(@$source->id == @$admission_query->source? 'selected':''); ?>><?php echo e(@$source->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <span class="text-danger" role="alert" id="sourceError">                                
                            </span>
                        </div>
                        <div class="col-lg-3">
                            <select class="niceSelect w-100 bb" name="class" id="class" id="class" required>
                                <option data-display="<?php echo app('translator')->get('lang.class'); ?>" value=""><?php echo app('translator')->get('lang.class'); ?></option>
                                <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $class): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e(@$class->id); ?>" <?php echo e(@$class->id == @$admission_query->class? 'selected':''); ?>><?php echo e(@$class->class_name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <span class="text-danger" role="alert" id="classError"></span>   
                        </div>
                        <div class="col-lg-3">
                            <div class="input-effect">
                                <input class="primary-input form-control" type="text" name="no_of_child" value="<?php echo e(@$admission_query->no_of_child); ?>" id="no_of_child" required>
                                <label><?php echo app('translator')->get('lang.number_of_child'); ?> <span></span></label>
                                <span class="text-danger" role="alert" id="no_of_childError"></span> 
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 mt-25">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="input-effect">
                                <input class="primary-input read-only-input form-control" type="text" name="father_name" value="<?php echo e($admission_query->father_name); ?>">
                                <label><?php echo app('translator')->get('lang.father_name'); ?><span></span></label>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="input-effect">
                                <input class="primary-input read-only-input form-control" type="text" name="father_occupations" value="<?php echo e($admission_query->father_occupations); ?>">
                                <label><?php echo app('translator')->get('lang.father_occupations'); ?><span></span></label>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="input-effect">
                                <input class="primary-input read-only-input form-control" type="text" name="mother_name" value="<?php echo e($admission_query->mother_name); ?>">
                                <label><?php echo app('translator')->get('lang.mother_name'); ?><span></span></label>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="input-effect">
                                <input class="primary-input read-only-input form-control" type="text" name="mother_occupations" value="<?php echo e($admission_query->mother_occupations); ?>">
                                <label><?php echo app('translator')->get('lang.mother_occupations'); ?><span></span></label>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-lg-12 mt-25">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="input-effect">
                                <textarea class="primary-input form-control" cols="0" rows="3" name="previous_school_details" id="previous_school_details"><?php echo e($admission_query->previous_school_details); ?></textarea>
                                <label><?php echo app('translator')->get('lang.previous_school_details'); ?><span></span> </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 mt-25">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="no-gutters input-right-icon">
                                <div class="col">
                                    <div class="input-effect">
                                        <input class="primary-input date form-control<?php echo e($errors->has('date_of_birth') ? ' is-invalid' : ''); ?>" id="startDate" type="text" name="date_of_birth" value="<?php echo e(date('m/d/Y', strtotime($admission_query->date_of_birth))); ?>" autocomplete="off">
                                        <span class="focus-border"></span>
                                        <label><?php echo app('translator')->get('lang.date_of_birth'); ?> <span>*</span></label>
                                        <?php if($errors->has('date_of_birth')): ?>
                                            <span class="invalid-feedback" role="alert">
											<strong><?php echo e($errors->first('date_of_birth')); ?></strong>
										</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <button class="" type="button">
                                        <i class="ti-calendar" id="start-date-icon"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 mt-25">
                    <div class="row addNewLayoutEdit">
                        <?php if(@$admission_query->follow_up): ?>
                            <?php $__currentLoopData = json_decode($admission_query->follow_up); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $follow_up): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-lg-12 layouts layout-<?php echo e($i); ?>">

                                    <div class="input-effect">
                                        <input class="primary-input form-control" type="text" name="follow_up[<?php echo e($i); ?>]" value="<?php echo e($follow_up); ?>">
                                        <label><?php echo app('translator')->get('lang.follow_up'); ?> <?php echo e($i); ?><span></span></label>
                                    </div>
                                    <?php if($i==1): ?>
                                        <div class="col-lg-2 addbutton">
                                            <button type="button" class="primary-btn-small-input primary-btn small fix-gr-bg" onclick="addNewFiled()"><i class="fa fa-plus"></i></button>
                                        </div>
                                    <?php else: ?>
                                        <div class="col-lg-2 removebutton">
                                            <button type="button" class="btn btn-warning btn-icon" onclick="removeLayoutFiled(<?php echo e($i); ?>)"><i class="fa fa-minus"></i></button>
                                        </div>
                                    <?php endif; ?>

                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <div class="col-lg-12 layouts layout-1">

                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="follow_up[1]" value="">
                                    <label><?php echo app('translator')->get('lang.follow_up'); ?> 1</label>
                                </div>

                                <div class="col-lg-2 addbutton">
                                    <button type="button" class="primary-btn-small-input primary-btn small fix-gr-bg" onclick="addNewFiled()"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="col-lg-12 text-center mt-40">
                    <div class="mt-40 d-flex justify-content-between">
                        <button type="button" class="primary-btn tr-bg" data-dismiss="modal"><?php echo app('translator')->get('lang.cancel'); ?></button>

                        <button class="primary-btn fix-gr-bg" id="save_button_query" type="submit"><?php echo app('translator')->get('lang.update'); ?></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<div id="addNewInput" style="display: none">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12 layouts layout-0key0">
                <div class="input-effect">
                    <input class="primary-input form-control" type="text" name="follow_up[0key0]">
                    <label><?php echo app('translator')->get('lang.follow_up'); ?> 0key0<span></span></label>
                </div>

                <div class="col-lg-2 removebutton">
                    <button type="button" class="btn btn-warning btn-icon" onclick="removeLayoutFiled(0key0)"><i class="fa fa-minus"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo e(Form::close()); ?>

<style>
    .col-lg-2.addbutton {
        float: right;
        margin-right: -137px;
    }
    .col-lg-2.removebutton {
        float: right;
        margin-right: -137px;
        margin-top: 21px;
    }
</style>
<!-- End Sibling Add Modal -->
<script type="text/javascript">

    //add new layout
    function addNewFiled(){
        var rowCount = $('.addNewLayoutEdit .layouts').length;
        rowCount = rowCount + 1;

        if (rowCount > 10){
            $this.parents('form-group').addClass('has-error');
            $this.parents('.form-group').find('.help-block').append('<p>Please upload maximum </p>');
            $this.val('');
            event.preventDefault();
        }
        var new_cf = $('#addNewInput').clone();
        var html = $(new_cf).html().replace(/0key0/g,rowCount);

        $('.addNewLayoutEdit').append(html);
    }


    //remove layout
    function removeLayoutFiled(key){
        jQuery('.layout-'+key).remove();
    }

</script>


<!-- End Sibling Add Modal -->
<?php /**PATH D:\xampp\htdocs\schollmanagementv5\resources\views/backEnd/admin/admission_query_edit.blade.php ENDPATH**/ ?>