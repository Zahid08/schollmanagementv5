
<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/backEnd/')); ?>/css/croppie.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/backEnd/')); ?>/css/custom.css">
<?php $__env->stopSection(); ?>

<?php
    function showPicName($data){
        $name = explode('/', $data);
        return $name[3];
    }
?>

<?php
    function showPicNameDoc($data){
        $name = explode('/', $data);
        return $name[4];
    }
?>

<?php $__env->startSection('mainContent'); ?>
    <section class="sms-breadcrumb mb-40 up_breadcrumb white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1><?php echo app('translator')->get('lang.admission_form'); ?></h1>
                <div class="bc-pages">
                    <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
                    <a href="#"><?php echo app('translator')->get('lang.student_information'); ?></a>
                    <a href="#"><?php echo app('translator')->get('lang.admission_form'); ?></a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_st_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-6">
                    <div class="main-title">
                        <h3 class="mb-0"><?php echo app('translator')->get('lang.add'); ?> <?php echo app('translator')->get('lang.admission_form'); ?></h3>
                    </div>
                </div>
                <?php if(in_array(63, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ): ?>
                    <div class="offset-lg-3 col-lg-3 text-right mb-20">
                        <a href="#" class="primary-btn small fix-gr-bg">
                            <span class="ti-plus pr-2"></span>
                            <?php echo app('translator')->get('lang.import'); ?> <?php echo app('translator')->get('lang.admission_form'); ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>

            <?php if(in_array(65, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ): ?>
                <?php echo e(Form::open(['class' => 'form-horizontal studentadmission', 'files' => true, 'route' => 'student_admission_form_update',
                'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'student_form'])); ?>

            <?php endif; ?>
            <div class="row">

                <input type="hidden" name="url" id="url" value="<?php echo e(URL::to('/')); ?>">
                <input type="hidden" name="id" id="id" value="<?php echo e($student->id); ?>">

                <div class="col-lg-12">
                    <?php if(session()->has('message-success')): ?>
                        <div class="alert alert-success">
                            <?php echo e(session()->get('message-success')); ?>

                        </div>
                    <?php elseif(session()->has('message-danger')): ?>
                        <div class="alert alert-danger">
                            <?php echo e(session()->get('message-danger')); ?>

                        </div>
                    <?php endif; ?>
                    <div class="white-box">
                        <div class="">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <?php if($errors->any()): ?>
                                        <div class="error text-danger "><?php echo e('Something went wrong, please try again'); ?></div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head"><?php echo app('translator')->get('lang.personal'); ?> <?php echo app('translator')->get('lang.info'); ?></h4>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="url" id="url" value="<?php echo e(URL::to('/')); ?>">
                            <div class="row mb-40 mt-30">
                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('admission_number') ? ' is-invalid' : ''); ?>" type="text" name="admission_number" value="<?php echo e($student->admission_no); ?>" onkeyup="GetAdminUpdate(this.value,<?php echo e($student->id); ?>)">
                                        <label>Form No</label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('admission_number')): ?>
                                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('admission_number')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>


                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('session') ? ' is-invalid' : ''); ?>" name="session" id="session">
                                            <option data-display="<?php echo app('translator')->get('lang.acadimic'); ?> <?php echo app('translator')->get('lang.year'); ?> *" value=""><?php echo app('translator')->get('lang.acadimic'); ?> <?php echo app('translator')->get('lang.year'); ?> *</option>
                                            <?php $__currentLoopData = $sessions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $session): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if(isset($student->session)): ?>
                                                    <option value="<?php echo e($session->id); ?>" <?php echo e($student->session_id == $session->id? 'selected':''); ?>><?php echo e($session->year); ?> [<?php echo e($session->title); ?>]</option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($session->id); ?>"><?php echo e($session->year); ?>[<?php echo e($session->title); ?>]</option>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('session')): ?>
                                            <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('session')); ?></strong>
								</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('full_name') ? ' is-invalid' : ''); ?>" type="text" name="full_name" value="<?php echo e($student->full_name); ?>">
                                        <label>Full Name<span>*</span> </label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('full_name')): ?>
                                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('full_name')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>


                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('admited_to_class') ? ' is-invalid' : ''); ?>" name="admited_to_class" id="classSelectStudent">
                                            <option data-display="Admitted to class *" value="">Admitted to class *</option>
                                            <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $class): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($class->id); ?>" <?php echo e($student->class_id == $class->id? 'selected':''); ?>><?php echo e($class->class_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>

                                        <span class="focus-border"></span>
                                        <?php if($errors->has('admited_to_class')): ?>
                                            <span class="invalid-feedback invalid-select" role="alert">
                                        <strong><?php echo e($errors->first('admited_to_class')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40">
                                <div class="col-lg-3">
                                    <div class="input-effect" id="sectionStudentDiv">
                                        <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('section') ? ' is-invalid' : ''); ?>" name="section" id="sectionSelectStudent">
                                            <option data-display="Division *" value="">Division *</option>
                                            <?php if(isset($student->section_id)): ?>
                                                <?php $__currentLoopData = $student->sections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $section): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($section->id); ?>" <?php echo e($student->section_id == $section->id? 'selected': ''); ?>><?php echo e($section->section_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('section')): ?>
                                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('section')); ?></strong>
                                    </span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('roll_no') ? ' is-invalid' : ''); ?>" type="number" name="roll_no" value="<?php echo e($student->roll_no); ?>">
                                        <label>Roll Number <span>*</span></label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('roll_no')): ?>
                                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('roll_no')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="no-gutters input-right-icon">
                                        <div class="col">
                                            <div class="input-effect">
                                                <input class="primary-input date" id="admissionDate" type="text"
                                                       name="admission_date" value="<?php echo e($student->admission_date != ""? date('m/d/Y', strtotime($student->admission_date)): date('m/d/Y')); ?>" autocomplete="off">
                                                <label>Admission Date <span>*</span></label>
                                                <span class="focus-border"></span>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <button class="" type="button">
                                                <i class="ti-calendar" id="admission-date-icon"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="input-effect">
                                        <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('fees_package_id') ? ' is-invalid' : ''); ?>" name="fees_package_id">
                                            <option data-display="Fees Package *" value="">Fees Package *</option>
                                            <?php $__currentLoopData = $feesList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $feesPackage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($feesPackage->id); ?>" <?php echo e($student->fees_package_id==$feesPackage->id?'selected':''); ?>><?php echo e($feesPackage->feesGroups->name); ?> <?php echo e('₹'. $feesPackage->amount); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('fees_package_id')): ?>
                                            <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('fees_package_id')); ?></strong>
								</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="row mb-40">

                            <div class="col-lg-3">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input date" id="date_of_birth" type="text"
                                                   name="date_of_birth" value="<?php echo e($student->date_of_birth != ""? date('m/d/Y', strtotime($student->date_of_birth)): date('m/d/Y')); ?>" autocomplete="off">
                                            <label>DOB <span>*</span></label>
                                            <span class="focus-border"></span>
                                            <?php if($errors->has('af_date_of_birth')): ?>
                                                <span class="date_of_birth-feedback" role="alert">
                                                <strong><?php echo e($errors->first('date_of_birth')); ?></strong>
											</span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="" type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input form-control<?php echo e($errors->has('age') ? ' is-invalid' : ''); ?>" type="text" name="age" value="<?php echo e($student->age); ?>" id="ageCalculated">
                                    <label>AGe <span>*</span></label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('age')): ?>
                                        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('age')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input read-only-input form-control<?php echo e($errors->has('date_of_place') ? ' is-invalid' : ''); ?>" type="text" name="date_of_place" value="<?php echo e($student->date_of_place); ?>" >
                                    <label>Birth of Place<span>*</span></label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('date_of_place')): ?>
                                        <span class="invalid-feedback" role="alert">
									<strong><?php echo e($errors->first('date_of_place')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('gender_id') ? ' is-invalid' : ''); ?>" name="gender_id">
                                        <option data-display="<?php echo app('translator')->get('lang.gender'); ?> *" value=""><?php echo app('translator')->get('lang.gender'); ?> *</option>
                                        <?php $__currentLoopData = $genders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gender): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(isset($student->gender_id)): ?>
                                                <option value="<?php echo e($gender->id); ?>" <?php echo e($student->gender_id == $gender->id? 'selected': ''); ?>><?php echo e($gender->base_setup_name); ?></option>
                                            <?php else: ?>
                                                <option value="<?php echo e($gender->id); ?>"><?php echo e($gender->base_setup_name); ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('gender_id')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
                                        <strong><?php echo e($errors->first('gender_id')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            
                        </div>

                        <div class="row mb-40">

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input read-only-input form-control<?php echo e($errors->has('no_of_sibling') ? ' is-invalid' : ''); ?>" type="text" name="no_of_sibling" value="<?php echo e($student->no_of_sibling); ?>" >
                                    <label>No of Siblings <span>*</span></label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('no_of_sibling')): ?>
                                        <span class="invalid-feedback" role="alert">
									<strong><?php echo e($errors->first('no_of_sibling')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('bloodgroup_id') ? ' is-invalid' : ''); ?>" name="bloodgroup_id">
                                        <option data-display="<?php echo app('translator')->get('lang.blood_group'); ?> *" value=""><?php echo app('translator')->get('lang.blood_group'); ?><span>*</span></option>
                                        <?php $__currentLoopData = $blood_groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blood_group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(isset($student->bloodgroup_id)): ?>
                                                <option value="<?php echo e($blood_group->id); ?>" <?php echo e($blood_group->id == $student->bloodgroup_id? 'selected': ''); ?>><?php echo e($blood_group->base_setup_name); ?></option>
                                            <?php else: ?>
                                                <option value="<?php echo e($blood_group->id); ?>"><?php echo e($blood_group->base_setup_name); ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('bloodgroup_id')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('bloodgroup_id')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('nationality') ? ' is-invalid' : ''); ?>" name="nationality">
                                        <option data-display="Nationality *" value="">Nationality *</option>
                                        <option value="Indian" <?php echo e($student->nationality =='Indian'? 'selected': ''); ?>>Indian</option>
                                        <option value="NRI" <?php echo e($student->nationality =='NRI'? 'selected': ''); ?>>NRI</option>
                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('nationality')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('nationality')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('religion_id') ? ' is-invalid' : ''); ?>" name="religion_id">
                                        <option data-display="<?php echo app('translator')->get('lang.religion'); ?> *" value=""><?php echo app('translator')->get('lang.religion'); ?><span>*</span></option>
                                        <?php $__currentLoopData = $religions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $religion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($religion->id); ?>" <?php echo e($student->religion_id != ""? ($student->religion_id == $religion->id? 'selected':''):''); ?>><?php echo e($religion->base_setup_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('religion_id')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('religion_id')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-2">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('caste') ? ' is-invalid' : ''); ?>" name="caste">
                                        <option data-display="Caste *" value="">Caste *</option>
                                        <option value="General" <?php echo e($student->caste =='General'? 'selected': ''); ?>>General</option>
                                        <option value="OBC" <?php echo e($student->caste =='OBC'? 'selected': ''); ?>>OBC</option>
                                        <option value="EBC" <?php echo e($student->caste =='EBC'? 'selected': ''); ?>>EBC</option>
                                        <option value="SC" <?php echo e($student->caste =='SC'? 'selected': ''); ?>>SC</option>
                                        <option value="ST" <?php echo e($student->caste =='ST'? 'selected': ''); ?>>ST</option>
                                        <option value="other" <?php echo e($student->caste =='other'? 'selected': ''); ?>>other</option>
                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('caste')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('caste')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="input-effect">
                                    <input class="primary-input read-only-input form-control<?php echo e($errors->has('sub_caste') ? ' is-invalid' : ''); ?>" type="text" name="sub_caste" value="<?php echo e($student->sub_caste); ?>">
                                    <label>Sub-Caste <span>*</span></label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('sub_caste')): ?>
                                        <span class="invalid-feedback" role="alert">
									<strong><?php echo e($errors->first('sub_caste')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>


                        
                        <div class="row mb-40">
                            <div class="col-lg-6">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input" type="text" id="placeholderPhoto" value="<?php echo e($student->student_photo != ""? showPicName($student->student_photo):''); ?>" placeholder="<?php echo e($student->student_photo != ""? showPicName($student->student_photo):'Student Photo'); ?> "
                                                   readonly="">
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg" for="photo"><?php echo app('translator')->get('lang.browse'); ?></label>
                                            <input type="file" class="d-none" value="<?php echo e(old('photo')); ?>" name="photo" id="photo">
                                        </button>
                                    </div>
                                    <div class="col-lg-3 webcam">
                                        <div class="col-auto">
                                            <button class="primary-btn-small-input open" type="button">
                                                <label class="primary-btn small fix-gr-bg" for="webcam"><?php echo app('translator')->get('lang.web_cam'); ?></label>
                                            </button>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="results">Your captured image will appear here...</div>
                                <input type="hidden" name="webcam_photo" class="image-tag" value="">
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('student_group') ? ' is-invalid' : ''); ?>" name="student_group">
                                        <option data-display="Students Group *" value="">Students Group <span>*</span></option>
                                        <option value="Open" <?php echo e($student->student_group =='Open'? 'selected': ''); ?>>Open</option>
                                        <option value="RTE" <?php echo e($student->student_group =='RTE'? 'selected': ''); ?>>RTE</option>
                                        <option value="other" <?php echo e($student->student_group =='other'? 'selected': ''); ?>>other</option>
                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('student_group')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('student_group')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="last_school_attend" value="<?php echo e($student->last_school_attend); ?>">
                                    <label>Last School Attended</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="last_class_attend" value="<?php echo e($student->last_class_attend); ?>">
                                    <label>Last Class Attended</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="student_uid_no" value="<?php echo e($student->student_uid_no); ?>">
                                    <label>Student’s UID No</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="student_rfid_no" value="<?php echo e($student->student_rfid_no); ?>">
                                    <label>RFID No</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control<?php echo e($errors->has('student_email') ? ' is-invalid' : ''); ?>" type="email" name="student_email" value="<?php echo e($student->email); ?>">
                                    <label><?php echo app('translator')->get('lang.email'); ?> <?php echo app('translator')->get('lang.address'); ?> <span></span> </label>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('student_email')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('student_email')); ?></strong>
								</span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-2">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="student_height" value="<?php echo e($student->height); ?>">
                                    <label><?php echo app('translator')->get('lang.height'); ?> (<?php echo app('translator')->get('lang.in'); ?>) <span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="student_weight" value="<?php echo e($student->weight); ?>">
                                    <label><?php echo app('translator')->get('lang.Weight'); ?> (<?php echo app('translator')->get('lang.kg'); ?>) <span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="student_details_handicapped" value="<?php echo e($student->student_details_handicapped); ?>">
                                    <label>Details, If Handicapped</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="student_medical_issue" value="<?php echo e($student->student_medical_issue); ?>">
                                    <label>Details, If Any Medical Issue</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                        </div>

                        
                        <div class="parent_details" id="parent_details">
                            <div class="row mt-40">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">Parent Details</h4>
                                    </div>
                                </div>
                            </div>

                            
                            <div class="row mb-40 mt-30">
                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('fathers_name') ? ' is-invalid' : ''); ?>" type="text" name="fathers_name" id="fathers_name" value="<?php echo e($student->fathers_name); ?>">
                                        <label>Father Name <span>*</span> </label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('fathers_name')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('fathers_name')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('fathers_occupation') ? ' is-invalid' : ''); ?>" name="fathers_occupation">
                                            <option data-display="Father’s Occupation *" value="">Father’s Occupation *</option>
                                            <option value="self-employed" <?php echo e($student->fathers_occupation =='self-employed'? 'selected': ''); ?>>Self Employed</option>
                                            <option value="private-service" <?php echo e($student->fathers_occupation =='private-service'? 'selected': ''); ?>>Private Service</option>
                                            <option value="government-service" <?php echo e($student->fathers_occupation =='government-service'? 'selected': ''); ?>>Government Service</option>
                                            <option value="other" <?php echo e($student->fathers_occupation =='other'? 'selected': ''); ?>>other</option>
                                        </select>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('fathers_occupation')): ?>
                                            <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('fathers_occupation')); ?></strong>
								</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('fathers_email_id') ? ' is-invalid' : ''); ?>" type="email" name="fathers_email_id" value="<?php echo e($student->fathers_email_id); ?>">
                                        <label>Father’s Email Id <span>*</span> </label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('fathers_email_id')): ?>
                                            <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('fathers_email_id')); ?></strong>
								</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>


                            <div class="row mb-30">
                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('fathers_educations') ? ' is-invalid' : ''); ?>" name="fathers_educations">
                                            <option data-display="Educations *" value="">Educations *</option>
                                            <option value="10thssc" <?php echo e($student->fathers_educations =='10thssc'? 'selected': ''); ?>>10th/SSC</option>
                                            <option value="12hsc"  <?php echo e($student->fathers_educations =='12hsc'? 'selected': ''); ?>> 12th HSC</option>
                                            <option value="graducations" <?php echo e($student->fathers_educations =='graducations'? 'selected': ''); ?>>Graduation</option>
                                            <option value="PostGraduation" <?php echo e($student->fathers_educations =='PostGraduation'? 'selected': ''); ?>>Post Graduation</option>
                                            <option value="doctorate" <?php echo e($student->fathers_educations =='doctorate'? 'selected': ''); ?>>Doctorate</option>
                                        </select>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('fathers_educations')): ?>
                                            <span class="invalid-feedback invalid-select" role="alert">
									    <strong><?php echo e($errors->first('fathers_educations')); ?></strong>
								    </span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input" type="text" name="fathers_anual_income" id="fathers_anual_income" value="<?php echo e($student->fathers_anual_income); ?>">
                                        <label>Annual Income (INR)</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('fathers_office_no') ? ' is-invalid' : ''); ?>"  type="number" name="fathers_office_no" id="fathers_office_no" value="<?php echo e($student->fathers_office_no); ?>">
                                        <label>Father’s Office No</label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('fathers_office_no')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('fathers_office_no')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-30">
                                <div class="col-lg-8">
                                    <div class="input-effect">
                                        <textarea class="primary-input form-control" cols="0" rows="3" name="fathers_office_address" id="fathers_office_address"><?php echo e($student->fathers_office_address); ?></textarea>
                                        <label>Father’s Office Address</label>
                                        <span class="focus-border textarea"></span>
                                        <?php if($errors->has('fathers_office_address')): ?>
                                            <span class="invalid-feedback">
                                    <strong><?php echo e($errors->first('fathers_office_address')); ?></strong>
                                </span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-4" style="margin-top: 30px;">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('fathers_contact_no') ? ' is-invalid' : ''); ?>" type="number" name="fathers_contact_no" id="fathers_contact_no" value="<?php echo e($student->fathers_contact_no); ?>">
                                        <label>Contact No<span>*</span> </label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('fathers_contact_no')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('fathers_contact_no')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            

                            
                            <div class="row mb-40 mt-30">

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('mothers_name') ? ' is-invalid' : ''); ?>" type="text" name="mothers_name" id="mothers_name" value="<?php echo e($student->mothers_name); ?>">
                                        <label>Mother's Name <span>*</span><span></span> </label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('mothers_name')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('mothers_name')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>


                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('mothers_occupation') ? ' is-invalid' : ''); ?>" name="mothers_occupation">
                                            <option data-display="Mother’s Occupation *" value="">Mother’s Occupation *</option>
                                            <option value="housewife" <?php echo e($student->mothers_occupation =='housewife'? 'selected': ''); ?>>Housewife</option>
                                            <option value="self-employed" <?php echo e($student->mothers_occupation =='self-employed'? 'selected': ''); ?>>Self Employed</option>
                                            <option value="private-service" <?php echo e($student->mothers_occupation =='private-service'? 'selected': ''); ?>>Private Service</option>
                                            <option value="government-service" <?php echo e($student->mothers_occupation =='government-service'? 'selected': ''); ?>>Government Service</option>
                                            <option value="other" <?php echo e($student->mothers_occupation =='other'? 'selected': ''); ?>>other</option>
                                        </select>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('mothers_occupation')): ?>
                                            <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('mothers_occupation')); ?></strong>
								</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('mothers_email_id') ? ' is-invalid' : ''); ?>" type="email" name="mothers_email_id" value="<?php echo e($student->mothers_email_id); ?>">
                                        <label>Mother’s Email Id <span>*</span></label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('mothers_email_id')): ?>
                                            <span class="invalid-feedback invalid-select" role="alert">
									<strong><?php echo e($errors->first('mothers_email_id')); ?></strong>
								</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>


                            <div class="row mb-30">

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('mothers_educations') ? ' is-invalid' : ''); ?>" name="mothers_educations">
                                            <option data-display="Educations *" value="">Educations *</option>
                                            <option value="10thssc" <?php echo e($student->mothers_educations =='10thssc'? 'selected': ''); ?>>10th/SSC</option>
                                            <option value="12hsc" <?php echo e($student->mothers_educations =='12hsc'? 'selected': ''); ?>> 12th HSC</option>
                                            <option value="graducations" <?php echo e($student->mothers_educations =='graducations'? 'selected': ''); ?>>Graduation</option>
                                            <option value="PostGraduation" <?php echo e($student->mothers_educations =='PostGraduation'? 'selected': ''); ?>>Post Graduation</option>
                                            <option value="doctorate" <?php echo e($student->mothers_educations =='doctorate'? 'selected': ''); ?>>Doctorate</option>
                                        </select>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('mothers_educations')): ?>
                                            <span class="invalid-feedback invalid-select" role="alert">
									    <strong><?php echo e($errors->first('mothers_educations')); ?></strong>
								    </span>
                                        <?php endif; ?>
                                    </div>
                                </div>


                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input" type="text" name="mothers_anual_income" id="mothers_anual_income" value="<?php echo e($student->mothers_anual_income); ?>">
                                        <label>Annual Income (INR)</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('mothers_office_no') ? ' is-invalid' : ''); ?>" type="number" name="mothers_office_no" id="mothers_office_no" value="<?php echo e($student->mothers_office_no); ?>">
                                        <label>Mother’s Office No <span></span> </label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('mothers_office_no')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('mothers_office_no')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-30">
                                <div class="col-lg-8">
                                    <div class="input-effect">
                                        <textarea class="primary-input form-control" cols="0" rows="3" name="mothers_office_address" id="mothers_office_address"><?php echo e($student->mothers_office_address); ?></textarea>
                                        <label>Mother’s Office Address <span></span> </label>
                                        <span class="focus-border textarea"></span>
                                        <?php if($errors->has('mothers_office_address')): ?>
                                            <span class="invalid-feedback">
                                    <strong><?php echo e($errors->first('mothers_office_address')); ?></strong>
                                </span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-4" style="margin-top: 30px;">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('mothers_contact_no') ? ' is-invalid' : ''); ?>" type="number" name="mothers_contact_no" id="mothers_contact_no" value="<?php echo e($student->mothers_contact_no); ?>">
                                        <label>Contact No<span>*</span> </label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('mothers_contact_no')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('mothers_contact_no')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            


                            <div class="row mb-30">
                                <div class="col-lg-12">
                                    <div class="input-effect">
                                        <textarea class="primary-input form-control" cols="0" rows="3" name="recedentials_address" id="recedentials_address"><?php echo e($student->recedentials_address); ?></textarea>
                                        <label>Residentials Address <span>*</span> </label>
                                        <span class="focus-border textarea"></span>
                                        <?php if($errors->has('recedentials_address')): ?>
                                            <span class="invalid-feedback">
										<strong><?php echo e($errors->first('recedentials_address')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-30">

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('city') ? ' is-invalid' : ''); ?>" type="text" name="city" id="city" value="<?php echo e($student->city); ?>">
                                        <label>City <span>*</span> </label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('city')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('city')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('state') ? ' is-invalid' : ''); ?>" type="text" name="state" id="state" value="<?php echo e($student->state); ?>">
                                        <label>State <span>*</span> </label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('state')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('state')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect">
                                        <input class="primary-input form-control<?php echo e($errors->has('district') ? ' is-invalid' : ''); ?>" type="text" name="district" id="district" value="<?php echo e($student->district); ?>">
                                        <label>District <span>*</span> </label>
                                        <span class="focus-border"></span>
                                        <?php if($errors->has('district')): ?>
                                            <span class="invalid-feedback" role="alert">
										<strong><?php echo e($errors->first('district')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>


                            <div class="row mb-30">
                                <div class="col-lg-12">
                                    <div class="input-effect">
                                        <textarea class="primary-input form-control" cols="0" rows="3" name="permanent_address" id="permanent_address"><?php echo e($student->permanent_address); ?></textarea>
                                        <label>Permanent Address <span id="validationsColorStar">*</span> </label>
                                        <span class="focus-border textarea"></span>
                                        <?php if($errors->has('permanent_address')): ?>
                                            <span class="invalid-feedback">
										<strong><?php echo e($errors->first('permanent_address')); ?></strong>
									</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row mb-30">
                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('trnasport') ? ' is-invalid' : ''); ?>" name="trnasport" id="trnasport">
                                        <option data-display="Transport *" value="">Transport *</option>
                                        <option value="yes" <?php echo e($student->trnasport =='yes'? 'selected': ''); ?>>Yes</option>
                                        <option value="no" <?php echo e($student->trnasport =='no'? 'selected': ''); ?>> No</option>
                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('trnasport')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
									    <strong><?php echo e($errors->first('trnasport')); ?></strong>
								    </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <?php
                            $className='disbaledItem';
                            if ($student->trnasport=='yes'){
                                $className='';
                            }
                            ?>
                            <div class="col-lg-4 <?=$className?>" id="<?=$className?>">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="route" id="route" value="<?php echo e($student->route); ?>">
                                    <label>Route No</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4 <?=$className?>" id="<?=$className?>">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="bus_no" id="bus_no" value="<?php echo e($student->bus_no); ?>">
                                    <label>Bus No</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                        </div>

                        <div class="row mt-40">
                            <div class="col-lg-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">Upload Documents</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40 mt-30">
                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="document_title_1" value="<?php echo e($student->document_title_1); ?>">
                                    <label>Birth Certificate</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="document_title_2" value="<?php echo e($student->document_title_2); ?>">
                                    <label>Transfer Certificate</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="document_title_3" value="<?php echo e($student->document_title_3); ?>">
                                    <label>Mark Sheet/Report Card</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="input-effect">
                                    <input class="primary-input" type="text" name="document_title_4" value="<?php echo e($student->document_title_4); ?>">
                                    <label>UID</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-30">
                            <div class="col-lg-3">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input" type="text" id="placeholderFileOneName" placeholder="<?php echo e($student->document_file_1 != ""? showPicNameDoc($student->document_file_1):'01'); ?>"
                                                   disabled>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg" for="document_file_1"><?php echo app('translator')->get('lang.browse'); ?></label>
                                            <input type="file" class="d-none" name="document_file_1" id="document_file_1" value="<?php echo e($student->document_file_1 != ""? showPicName($student->document_file_1):''); ?>">
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input" type="text" id="placeholderFileTwoName" placeholder="<?php echo e(isset($student->document_file_2) && $student->document_file_2 != ""? showPicNameDoc($student->document_file_2):'02'); ?>"
                                                   disabled>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg" for="document_file_2"><?php echo app('translator')->get('lang.browse'); ?></label>
                                            <input type="file" class="d-none" name="document_file_2" id="document_file_2" value="<?php echo e($student->document_file_2 != ""? showPicName($student->document_file_2):''); ?>" >
                                        </button>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input" type="text" id="placeholderFileThreeName" placeholder="<?php echo e(isset($student->document_file_3) && $student->document_file_3 != ""? showPicNameDoc($student->document_file_3):'03'); ?>"
                                                   disabled>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg" for="document_file_3"><?php echo app('translator')->get('lang.browse'); ?></label>
                                            <input type="file" class="d-none" name="document_file_3" id="document_file_3" value="<?php echo e($student->document_file_3 != ""? showPicName($student->document_file_3):''); ?>">
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input" type="text" id="placeholderFileFourName" placeholder="<?php echo e(isset($student->document_file_4) && $student->document_file_4 != ""? showPicNameDoc($student->document_file_4):'04'); ?>"
                                                   disabled>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg" for="document_file_4"><?php echo app('translator')->get('lang.browse'); ?></label>
                                            <input type="file" class="d-none" name="document_file_4" id="document_file_4" value="<?php echo e($student->document_file_4 != ""? showPicName($student->document_file_4):''); ?>">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                            $tooltip = "";
                            if(in_array(65, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ){
                            $tooltip = "";
                            }else{
                            $tooltip = "You have no permission to add";
                            }
                        ?>


                        <div class="row mt-40">
                            <div class="col-lg-12 text-center">
                                <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="<?php echo e($tooltip); ?>">
                                    <span class="ti-check"></span>
                                    <?php echo app('translator')->get('lang.save'); ?> <?php echo app('translator')->get('lang.admission_form'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo e(Form::close()); ?>

        </div>
    </section>
    
    <input type="text" id="STurl" value="<?php echo e(route('student_admission_pic')); ?>" hidden>
    <div class="modal" id="LogoPic">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Crop Image And Upload</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div id="resize"></div>
                    <button class="btn rotate float-lef" data-deg="90" >
                        <i class="ti-back-right"></i></button>
                    <button class="btn rotate float-right" data-deg="-90" >
                        <i class="ti-back-left"></i></button>
                    <hr>

                    <button class="primary-btn fix-gr-bg pull-right" id="upload_logo">Crop</button>
                </div>
            </div>
        </div>
    </div>
    

    

    <div class="modal" id="FatherPic">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Crop Image And Upload</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div id="fa_resize"></div>
                    <button class="btn rotate float-lef" data-deg="90" >
                        <i class="ti-back-right"></i></button>
                    <button class="btn rotate float-right" data-deg="-90" >
                        <i class="ti-back-left"></i></button>
                    <hr>

                    <button class="primary-btn fix-gr-bg pull-right" id="FatherPic_logo">Crop</button>
                </div>
            </div>
        </div>
    </div>
    
    

    <div class="modal" id="MotherPic">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Crop Image And Upload</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div id="ma_resize"></div>
                    <button class="btn rotate float-lef" data-deg="90" >
                        <i class="ti-back-right"></i></button>
                    <button class="btn rotate float-right" data-deg="-90" >
                        <i class="ti-back-left"></i></button>
                    <hr>

                    <button class="primary-btn fix-gr-bg pull-right" id="Mother_logo">Crop</button>
                </div>
            </div>
        </div>
    </div>
    
    

    <div class="modal" id="GurdianPic">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Crop Image And Upload</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div id="Gu_resize"></div>
                    <button class="btn rotate float-lef" data-deg="90" >
                        <i class="ti-back-right"></i></button>
                    <button class="btn rotate float-right" data-deg="-90" >
                        <i class="ti-back-left"></i></button>
                    <hr>

                    <button class="primary-btn fix-gr-bg pull-right" id="Gurdian_logo">Crop</button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="pop-outer" style="display:none">
        <div class="pop-inner">
            <div id="my_camera"></div>
            <span class="buttonwebcam">
			<input class="buttonsnap" type=button value="Take Snapshot" onClick="take_snapshot()">
			<input type=button class="closebutton" value="Close">

		</span>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(asset('public/backEnd/')); ?>/js/croppie.js"></script>
    <script src="<?php echo e(asset('public/backEnd/')); ?>/js/st_addmision.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <script>
        $(document).ready(function (){
            $(".open").click(function (){
                Webcam.set({
                    width: 414,
                    height: 205,
                    image_format: 'jpeg',
                    jpeg_quality: 90
                });
                Webcam.attach( '#my_camera' );
                $(".pop-outer").show();
            });

            $(".closebutton").click(function (){
                Webcam.reset()
                $(".pop-outer").hide();
            });
        });

        var data_uri = '/student-store';
        function take_snapshot() {
            $(".pop-outer").hide();
            Webcam.snap( function(data_uri) {
                $(".image-tag").val(data_uri);
                document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
            } );
            Webcam.reset()
        }

        $('div#disbaledItem').css("display", "none");

        $(document).on('change', '#trnasport', function() {
            var value=$(this).val();
            if (value=='yes'){
                $('div#disbaledItem').css("display", "block");
            }else {
                $('div#disbaledItem').css("display", "none");
            }
        });

        $(document).on('keypress','#fathers_office_no, #fathers_contact_no , #mothers_office_no , #mothers_contact_no',function(e){
            if($(e.target).prop('value').length>=12){
                if(e.keyCode!=32)
                {return false}
            }});


        function getAge(dateString) {
            var now = new Date();
            var today = new Date(now.getYear(),now.getMonth(),now.getDate());

            var yearNow = now.getYear();
            var monthNow = now.getMonth();
            var dateNow = now.getDate();

            var dob = new Date(dateString.substring(6,10),
                dateString.substring(0,2)-1,
                dateString.substring(3,5)
            );

            var yearDob = dob.getYear();
            var monthDob = dob.getMonth();
            var dateDob = dob.getDate();
            var age = {};
            var ageString = "";
            var yearString = "";
            var monthString = "";
            var dayString = "";


            yearAge = yearNow - yearDob;

            if (monthNow >= monthDob)
                var monthAge = monthNow - monthDob;
            else {
                yearAge--;
                var monthAge = 12 + monthNow -monthDob;
            }

            if (dateNow >= dateDob)
                var dateAge = dateNow - dateDob;
            else {
                monthAge--;
                var dateAge = 31 + dateNow - dateDob;

                if (monthAge < 0) {
                    monthAge = 11;
                    yearAge--;
                }
            }

            age = {
                years: yearAge,
                months: monthAge,
                days: dateAge
            };

            if ( age.years > 1 ) yearString = " years";
            else yearString = " year";
            if ( age.months> 1 ) monthString = " months";
            else monthString = " month";
            if ( age.days > 1 ) dayString = " days";
            else dayString = " day";


            if ( (age.years > 0) && (age.months > 0) && (age.days > 0) )
                ageString = age.years + yearString + ", " + age.months + monthString + ", and " + age.days + dayString + " old.";
            else if ( (age.years == 0) && (age.months == 0) && (age.days > 0) )
                ageString = "Only " + age.days + dayString + " old!";
            else if ( (age.years > 0) && (age.months == 0) && (age.days == 0) )
                ageString = age.years + yearString + " old. Happy Birthday!!";
            else if ( (age.years > 0) && (age.months > 0) && (age.days == 0) )
                ageString = age.years + yearString + " and " + age.months + monthString + " old.";
            else if ( (age.years == 0) && (age.months > 0) && (age.days > 0) )
                ageString = age.months + monthString + " and " + age.days + dayString + " old.";
            else if ( (age.years > 0) && (age.months == 0) && (age.days > 0) )
                ageString = age.years + yearString + " and " + age.days + dayString + " old.";
            else if ( (age.years == 0) && (age.months > 0) && (age.days == 0) )
                ageString = age.months + monthString + " old.";
            else ageString = "Oops! Could not calculate age!";

            return ageString;
        }


        //mm/dd/year
        $("#date_of_birth").change(function(){
            var dataItem=$(this).val();
            var TodayDate = new Date();
            var endDate= new Date(Date.parse(dataItem));

            if (endDate> TodayDate) {
                alert("Should be less than today date");
                $('#ageCalculated').val(0);
                $('#ageCalculated').addClass('has-content');
                $(this).val('');
            }

            var ageCalculated =getAge($('#date_of_birth').val());

            $('#ageCalculated').val(ageCalculated);
            $('#ageCalculated').addClass('has-content');
        });

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\schollmanagementv5\resources\views/backEnd/Admissions/student_admission_form_edit.blade.php ENDPATH**/ ?>