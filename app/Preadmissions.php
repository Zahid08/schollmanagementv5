<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preadmissions extends Model
{
    public function className()
    {
        return $this->belongsTo('App\SmClass', 'admited_to_class', 'id');
    }
    public function gender()
    {
        return $this->belongsTo('App\SmBaseSetup', 'gender_id', 'id');
    }
}
