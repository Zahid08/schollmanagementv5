<?php

namespace App\Http\Controllers;

use App\Preadmissions;
use Illuminate\Http\Request;
use App\Admissionform;
use App\SmStudent;
use App\SmClass;
use App\SmBaseSetup;
use App\SmSession;
use Illuminate\Support\Facades\DB;
use App\User;
use App\SmGeneralSettings;
use App\SmUserLog;
use App\InfixModuleManager;
use App\SmParent;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Hash;
use App\SmRoute;
use Illuminate\Support\Facades\Session;
use App\SmVehicle;
use Illuminate\Support\Facades\Auth;
use App\SmFeesAssign;
use App\SmFeesAssignDiscount;
use App\SmStudentDocument;
use App\SmStudentTimeline;
use App\SmAcademicYear;
use App\YearCheck;
use App\SmFeesMaster;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class PreadmissionsController extends Controller
{
    private $User;
    private $SmGeneralSettings;
    private $SmUserLog;
    private $InfixModuleManager;
    private $URL;

    public function __construct()
    {
        $this->middleware('PM');
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->User                 = json_encode(User::find(1));
        $this->SmGeneralSettings    = json_encode(SmGeneralSettings::find(1));
        $this->SmUserLog            = json_encode(SmUserLog::find(1));
        $this->InfixModuleManager   = json_encode(InfixModuleManager::find(1));
        $this->URL                  = url('/');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feesList = SmFeesMaster::where('active_status', '=', '1')->get();
        $max_admission_id = Preadmissions::max('admission_number');

        $classes = SmClass::where('active_status', '=', '1')->get();
        $genders = SmBaseSetup::where('active_status', '=', '1')->where('base_group_id', '=', '1')->get();
        $sessions = SmSession::where('active_status', '=', '1')->get();

        return view('backEnd.preadmissions.student_pre_admisisons_form', compact('classes', 'genders', 'sessions', 'max_admission_id'));
    }


    //Store Pre Admission Informations
    public function studentPreAdmissionStore(Request $request)
    {
        $request->validate([
            'date' => 'required',
            'session' => 'required',
            'student_name' => 'required',
            'fathers_name' => 'required',
            'mothers_name' => 'required',
            'contact_no_1' => 'required',
            'student_email' => 'required',
            'date_of_birth' => 'required',
            'age' => 'required',
            'admited_to_class' => 'required',
            'gender_id' => 'required',
            'no_of_sibling' => 'required',
            'permanent_address' => 'required',
            'pin_code' => 'required',
            'fathers_occupation' => 'required',
            'mothers_occupation' => 'required',
            'full_name' => 'required'
        ]);

        DB::beginTransaction();

        try {
            if ($_REQUEST['pre_admission']=='Save'){
                $studentPreAdmissionForm = new Preadmissions();
                $studentPreAdmissionForm->admission_number              =!empty($request->admission_number)?$request->admission_number:'';
                $studentPreAdmissionForm->date                          = date('Y-m-d', strtotime($request->date));
                $studentPreAdmissionForm->session                       = !empty($request->session)?$request->session:'';
                $studentPreAdmissionForm->student_name                  = !empty($request->student_name)?$request->student_name:'';
                $studentPreAdmissionForm->fathers_name                  = !empty($request->fathers_name)?$request->fathers_name:'';
                $studentPreAdmissionForm->mothers_name                  = !empty($request->mothers_name)?$request->mothers_name:'';
                $studentPreAdmissionForm->surname                       = !empty($request->surname)?$request->surname:'';
                $studentPreAdmissionForm->contact_no_1                  = !empty($request->contact_no_1)?$request->contact_no_1:'';
                $studentPreAdmissionForm->contact_no_2                  = !empty($request->contact_no_2)?$request->contact_no_2:'';
                $studentPreAdmissionForm->student_email                 = !empty($request->student_email)?$request->student_email:'';
                $studentPreAdmissionForm->date_of_birth                 = date('Y-m-d', strtotime($request->date_of_birth));
                $studentPreAdmissionForm->age                           = !empty($request->age)?$request->age:'';
                $studentPreAdmissionForm->admited_to_class              =!empty($request->admited_to_class)?$request->admited_to_class:'';
                $studentPreAdmissionForm->gender_id                     = !empty($request->gender_id)?$request->gender_id:'';
                $studentPreAdmissionForm->no_of_sibling                 = !empty($request->no_of_sibling)?$request->no_of_sibling:'';
                $studentPreAdmissionForm->permanent_address             = !empty($request->permanent_address)?$request->permanent_address:'';
                $studentPreAdmissionForm->pin_code                      = !empty($request->pin_code)?$request->pin_code:'';
                $studentPreAdmissionForm->fathers_occupation            = !empty($request->fathers_occupation)?$request->fathers_occupation:'';
                $studentPreAdmissionForm->mothers_occupation            = !empty($request->mothers_occupation)?$request->mothers_occupation:'';
                $studentPreAdmissionForm->reference                     = !empty($request->reference)?$request->reference:'';
                $studentPreAdmissionForm->source                        = !empty($request->source)?$request->source:'';
                $studentPreAdmissionForm->previous_school_details       = !empty($request->previous_school_details)?$request->previous_school_details:'';
                $studentPreAdmissionForm->descriptions                  = !empty($request->descriptions)?$request->descriptions:'';
                $studentPreAdmissionForm->full_name                     = !empty($request->full_name)?$request->full_name:'';

                $photo = '';
                if(@$request->webcam_photo){
                    $data = str_replace('data:image/jpeg;base64,', '', $request->webcam_photo);

                    $path = public_path('uploads/student/');
                    $data = base64_decode($data);

                    $fileName = 'stu-'. time() . uniqid() . '.jpeg';
                    $filePath = $path . $fileName;
                    file_put_contents($filePath, $data);
                    $photo = 'public/uploads/student/'.$fileName;
                }

                if(@$photo){
                    $studentPreAdmissionForm->student_photo = $photo;
                }
                else {
                    $studentPreAdmissionForm->student_photo = Session::get('student_photo');
                }

                $studentPreAdmissionForm->save();
                $studentPreAdmissionForm->toArray();
                DB::commit();

                Toastr::success('Operation successful', 'Success');
                return redirect('student-pre-admission-form-list');
            }elseif ($_REQUEST['pre_admission']=='Admission Confirmed'){
                $studentAdmissionForm = new SmStudent();
                $studentAdmissionForm->admission_number              =!empty($request->admission_number)?$request->admission_number:'';
                $studentAdmissionForm->admission_date               = date('Y-m-d', strtotime($request->date));
                $studentAdmissionForm->session                       = !empty($request->session)?$request->session:'';
                $studentAdmissionForm->fathers_name                  = !empty($request->fathers_name)?$request->fathers_name:'';
                $studentAdmissionForm->mothers_name                  = !empty($request->mothers_name)?$request->mothers_name:'';
                $studentAdmissionForm->fathers_contact_no                  = !empty($request->contact_no_1)?$request->contact_no_1:'';
                $studentAdmissionForm->mothers_contact_no                  = !empty($request->contact_no_2)?$request->contact_no_2:'';
                $studentAdmissionForm->student_email                 = !empty($request->student_email)?$request->student_email:'';
                $studentAdmissionForm->date_of_birth                 = date('Y-m-d', strtotime($request->date_of_birth));
                $studentAdmissionForm->age                           = !empty($request->age)?$request->age:'';
                $studentAdmissionForm->admited_to_class              =!empty($request->admited_to_class)?$request->admited_to_class:'';
                $studentAdmissionForm->gender_id                     = !empty($request->gender_id)?$request->gender_id:'';
                $studentAdmissionForm->no_of_sibling                 = !empty($request->no_of_sibling)?$request->no_of_sibling:'';
                $studentAdmissionForm->permanent_address             = !empty($request->permanent_address)?$request->permanent_address:'';
                $studentAdmissionForm->fathers_occupation            = !empty($request->fathers_occupation)?$request->fathers_occupation:'';
                $studentAdmissionForm->mothers_occupation            = !empty($request->mothers_occupation)?$request->mothers_occupation:'';
                $studentAdmissionForm->full_name                     = !empty($request->full_name)?$request->full_name:'';

                $photo = '';
                if(@$request->webcam_photo){
                    $data = str_replace('data:image/jpeg;base64,', '', $request->webcam_photo);

                    $path = public_path('uploads/student/');
                    $data = base64_decode($data);

                    $fileName = 'stu-'. time() . uniqid() . '.jpeg';
                    $filePath = $path . $fileName;
                    file_put_contents($filePath, $data);
                    $photo = 'public/uploads/student/'.$fileName;
                }

                if(@$photo){
                    $studentAdmissionForm->student_photo = $photo;
                }
                else {
                    $studentAdmissionForm->student_photo = Session::get('student_photo');
                }

                $studentAdmissionForm->save();
                $studentAdmissionForm->toArray();

                DB::commit();

                Toastr::success('Operation successful', 'Success');
                return redirect('student-admission-form-list');

            }else{
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            DB::rollback();
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function studentAdmissionFormDetails(Request $request)
    {
        $studentsPreAdmissionForm = Preadmissions::where('active_status', 1)->get();
        $classes = SmClass::where('active_status', 1)->get();

        return view('backEnd.preadmissions.student_pre_admision_details', compact('studentsPreAdmissionForm', 'classes'));
    }


    public function studentPreAdmissionFormView(Request $request, $id)
    {
        try {
            $student_detail = Preadmissions::find($id);
            return view('backEnd.preadmissions.student_pre_admission_form_view', compact('student_detail'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function studentPreAdmissionFormEdit(Request $request, $id)
    {

        try {
            $student = Preadmissions::find($id);
            $classes = SmClass::where('active_status', '=', '1')->where('academic_id', YearCheck::getAcademicId())->where('school_id', Auth::user()->school_id)->get();
            $genders = SmBaseSetup::where('active_status', '=', '1')->where('base_group_id', '=', '1')->get();;
            $sessions = SmAcademicYear::where('active_status', '=', '1')->where('school_id', Auth::user()->school_id)->get();
            return view('backEnd.preadmissions.student_pre_admission_form_edit', compact('student', 'classes', 'genders', 'sessions'));

        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function studentPreAdmissionFormUpdate(Request $request)
    {

        $request->validate([
            'date' => 'required',
            'session' => 'required',
            'student_name' => 'required',
            'fathers_name' => 'required',
            'mothers_name' => 'required',
            'contact_no_1' => 'required',
            'student_email' => 'required',
            'date_of_birth' => 'required',
            'age' => 'required',
            'admited_to_class' => 'required',
            'gender_id' => 'required',
            'no_of_sibling' => 'required',
            'permanent_address' => 'required',
            'pin_code' => 'required',
            'fathers_occupation' => 'required',
            'mothers_occupation' => 'required',
            'full_name' => 'required'
        ]);

        // always happen start
        DB::beginTransaction();

        try {
            if ($_REQUEST['pre_admission']=='Update') {

                $studentPreAdmissionForm = Preadmissions::find($request->id);

                $studentPreAdmissionForm->admission_number = !empty($request->admission_number) ? $request->admission_number : '';
                $studentPreAdmissionForm->date = date('Y-m-d', strtotime($request->date));
                $studentPreAdmissionForm->session = !empty($request->session) ? $request->session : '';
                $studentPreAdmissionForm->student_name = !empty($request->student_name) ? $request->student_name : '';
                $studentPreAdmissionForm->fathers_name = !empty($request->fathers_name) ? $request->fathers_name : '';
                $studentPreAdmissionForm->mothers_name = !empty($request->mothers_name) ? $request->mothers_name : '';
                $studentPreAdmissionForm->surname = !empty($request->surname) ? $request->surname : '';
                $studentPreAdmissionForm->contact_no_1 = !empty($request->contact_no_1) ? $request->contact_no_1 : '';
                $studentPreAdmissionForm->contact_no_2 = !empty($request->contact_no_2) ? $request->contact_no_2 : '';
                $studentPreAdmissionForm->student_email = !empty($request->student_email) ? $request->student_email : '';
                $studentPreAdmissionForm->date_of_birth = date('Y-m-d', strtotime($request->date_of_birth));
                $studentPreAdmissionForm->age = !empty($request->age) ? $request->age : '';
                $studentPreAdmissionForm->admited_to_class = !empty($request->admited_to_class) ? $request->admited_to_class : '';
                $studentPreAdmissionForm->gender_id = !empty($request->gender_id) ? $request->gender_id : '';
                $studentPreAdmissionForm->no_of_sibling = !empty($request->no_of_sibling) ? $request->no_of_sibling : '';
                $studentPreAdmissionForm->permanent_address = !empty($request->permanent_address) ? $request->permanent_address : '';
                $studentPreAdmissionForm->pin_code = !empty($request->pin_code) ? $request->pin_code : '';
                $studentPreAdmissionForm->fathers_occupation = !empty($request->fathers_occupation) ? $request->fathers_occupation : '';
                $studentPreAdmissionForm->mothers_occupation = !empty($request->mothers_occupation) ? $request->mothers_occupation : '';
                $studentPreAdmissionForm->reference = !empty($request->reference) ? $request->reference : '';
                $studentPreAdmissionForm->source = !empty($request->source) ? $request->source : '';
                $studentPreAdmissionForm->previous_school_details = !empty($request->previous_school_details) ? $request->previous_school_details : '';
                $studentPreAdmissionForm->descriptions = !empty($request->descriptions) ? $request->descriptions : '';
                $studentPreAdmissionForm->full_name = !empty($request->full_name) ? $request->full_name : '';

                $photo = '';
                if (@$request->webcam_photo) {
                    $data = str_replace('data:image/jpeg;base64,', '', $request->webcam_photo);

                    $path = public_path('uploads/student/');
                    $data = base64_decode($data);

                    $fileName = 'stu-' . time() . uniqid() . '.jpeg';
                    $filePath = $path . $fileName;
                    file_put_contents($filePath, $data);
                    $photo = 'public/uploads/student/' . $fileName;
                }

                if (@$photo) {
                    $studentPreAdmissionForm->student_photo = $photo;
                } else {
                    $studentPreAdmissionForm->student_photo = Session::get('student_photo');
                }

                $studentPreAdmissionForm->save();

                DB::commit();
                Toastr::success('Operation successful', 'Success');
                return redirect('student-pre-admission-form-list');
            }elseif ($_REQUEST['pre_admission']=='Admission Confirmed'){

                $studentAdmissionForm = new SmStudent();
                $studentAdmissionForm->admission_no                     =!empty($request->admission_number)?$request->admission_number:'';
                $studentAdmissionForm->admission_date                   = date('Y-m-d', strtotime($request->date));
                $studentAdmissionForm->session_id                       = !empty($request->session)?$request->session:'';
                $studentAdmissionForm->fathers_name                     = !empty($request->fathers_name)?$request->fathers_name:'';
                $studentAdmissionForm->mothers_name                     = !empty($request->mothers_name)?$request->mothers_name:'';
                $studentAdmissionForm->fathers_contact_no               = !empty($request->contact_no_1)?$request->contact_no_1:'';
                $studentAdmissionForm->mothers_contact_no               = !empty($request->contact_no_2)?$request->contact_no_2:'';
                $studentAdmissionForm->email                            = !empty($request->student_email)?$request->student_email:'';
                $studentAdmissionForm->date_of_birth                    = date('Y-m-d', strtotime($request->date_of_birth));
                $studentAdmissionForm->age                              = !empty($request->age)?$request->age:'';
                $studentAdmissionForm->class_id                         =!empty($request->admited_to_class)?$request->admited_to_class:'';
                $studentAdmissionForm->gender_id                        = !empty($request->gender_id)?$request->gender_id:'';
                $studentAdmissionForm->no_of_sibling                    = !empty($request->no_of_sibling)?$request->no_of_sibling:'';
                $studentAdmissionForm->permanent_address                = !empty($request->permanent_address)?$request->permanent_address:'';
                $studentAdmissionForm->fathers_occupation               = !empty($request->fathers_occupation)?$request->fathers_occupation:'';
                $studentAdmissionForm->mothers_occupation               = !empty($request->mothers_occupation)?$request->mothers_occupation:'';
                $studentAdmissionForm->full_name                        = !empty($request->full_name)?$request->full_name:'';
                $studentAdmissionForm->first_name                       = !empty($request->full_name)?$request->full_name:'';

                $photo = '';
                if(@$request->webcam_photo){
                    $data = str_replace('data:image/jpeg;base64,', '', $request->webcam_photo);

                    $path = public_path('uploads/student/');
                    $data = base64_decode($data);

                    $fileName = 'stu-'. time() . uniqid() . '.jpeg';
                    $filePath = $path . $fileName;
                    file_put_contents($filePath, $data);
                    $photo = 'public/uploads/student/'.$fileName;
                }

                if(@$photo){
                    $studentAdmissionForm->student_photo = $photo;
                }
                else {
                    $studentAdmissionForm->student_photo = Session::get('student_photo');
                }

                $studentAdmissionForm->save();
                $studentAdmissionForm->toArray();

                DB::commit();

                Toastr::success('Operation successful', 'Success');
                return redirect('student-admission-form-list');

            }else{
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            DB::rollback();
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function studentPreAdmissionFormDelete(Request $request)
    {

        $student_detail = Preadmissions::find($request->modalDeletedId);


        DB::beginTransaction();

        try {

            $student_detail->active_status = 0;
            $student_detail->save();

            DB::commit();
            Toastr::success('Operation successful', 'Success');

            return redirect('student-pre-admission-form-list');

        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    function preAdmissionPic(Request $r)
    {

        try {
            $validator = Validator::make($r->all(), [
                'logo_pic' => 'sometimes|required|mimes:jpg,png|max:40000',

            ]);
            if ($validator->fails()) {
                return response()->json(['error' => 'error'], 201);
            }
            $data = new SmStudent();
            $data_parent = new SmParent();
            if ($r->hasFile('logo_pic')) {
                $file = $r->file('logo_pic');
                $images = Image::make($file)->insert($file);
                $pathImage = 'public/uploads/student/';
                if (!file_exists($pathImage)) {
                    mkdir($pathImage, 0777, true);
                    $name = md5($file->getClientOriginalName() . time()) . "." . "png";
                    $images->save('public/uploads/student/' . $name);
                    $imageName = 'public/uploads/student/' . $name;
                    // $data->staff_photo =  $imageName;
                    Session::put('student_photo', $imageName);
                } else {
                    $name = md5($file->getClientOriginalName() . time()) . "." . "png";
                    if (file_exists(Session::get('student_photo'))) {
                        File::delete(Session::get('student_photo'));
                    }
                    $images->save('public/uploads/student/' . $name);
                    $imageName = 'public/uploads/student/' . $name;
                    // $data->student_photo =  $imageName;
                    Session::put('student_photo', $imageName);
                }
            }
            return response()->json('success', 200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'error'], 201);
        }
    }
}
