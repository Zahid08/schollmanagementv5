<?php

namespace App\Http\Controllers;

use App\Admissionform;
use Illuminate\Http\Request;
use App\SmStudent;
use App\SmClass;
use App\SmBaseSetup;
use App\SmSession;
use Illuminate\Support\Facades\DB;
use App\User;
use App\SmGeneralSettings;
use App\SmUserLog;
use App\InfixModuleManager;
use App\SmParent;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Hash;
use App\SmRoute;
use Illuminate\Support\Facades\Session;
use App\SmVehicle;
use Illuminate\Support\Facades\Auth;
use App\SmFeesAssign;
use App\SmFeesAssignDiscount;
use App\SmStudentDocument;
use App\SmStudentTimeline;
use App\SmAcademicYear;
use App\YearCheck;
use App\SmFeesMaster;
use App\SmNotification;

class AdmissionformController extends Controller
{
    private $User;
    private $SmGeneralSettings;
    private $SmUserLog;
    private $InfixModuleManager;
    private $URL;


    public function __construct()
    {
        $this->middleware('PM');
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->User                 = json_encode(User::find(1));
        $this->SmGeneralSettings    = json_encode(SmGeneralSettings::find(1));
        $this->SmUserLog            = json_encode(SmUserLog::find(1));
        $this->InfixModuleManager   = json_encode(InfixModuleManager::find(1));
        $this->URL                  = url('/');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feesList = SmFeesMaster::where('active_status', '=', '1')->get();
        $max_admission_id = SmStudent::max('admission_no');

        $max_roll_id = SmStudent::max('roll_no');

        $classes = SmClass::where('active_status', '=', '1')->get();
        $religions = SmBaseSetup::where('active_status', '=', '1')->where('base_group_id', '=', '2')->get();
        $blood_groups = SmBaseSetup::where('active_status', '=', '1')->where('base_group_id', '=', '3')->get();
        $genders = SmBaseSetup::where('active_status', '=', '1')->where('base_group_id', '=', '1')->get();
        $sessions = SmSession::where('active_status', '=', '1')->get();

        return view('backEnd.Admissions.student_admisisons_form', compact('classes', 'religions', 'blood_groups', 'genders', 'sessions', 'max_admission_id', 'max_roll_id','feesList'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function studentAdmissionStore(Request $request)
    {
        $request->validate([
            'session' => 'required',
            'full_name' => 'required',
            'admited_to_class' => 'required',
            'section' => 'required',
            'roll_no' => 'required',
            'admission_date' => 'required',
            'fees_package_id' => 'required',
            'date_of_birth' => 'required',
            'age' => 'required',
            'date_of_place' => 'required',
            'gender_id' => 'required',
            'no_of_sibling' => 'required',
            'bloodgroup_id' => 'required',
            'nationality' => 'required',
            'religion_id' => 'required',
            'caste' => 'required',
            'sub_caste' => 'required',
            'student_group' => 'required',
            'fathers_name' => 'required',
            'fathers_occupation' => 'required',
            'fathers_email_id' => "required|unique:sm_parents,guardians_email",
            'fathers_educations' => 'required',
            'fathers_contact_no' =>"required|unique:sm_parents,guardians_mobile",
            'mothers_name' => 'required',
            'mothers_occupation' => 'required',
            'mothers_email_id' => 'required',
            'mothers_educations' => 'required',
            'mothers_contact_no' => 'required',
            'state' => 'required',
            'city' => 'required',
            'district' => 'required',
            'permanent_address' => 'required',
            'recedentials_address' => 'required',
        ]);


        $document_file_1 = "";
        if ($request->file('document_file_1') != "") {
            $file = $request->file('document_file_1');
            $document_file_1 = 'doc1-' . md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('public/uploads/student/document/', $document_file_1);
            $document_file_1 =  'public/uploads/student/document/' . $document_file_1;
        }

        $document_file_2 = "";
        if ($request->file('document_file_2') != "") {
            $file = $request->file('document_file_2');
            $document_file_2 = 'doc2-' . md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('public/uploads/student/document/', $document_file_2);
            $document_file_2 =  'public/uploads/student/document/' . $document_file_2;
        }

        $document_file_3 = "";
        if ($request->file('document_file_3') != "") {
            $file = $request->file('document_file_3');
            $document_file_3 = 'doc3-' . md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('public/uploads/student/document/', $document_file_3);
            $document_file_3 =  'public/uploads/student/document/' . $document_file_3;
        }

        $document_file_4 = "";
        if ($request->file('document_file_4') != "") {
            $file = $request->file('document_file_4');
            $document_file_4 = 'doc4-' . md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('public/uploads/student/document/', $document_file_4);
            $document_file_4 =  'public/uploads/student/document/' . $document_file_4;
        }

        $document_file_5 = "";
        if ($request->file('document_file_5') != "") {
            $file = $request->file('document_file_5');
            $document_file_5 = 'doc5-' . md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('public/uploads/student/document/', $document_file_5);
            $document_file_5 =  'public/uploads/student/document/' . $document_file_5;
        }



        $shcool_details = SmGeneralSettings::find(1);
        $school_name = explode(' ', $shcool_details->school_name);
        $short_form = '';
        foreach ($school_name as $value) {
            $ch = str_split($value);
            $short_form = $short_form . '' . $ch[0];
        }

        DB::beginTransaction();
        try {

            //Student Register
            $user_stu = new User();
            $user_stu->role_id = 2;
            $user_stu->full_name =!empty($request->full_name)?$request->full_name:'';;

            if ($request->student_email != null) {
                $user_stu->username = $request->student_email;
            } else {
                $user_stu->username = $short_form . '-' . $request->admission_number;
            }


            if (empty($request->student_email)) {
                $user_stu->email = $short_form . '-' . $request->admission_number;
            } else {
                $user_stu->email = $request->student_email;
            }

            $user_stu->password = Hash::make(123456);
            $user_stu->save();

            $user_stu->toArray();


            try {
                //Parent Register
                $user_parent = new User();
                $user_parent->role_id = 3;
                $user_parent->full_name = $request->fathers_name;
                $user_parent->username = $request->fathers_email_id;
                $user_parent->email = $request->fathers_email_id;
                $user_parent->password = Hash::make(123456);
                $user_parent->save();
                $user_parent->toArray();
                try {
                    try {

                        $parent = new SmParent();
                        $parent->user_id                = $user_parent->id;
                        $parent->fathers_name           = !empty($request->fathers_name)?$request->fathers_name:'';
                        $parent->fathers_mobile         =!empty($request->fathers_contact_no)?$request->fathers_contact_no:'';
                        $parent->fathers_occupation     =!empty($request->fathers_occupation)?$request->fathers_occupation:'';
                        $parent->fathers_photo          ='';
                        $parent->mothers_name           =!empty($request->mothers_name)?$request->mothers_name:'';
                        $parent->mothers_mobile         =!empty($request->mothers_contact_no)?$request->mothers_contact_no:'';
                        $parent->mothers_occupation     =!empty($request->mothers_occupation)?$request->mothers_occupation:'';
                        $parent->mothers_photo          ='';
                        $parent->guardians_name         =!empty($request->fathers_name)?$request->fathers_name:'';
                        $parent->guardians_mobile       =!empty($request->fathers_contact_no)?$request->fathers_contact_no:'';
                        $parent->guardians_email        =!empty($request->fathers_email_id)?$request->fathers_email_id:'';
                        $parent->guardians_occupation   =!empty($request->fathers_occupation)?$request->fathers_occupation:'';
                        $parent->guardians_relation     = '';
                        $parent->relation               = '';
                        $parent->guardians_photo        = Session::get('guardians_photo');
                        $parent->guardians_address      =!empty($request->fathers_office_address)?$request->fathers_office_address:'';
                        $parent->is_guardian            =1;
                        $parent->save();
                        $parent->toArray();


                        $studentAdmissionForm = new SmStudent();
                        $studentAdmissionForm->class_id = $request->admited_to_class;
                        $studentAdmissionForm->user_id = $user_stu->id;
                        $studentAdmissionForm->parent_id = $parent->id;

                        $photo = '';
                        if(@$request->webcam_photo){
                            $data = str_replace('data:image/jpeg;base64,', '', $request->webcam_photo);

                            $path = public_path('uploads/student/');
                            $data = base64_decode($data);

                            $fileName = 'stu-'. time() . uniqid() . '.jpeg';
                            $filePath = $path . $fileName;
                            file_put_contents($filePath, $data);
                            $photo = 'public/uploads/student/'.$fileName;
                        }



                        $studentAdmissionForm->role_id = 2;

                        $studentAdmissionForm->admission_no             =!empty($request->admission_number)?$request->admission_number:'';
                        $studentAdmissionForm->session_id               = !empty($request->session)?$request->session:'';
                        $studentAdmissionForm->full_name                = !empty($request->full_name)?$request->full_name:'';
                        $studentAdmissionForm->first_name                = !empty($request->full_name)?$request->full_name:'';
                        $studentAdmissionForm->section_id               = !empty($request->section)?$request->section:'';
                        $studentAdmissionForm->roll_no                  = !empty($request->roll_no)?$request->roll_no:'';
                        $studentAdmissionForm->admission_date           = date('Y-m-d', strtotime($request->admission_date));
                        $studentAdmissionForm->general_register_id      =$short_form . '-' . $request->admission_number;    //Auto Genrated
                        $studentAdmissionForm->date_of_birth            = date('Y-m-d', strtotime($request->date_of_birth));

                        $studentAdmissionForm->age                          = !empty($request->age)?$request->age:'';
                        $studentAdmissionForm->date_of_place                = !empty($request->date_of_place)?$request->date_of_place:'';
                        $studentAdmissionForm->no_of_sibling                = !empty($request->no_of_sibling)?$request->no_of_sibling:'';
                        $studentAdmissionForm->nationality                  = !empty($request->nationality)?$request->nationality:'';
                        $studentAdmissionForm->caste                        = !empty($request->caste)?$request->caste:'';
                        $studentAdmissionForm->sub_caste                    = !empty($request->sub_caste)?$request->sub_caste:'';
                        $studentAdmissionForm->last_school_attend           = !empty($request->last_school_attend)?$request->last_school_attend:'';
                        $studentAdmissionForm->last_class_attend            = !empty($request->last_class_attend)?$request->last_class_attend:'';
                        //  $studentAdmissionForm->student_national_id_no = !empty($request->student_national_id_no)?$request->student_national_id_no:'';
                        $studentAdmissionForm->student_rfid_no              = !empty($request->student_rfid_no)?$request->student_rfid_no:'';
                        $studentAdmissionForm->student_uid_no               = !empty($request->student_uid_no)?$request->student_uid_no:'';
                        $studentAdmissionForm->email                        = !empty($request->student_email)?$request->student_email:'';
                        $studentAdmissionForm->height                       = !empty($request->student_height)?$request->student_height:'';
                        $studentAdmissionForm->weight                       = !empty($request->student_weight)?$request->student_weight:'';
                        $studentAdmissionForm->gender_id                    = !empty($request->gender_id)?$request->gender_id:'';

                        if(@$photo){
                            $studentAdmissionForm->student_photo = $photo;
                        }
                        else {
                            $studentAdmissionForm->student_photo = Session::get('student_photo');
                        }

                        //Parent Information Save
                        $studentAdmissionForm->fathers_name                 = !empty($request->fathers_name)?$request->fathers_name:'';
                        $studentAdmissionForm->fathers_occupation           = !empty($request->fathers_occupation)?$request->fathers_occupation:'';
                        $studentAdmissionForm->fathers_email_id             = !empty($request->fathers_email_id)?$request->fathers_email_id:'';
                        $studentAdmissionForm->fathers_educations           = !empty($request->fathers_educations)?$request->fathers_educations:'';
                        $studentAdmissionForm->fathers_anual_income         = !empty($request->fathers_anual_income)?$request->fathers_anual_income:'';
                        $studentAdmissionForm->fathers_office_no            = !empty($request->fathers_office_no)?$request->fathers_office_no:'';
                        $studentAdmissionForm->fathers_office_address       = !empty($request->fathers_office_address)?$request->fathers_office_address:'';
                        $studentAdmissionForm->fathers_contact_no           = !empty($request->fathers_contact_no)?$request->fathers_contact_no:'';


                        $studentAdmissionForm->mothers_name                 = !empty($request->mothers_name)?$request->mothers_name:'';
                        $studentAdmissionForm->mothers_occupation           = !empty($request->mothers_occupation)?$request->mothers_occupation:'';
                        $studentAdmissionForm->mothers_email_id             = !empty($request->mothers_email_id)?$request->mothers_email_id:'';
                        $studentAdmissionForm->mothers_educations           = !empty($request->mothers_educations)?$request->mothers_educations:'';
                        $studentAdmissionForm->mothers_anual_income         = !empty($request->mothers_anual_income)?$request->mothers_anual_income:'';
                        $studentAdmissionForm->mothers_office_no            = !empty($request->mothers_office_no)?$request->mothers_office_no:'';
                        $studentAdmissionForm->mothers_office_address       = !empty($request->mothers_office_address)?$request->mothers_office_address:'';
                        $studentAdmissionForm->mothers_contact_no           = !empty($request->mothers_contact_no)?$request->mothers_contact_no:'';


                        $studentAdmissionForm->recedentials_address         = !empty($request->recedentials_address)?$request->recedentials_address:'';
                        $studentAdmissionForm->permanent_address            = !empty($request->permanent_address)?$request->permanent_address:'';
                        $studentAdmissionForm->city                         = !empty($request->city)?$request->city:'';
                        $studentAdmissionForm->state                        = !empty($request->state)?$request->state:'';
                        $studentAdmissionForm->district                     = !empty($request->district)?$request->district:'';

                        $studentAdmissionForm->trnasport                    = !empty($request->trnasport)?$request->trnasport:'';
                        $studentAdmissionForm->route                        = !empty($request->route)?$request->route:'';
                        $studentAdmissionForm->bus_no                       = !empty($request->route)?$request->bus_no:'';

                        $studentAdmissionForm->student_details_handicapped  = !empty($request->student_details_handicapped)?$request->student_details_handicapped:'';
                        $studentAdmissionForm->student_medical_issue        = !empty($request->student_medical_issue)?$request->student_medical_issue:'';
                        $studentAdmissionForm->student_medical_issue        = !empty($request->student_medical_issue)?$request->student_medical_issue:'';


                        $studentAdmissionForm->document_title_1             =!empty($request->document_title_1)?$request->document_title_1:'';
                        $studentAdmissionForm->document_file_1              =  !empty($document_file_1)?$document_file_1:'';
                        $studentAdmissionForm->document_title_2             =!empty($request->document_title_2)?$request->document_title_2:'';
                        $studentAdmissionForm->document_file_2              = !empty($document_file_2)?$document_file_2:'';
                        $studentAdmissionForm->document_title_3             =!empty($request->document_title_3)?$request->document_title_3:'';
                        $studentAdmissionForm->document_file_3              = !empty($document_file_3)?$document_file_3:'';
                        $studentAdmissionForm->document_title_4             =!empty($request->document_title_4)?$request->document_title_4:'';
                        $studentAdmissionForm->document_file_4              =!empty($document_file_4)?$document_file_4:'';



                        $studentAdmissionForm->student_group                =!empty($request->student_group)?$request->student_group:'';
                        $studentAdmissionForm->religion_id                  =!empty($request->religion_id)?$request->religion_id:'';
                        $studentAdmissionForm->bloodgroup_id                 =!empty($request->bloodgroup_id)?$request->bloodgroup_id:'';
                        $studentAdmissionForm->fees_package_id              =!empty($request->fees_package_id)?$request->fees_package_id:'';

                        $studentAdmissionForm->save();
                        $studentAdmissionForm->toArray();
                        DB::commit();

                        if ($studentAdmissionForm->fees_package_id){

                            $fees_masters = SmFeesMaster::where('id', $request->fees_package_id)
                                ->where('school_id', Auth::user()->school_id)
                                ->first();

                            $old_fees_amounts=[];

                            $assign_fees = new SmFeesAssign();
                            $assign_fees->student_id = $studentAdmissionForm->id;

                            if (@$old_fees_amounts[$fees_masters->id]) {
                                $assign_fees->fees_amount = @$old_fees_amounts[$fees_masters->id];
                            } else {
                                $assign_fees->fees_amount = $fees_masters->amount;
                            }

                            $assign_fees->fees_master_id = $fees_masters->id;
                            $assign_fees->school_id = Auth::user()->school_id;
                            $assign_fees->academic_id = YearCheck::getAcademicId();
                            $assign_fees->save();

                            //Send Notificaitons
                            $student_info=SmStudent::find($studentAdmissionForm->id);
                            $notification = new SmNotification;
                            $notification->user_id = $student_info->user_id;
                            $notification->role_id = 2;
                            $notification->date = date('Y-m-d');
                            $notification->message = 'New fees Assigned';
                            $notification->school_id = Auth::user()->school_id;
                            $notification->academic_id = YearCheck::getAcademicId();
                            $notification->save();

                            // $parent=SmParent::find($student_info->parent_id);

                            // $notification = new SmNotification;
                            // $notification->user_id = $parent->user_id;
                            // $notification->role_id = 3;
                            // $notification->date = date('Y-m-d');
                            // $notification->message = 'New fees assigned for your child';
                            // $notification->school_id = Auth::user()->school_id;
                            // $notification->academic_id = YearCheck::getAcademicId();
                            // $notification->save();
                            //End
                        }
                        Toastr::success('Operation successful', 'Success');
                        return redirect('student-admission-form-list');
                    } catch (\Exception $e) {
                        DB::rollback();
                        Toastr::error('Operation Failed', 'Failed');
                        return redirect()->back();
                    }
                } catch (\Exception $e) {
                    DB::rollback();
                    Toastr::error('Operation Failed', 'Failed');
                    return redirect()->back();
                }
            } catch (\Exception $e) {
                DB::rollback();
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            DB::rollback();
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admissionform  $admissionform
     * @return \Illuminate\Http\Response
     */
    public function studentAdmissionFormDetails(Request $request)
    {
        $studentsAdmissionForm = SmStudent::where('active_status', 1)->get();
        $classes = SmClass::where('active_status', 1)->get();

        return view('backEnd.Admissions.student_admision_details', compact('studentsAdmissionForm', 'classes'));
    }


    public function studentAdmissionFormView(Request $request, $id)
    {
        try {
            $student_detail = SmStudent::find($id);

            // return $vehicle;
            $fees_assigneds = SmFeesAssign::where('student_id', $id)->where('academic_id', YearCheck::getAcademicId())->where('school_id', Auth::user()->school_id)->get();
            //  return $fees_assigneds;
            $fees_discounts = SmFeesAssignDiscount::where('student_id', $id)->where('academic_id', YearCheck::getAcademicId())->where('school_id', Auth::user()->school_id)->get();
            // $documents = SmStudentDocument::where('student_staff_id', $id)->where('type', 'stu')->where('academic_id', YearCheck::getAcademicId())->where('school_id',Auth::user()->school_id)->get();
            $documents = SmStudentDocument::where('student_staff_id', $id)->where('academic_id', YearCheck::getAcademicId())->where('school_id', Auth::user()->school_id)->get();
            $timelines = SmStudentTimeline::where('staff_student_id', $id)->where('type', 'stu')->where('academic_id', YearCheck::getAcademicId())->where('school_id', Auth::user()->school_id)->get();
            $academic_year = SmAcademicYear::where('id', $student_detail->session_id)->first();

            return view('backEnd.Admissions.student_admission_form_view', compact('student_detail',  'fees_assigneds', 'fees_discounts', 'documents', 'timelines', 'academic_year'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }


    public function studentAdmissionFormEdit(Request $request, $id)
    {

        try {
            $student = SmStudent::find($id);
            $feesList = SmFeesMaster::where('active_status', '=', '1')->get();

            $classes = SmClass::where('active_status', '=', '1')->where('academic_id', YearCheck::getAcademicId())->where('school_id', Auth::user()->school_id)->get();
            $religions = SmBaseSetup::where('active_status', '=', '1')->where('base_group_id', '=', '2')->get();
            $blood_groups = SmBaseSetup::where('active_status', '=', '1')->where('base_group_id', '=', '3')->get();
            $genders = SmBaseSetup::where('active_status', '=', '1')->where('base_group_id', '=', '1')->get();
            $route_lists = SmRoute::where('active_status', '=', '1')->where('school_id', Auth::user()->school_id)->get();
            $vehicles = SmVehicle::where('active_status', '=', '1')->where('school_id', Auth::user()->school_id)->get();
            $sessions = SmAcademicYear::where('active_status', '=', '1')->where('school_id', Auth::user()->school_id)->get();
            $siblings = SmStudent::where('parent_id', $student->parent_id)->where('school_id', Auth::user()->school_id)->get();

            return view('backEnd.Admissions.student_admission_form_edit', compact('student', 'classes', 'religions', 'blood_groups', 'genders', 'route_lists', 'vehicles', 'sessions', 'siblings','feesList'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }


    public function studentAdmissionFormUpdate(Request $request)
    {
        $student_detail = SmStudent::find($request->id);

        $request->validate([
            'session' => 'required',
            'full_name' => 'required',
            'admited_to_class' => 'required',
            'section' => 'required',
            'roll_no' => 'required',
            'admission_date' => 'required',
            'fees_package_id' => 'required',
            'date_of_birth' => 'required',
            'age' => 'required',
            'date_of_place' => 'required',
            'gender_id' => 'required',
            'no_of_sibling' => 'required',
            'bloodgroup_id' => 'required',
            'nationality' => 'required',
            'religion_id' => 'required',
            'caste' => 'required',
            'sub_caste' => 'required',
            'student_group' => 'required',
            'fathers_name' => 'required',
            'fathers_occupation' => 'required',
            'fathers_email_id' => "required",
            'fathers_educations' => 'required',
            'fathers_contact_no' => 'required',
            'mothers_name' => 'required',
            'mothers_occupation' => 'required',
            'mothers_email_id' => 'required',
            'mothers_educations' => 'required',
            'mothers_contact_no' => 'required',
            'state' => 'required',
            'city' => 'required',
            'district' => 'required',
            'permanent_address' => 'required',
            'recedentials_address' => 'required',
        ]);

        // always happen start

        $document_file_1 = "";
        if ($request->file('document_file_1') != "") {
            if ($student_detail->document_file_1 != "") {
                if (file_exists($student_detail->document_file_1)) {
                    unlink($student_detail->document_file_1);
                }
            }
            $file = $request->file('document_file_1');
            $document_file_1 = 'doc1-' . md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('public/uploads/student/document/', $document_file_1);
            $document_file_1 =  'public/uploads/student/document/' . $document_file_1;
        }

        $document_file_2 = "";
        if ($request->file('document_file_2') != "") {
            if ($student_detail->document_file_2 != "") {
                if (file_exists($student_detail->document_file_2)) {
                    unlink($student_detail->document_file_2);
                }
            }
            $file = $request->file('document_file_2');
            $document_file_2 = 'doc2-' . md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('public/uploads/student/document/', $document_file_2);
            $document_file_2 =  'public/uploads/student/document/' . $document_file_2;
        }

        $document_file_3 = "";
        if ($request->file('document_file_3') != "") {
            if ($student_detail->document_file_3 != "") {
                if (file_exists($student_detail->document_file_3)) {
                    unlink($student_detail->document_file_3);
                }
            }
            $file = $request->file('document_file_3');
            $document_file_3 = 'doc3-' . md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('public/uploads/student/document/', $document_file_3);
            $document_file_3 =  'public/uploads/student/document/' . $document_file_3;
        }

        $document_file_4 = "";
        if ($request->file('document_file_4') != "") {
            if ($student_detail->document_file_4 != "") {
                if (file_exists($student_detail->document_file_4)) {
                    unlink($student_detail->document_file_4);
                }
            }
            $file = $request->file('document_file_4');
            $document_file_4 = 'doc4-' . md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('public/uploads/student/document/', $document_file_4);
            $document_file_4 =  'public/uploads/student/document/' . $document_file_4;
        }


        $shcool_details = SmGeneralSettings::find(1);
        $school_name = explode(' ', $shcool_details->school_name);
        $short_form = '';

        foreach ($school_name as $value) {
            $ch = str_split($value);
            $short_form = $short_form . '' . $ch[0];
        }

        DB::beginTransaction();

        try {

            //User Informations Save IF new User not found on that table
            $user_stu = User::find($student_detail->user_id);

            if (empty($user_stu)) {
                //Student Register
                $user_stu = new User();
                $user_stu->role_id = 2;
                $user_stu->full_name =!empty($request->full_name)?$request->full_name:'';;

                if ($request->student_email != null) {
                    $user_stu->username = $request->student_email;
                } else {
                    $user_stu->username = $short_form . '-' . $request->admission_number;
                }


                if (empty($request->student_email)) {
                    $user_stu->email = $short_form . '-' . $request->admission_number;
                } else {
                    $user_stu->email = $request->student_email;
                }

                $user_stu->password = Hash::make(123456);
                $user_stu->save();

                $user_stu->toArray();
            }

            if (empty($student_detail->parent_id)){
                //Parent Register
                $user_parent = new User();
                $user_parent->role_id = 3;
                $user_parent->full_name = $request->fathers_name;
                $user_parent->username = $request->fathers_email_id;
                $user_parent->email = $request->fathers_email_id;
                $user_parent->password = Hash::make(123456);
                $user_parent->save();
                $user_parent->toArray();


                $parent = new SmParent();
                $parent->user_id                = $user_parent->id;
                $parent->fathers_name           = !empty($request->fathers_name)?$request->fathers_name:'';
                $parent->fathers_mobile         =!empty($request->fathers_contact_no)?$request->fathers_contact_no:'';
                $parent->fathers_occupation     =!empty($request->fathers_occupation)?$request->fathers_occupation:'';
                $parent->fathers_photo          ='';
                $parent->mothers_name           =!empty($request->mothers_name)?$request->mothers_name:'';
                $parent->mothers_mobile         =!empty($request->mothers_contact_no)?$request->mothers_contact_no:'';
                $parent->mothers_occupation     =!empty($request->mothers_occupation)?$request->mothers_occupation:'';
                $parent->mothers_photo          ='';
                $parent->guardians_name         =!empty($request->fathers_name)?$request->fathers_name:'';
                $parent->guardians_mobile       =!empty($request->fathers_contact_no)?$request->fathers_contact_no:'';
                $parent->guardians_email        =!empty($request->fathers_email_id)?$request->fathers_email_id:'';
                $parent->guardians_occupation   =!empty($request->fathers_occupation)?$request->fathers_occupation:'';
                $parent->guardians_relation     = '';
                $parent->relation               = '';
                $parent->guardians_photo        = Session::get('guardians_photo');
                $parent->guardians_address      =!empty($request->fathers_office_address)?$request->fathers_office_address:'';
                $parent->is_guardian            =1;
                $parent->save();
                $parent->toArray();
            }


            $studentAdmissionForm = SmStudent::find($request->id);
            $studentAdmissionForm->class_id = $request->admited_to_class;

            if (empty($student_detail->parent_id)){
                $studentAdmissionForm->parent_id = $parent->id;
            }

            if (empty($user_stu)) {
                $studentAdmissionForm->user_id = $user_stu->id;
            }

            $photo = '';
            if(@$request->webcam_photo){
                $data = str_replace('data:image/jpeg;base64,', '', $request->webcam_photo);

                $path = public_path('uploads/student/');
                $data = base64_decode($data);

                $fileName = 'stu-'. time() . uniqid() . '.jpeg';
                $filePath = $path . $fileName;
                file_put_contents($filePath, $data);
                $photo = 'public/uploads/student/'.$fileName;
            }

            $studentAdmissionForm->role_id = 2;

            $studentAdmissionForm->admission_no             =!empty($request->admission_number)?$request->admission_number:'';
            $studentAdmissionForm->session_id                  = !empty($request->session)?$request->session:'';
            $studentAdmissionForm->full_name                = !empty($request->full_name)?$request->full_name:'';
            $studentAdmissionForm->first_name                = !empty($request->full_name)?$request->full_name:'';

            $studentAdmissionForm->section_id               = !empty($request->section)?$request->section:'';
            $studentAdmissionForm->roll_no                  = !empty($request->roll_no)?$request->roll_no:'';
            $studentAdmissionForm->admission_date           = date('Y-m-d', strtotime($request->admission_date));
            $studentAdmissionForm->general_register_id      =$short_form . '-' . $request->admission_number;    //Auto Genrated
            $studentAdmissionForm->date_of_birth            = date('Y-m-d', strtotime($request->date_of_birth));

            $studentAdmissionForm->age                          = !empty($request->age)?$request->age:'';
            $studentAdmissionForm->date_of_place                = !empty($request->date_of_place)?$request->date_of_place:'';
            $studentAdmissionForm->no_of_sibling                = !empty($request->no_of_sibling)?$request->no_of_sibling:'';
            $studentAdmissionForm->nationality                  = !empty($request->nationality)?$request->nationality:'';
            $studentAdmissionForm->caste                        = !empty($request->caste)?$request->caste:'';
            $studentAdmissionForm->sub_caste                    = !empty($request->sub_caste)?$request->sub_caste:'';
            $studentAdmissionForm->last_school_attend           = !empty($request->last_school_attend)?$request->last_school_attend:'';
            $studentAdmissionForm->last_class_attend            = !empty($request->last_class_attend)?$request->last_class_attend:'';
            //  $studentAdmissionForm->student_national_id_no = !empty($request->student_national_id_no)?$request->student_national_id_no:'';
            $studentAdmissionForm->student_rfid_no              = !empty($request->student_rfid_no)?$request->student_rfid_no:'';
            $studentAdmissionForm->student_uid_no               = !empty($request->student_uid_no)?$request->student_uid_no:'';
            $studentAdmissionForm->email                        = !empty($request->student_email)?$request->student_email:'';
            $studentAdmissionForm->height                       = !empty($request->student_height)?$request->student_height:'';
            $studentAdmissionForm->weight                       = !empty($request->student_weight)?$request->student_weight:'';
            $studentAdmissionForm->gender_id                    = !empty($request->gender_id)?$request->gender_id:'';

            if(@$photo){
                $studentAdmissionForm->student_photo = $photo;
            }
            else {
                $studentAdmissionForm->student_photo = Session::get('student_photo');
            }

            //Parent Information Save
            $studentAdmissionForm->fathers_name                 = !empty($request->fathers_name)?$request->fathers_name:'';
            $studentAdmissionForm->fathers_occupation           = !empty($request->fathers_occupation)?$request->fathers_occupation:'';
            $studentAdmissionForm->fathers_email_id             = !empty($request->fathers_email_id)?$request->fathers_email_id:'';
            $studentAdmissionForm->fathers_educations           = !empty($request->fathers_educations)?$request->fathers_educations:'';
            $studentAdmissionForm->fathers_anual_income         = !empty($request->fathers_anual_income)?$request->fathers_anual_income:'';
            $studentAdmissionForm->fathers_office_no            = !empty($request->fathers_office_no)?$request->fathers_office_no:'';
            $studentAdmissionForm->fathers_office_address       = !empty($request->fathers_office_address)?$request->fathers_office_address:'';
            $studentAdmissionForm->fathers_contact_no           = !empty($request->fathers_contact_no)?$request->fathers_contact_no:'';


            $studentAdmissionForm->mothers_name                 = !empty($request->mothers_name)?$request->mothers_name:'';
            $studentAdmissionForm->mothers_occupation            = !empty($request->mothers_occupation)?$request->mothers_occupation:'';
            $studentAdmissionForm->mothers_email_id             = !empty($request->mothers_email_id)?$request->mothers_email_id:'';
            $studentAdmissionForm->mothers_educations           = !empty($request->mothers_educations)?$request->mothers_educations:'';
            $studentAdmissionForm->mothers_anual_income         = !empty($request->mothers_anual_income)?$request->mothers_anual_income:'';
            $studentAdmissionForm->mothers_office_no            = !empty($request->mothers_office_no)?$request->mothers_office_no:'';
            $studentAdmissionForm->mothers_office_address       = !empty($request->mothers_office_address)?$request->mothers_office_address:'';
            $studentAdmissionForm->mothers_contact_no           = !empty($request->mothers_contact_no)?$request->mothers_contact_no:'';


            $studentAdmissionForm->recedentials_address         = !empty($request->recedentials_address)?$request->recedentials_address:'';
            $studentAdmissionForm->permanent_address            = !empty($request->permanent_address)?$request->permanent_address:'';
            $studentAdmissionForm->city                         = !empty($request->city)?$request->city:'';
            $studentAdmissionForm->state                        = !empty($request->state)?$request->state:'';
            $studentAdmissionForm->district                     = !empty($request->district)?$request->district:'';

            $studentAdmissionForm->trnasport                    = !empty($request->trnasport)?$request->trnasport:'';
            $studentAdmissionForm->route                        = !empty($request->route)?$request->route:'';
            $studentAdmissionForm->bus_no                       = !empty($request->route)?$request->bus_no:'';

            $studentAdmissionForm->student_details_handicapped  = !empty($request->student_details_handicapped)?$request->student_details_handicapped:'';
            $studentAdmissionForm->student_medical_issue        = !empty($request->student_medical_issue)?$request->student_medical_issue:'';
            $studentAdmissionForm->student_medical_issue        = !empty($request->student_medical_issue)?$request->student_medical_issue:'';


            $studentAdmissionForm->document_title_1             =!empty($request->document_title_1)?$request->document_title_1:'';
            $studentAdmissionForm->document_title_2             =!empty($request->document_title_2)?$request->document_title_2:'';
            $studentAdmissionForm->document_title_3             =!empty($request->document_title_3)?$request->document_title_3:'';
            $studentAdmissionForm->document_title_4             =!empty($request->document_title_4)?$request->document_title_4:'';

            if ($document_file_1 != "") {
                $studentAdmissionForm->document_file_1 =  $document_file_1;
            }
            if ($document_file_2 != "") {
                $studentAdmissionForm->document_file_2 =  $document_file_2;
            }

            if ($document_file_3 != "") {
                $studentAdmissionForm->document_file_3 = $document_file_3;
            }

            if ($document_file_4 != "") {
                $studentAdmissionForm->document_file_4 = $document_file_4;
            }

            $studentAdmissionForm->student_group                =!empty($request->student_group)?$request->student_group:'';
            $studentAdmissionForm->religion_id                  =!empty($request->religion_id)?$request->religion_id:'';
            $studentAdmissionForm->bloodgroup_id                 =!empty($request->bloodgroup_id)?$request->bloodgroup_id:'';
            $studentAdmissionForm->fees_package_id              =!empty($request->fees_package_id)?$request->fees_package_id:'';

            if ($request->fees_package_id){

                $fees_masters = SmFeesMaster::where('id', $request->fees_package_id)
                    ->where('school_id', Auth::user()->school_id)
                    ->first();

                $old_fees_amounts=[];
                $assign_fees = SmFeesAssign::where('student_id', $studentAdmissionForm->id)->get();

                if (!empty($assign_fees)){
                    foreach ($assign_fees as $item) {
                        $old_fees_amounts[$fees_masters->id] = $item->fees_amount;
                        DB::table('sm_fees_assigns')->delete($item->id);
                        DB::commit();
                    }
                }


                $assign_fees = new SmFeesAssign();
                $assign_fees->student_id = $studentAdmissionForm->id;
                if (@$old_fees_amounts[$fees_masters->id]) {
                    $assign_fees->fees_amount = @$old_fees_amounts[$fees_masters->id];
                } else {
                    $assign_fees->fees_amount = $fees_masters->amount;
                }

                $assign_fees->fees_master_id = $fees_masters->id;
                $assign_fees->school_id = Auth::user()->school_id;
                $assign_fees->academic_id = YearCheck::getAcademicId();
                $assign_fees->save();


                //Send Notificaitons
                $student_info=SmStudent::find($studentAdmissionForm->id);
                $notification = new SmNotification;
                $notification->user_id = $student_info->user_id;
                $notification->role_id = 2;
                $notification->date = date('Y-m-d');
                $notification->message = 'New fees Assigned';
                $notification->school_id = Auth::user()->school_id;
                $notification->academic_id = YearCheck::getAcademicId();
                $notification->save();

                // $parent=SmParent::find($student_info->parent_id);

                // $notification = new SmNotification;
                // $notification->user_id = $parent->user_id;
                // $notification->role_id = 3;
                // $notification->date = date('Y-m-d');
                // $notification->message = 'New fees assigned for your child';
                // $notification->school_id = Auth::user()->school_id;
                // $notification->academic_id = YearCheck::getAcademicId();
                // $notification->save();
                //End
            }

            $studentAdmissionForm->save();

            DB::commit();
            Toastr::success('Operation successful', 'Success');
            return redirect('student-admission-form-list');
        } catch (\Exception $e) {
            DB::rollback();
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }


    public function studentAdmissionFormDelete(Request $request)
    {

        $student_detail = SmStudent::find($request->modalDeletedId);

        DB::beginTransaction();

        try {

            $student_detail->active_status = 0;
            $student_detail->save();


            $student_user = User::find($student_detail->user_id);
            if ($student_user) {
                $student_user->active_status = 0;
                $student_user->save();
            }

            DB::commit();
            Toastr::success('Operation successful', 'Success');

            return redirect('student-admission-form-list');

        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SmStudent  $admissionform
     * @return \Illuminate\Http\Response
     */
    public function destroy(SmStudent $admissionform)
    {
        //
    }
}
