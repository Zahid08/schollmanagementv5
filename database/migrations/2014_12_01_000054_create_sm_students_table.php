<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_students', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admission_no')->nullable();
            $table->integer('roll_no')->nullable();
            $table->string('full_name', 200)->nullable();
            $table->string('divison', 200)->nullable();
            $table->date('admission_date')->nullable();
            $table->string('general_register_id', 250)->nullable();


            $table->date('date_of_birth')->nullable();
            $table->string('age', 200)->nullable();
            $table->string('date_of_place', 250)->nullable();

            $table->integer('no_of_sibling')->nullable();
            $table->string('nationality')->nullable(); //extra

            $table->string('caste', 200)->nullable();
            $table->string('sub_caste', 200)->nullable();


            $table->string('last_school_attend', 200)->nullable(); //new
            $table->string('last_class_attend', 200)->nullable(); //new
            $table->string('student_national_id_no', 200)->nullable();

            $table->string('student_rfid_no', 200)->nullable(); //RFID
            $table->string('student_uid_no', 200)->nullable(); //RFID

            $table->string('email', 200)->nullable();
            $table->string('height', 200)->nullable();
            $table->string('weight', 200)->nullable();
            $table->string('mobile', 200)->nullable();
            $table->string('student_photo')->nullable();


            $table->string('current_address', 500)->nullable(); //Optional Field

            //Parent Informations
            $table->string('fathers_name', 200)->nullable();
            $table->string('fathers_occupation', 200)->nullable();
            $table->string('fathers_email_id', 200)->nullable(); //pend
            $table->string('fathers_educations', 200)->nullable(); //pend
            $table->string('fathers_anual_income', 200)->nullable();
            $table->string('fathers_office_no', 200)->nullable();
            $table->string('fathers_office_address', 500)->nullable();
            $table->string('fathers_contact_no', 200)->nullable();


            $table->string('mothers_name', 200)->nullable();
            $table->string('mothers_occupation', 200)->nullable();
            $table->string('mothers_email_id', 200)->nullable();
            $table->string('mothers_educations', 200)->nullable();
            $table->string('mothers_anual_income', 200)->nullable();
            $table->string('mothers_office_no', 200)->nullable();
            $table->string('mothers_office_address', 500)->nullable();
            $table->string('mothers_contact_no', 200)->nullable();


            $table->string('route', 200)->nullable();
            $table->string('bus_no', 200)->nullable();

            $table->string('recedentials_address', 500)->nullable();
            $table->string('permanent_address', 500)->nullable();

            $table->string('city', 500)->nullable();
            $table->string('state', 500)->nullable();
            $table->string('district', 500)->nullable();

            $table->string('trnasport', 500)->nullable();

            //Parent Informations End
            $table->string('student_group', 200)->nullable();

            $table->string('student_details_handicapped', 500)->nullable();
            $table->string('student_medical_issue', 500)->nullable();

            $table->string('driver_id', 200)->nullable();
            $table->string('national_id_no', 200)->nullable();
            $table->string('local_id_no', 200)->nullable();
            $table->string('bank_account_no', 200)->nullable();
            $table->string('bank_name', 200)->nullable();
            $table->string('previous_school_details', 500)->nullable();
            $table->string('aditional_notes', 500)->nullable();

            $table->string('document_title_1', 200)->nullable();
            $table->string('document_title_5', 200)->nullable();
            $table->string('document_file_5', 200)->nullable();
            $table->string('document_file_1', 200)->nullable();
            $table->string('document_title_2', 200)->nullable();
            $table->string('document_file_2', 200)->nullable();
            $table->string('document_title_3', 200)->nullable();
            $table->string('document_file_3', 200)->nullable();
            $table->string('document_title_4', 200)->nullable();
            $table->string('document_file_4', 200)->nullable();
            $table->tinyInteger('active_status')->default(1);

            $table->timestamps();

            $table->integer('bloodgroup_id')->nullable();
            $table->foreign('bloodgroup_id')->references('id')->on('sm_base_setups')->onDelete('cascade');

            $table->integer('religion_id')->nullable();
            $table->foreign('religion_id')->references('id')->on('sm_base_setups')->onDelete('cascade');

            $table->integer('route_list_id')->nullable();
            $table->foreign('route_list_id')->references('id')->on('sm_routes')->onDelete('cascade');

            $table->integer('dormitory_id')->nullable();
            $table->foreign('dormitory_id')->references('id')->on('sm_dormitory_lists')->onDelete('cascade');

            $table->integer('vechile_id')->nullable()->unsigned();
            $table->foreign('vechile_id')->references('id')->on('sm_vehicles')->onDelete('cascade');

            $table->integer('room_id')->nullable()->unsigned();
            $table->foreign('room_id')->references('id')->on('sm_room_lists')->onDelete('cascade');

            $table->integer('student_category_id')->nullable();
            $table->foreign('student_category_id')->references('id')->on('sm_student_categories')->onDelete('cascade');

            $table->integer('class_id')->unsigned();
            $table->foreign('class_id')->references('id')->on('sm_classes')->onDelete('cascade');

            $table->integer('section_id')->unsigned();
            $table->foreign('section_id')->references('id')->on('sm_sections')->onDelete('cascade');

            $table->integer('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('sm_academic_years')->onDelete('cascade');

            $table->integer('parent_id')->nullable()->unsigned();
            $table->foreign('parent_id')->references('id')->on('sm_parents')->onDelete('cascade');

            $table->integer('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('infix_roles')->onDelete('cascade');

            $table->integer('gender_id')->nullable()->unsigned();
            $table->foreign('gender_id')->references('id')->on('sm_base_setups')->onDelete('cascade');


            $table->integer('created_by')->nullable()->default(1)->unsigned();

            $table->integer('updated_by')->nullable()->default(1)->unsigned();

            $table->integer('school_id')->nullable()->default(1)->unsigned();
            $table->foreign('school_id')->references('id')->on('sm_schools')->onDelete('cascade');
            
            $table->integer('academic_id')->nullable()->default(1)->unsigned();
            $table->foreign('academic_id')->references('id')->on('sm_academic_years')->onDelete('cascade');

            $table->integer('fees_package_id')->nullable(); //Id need to store
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_students');
    }
}
