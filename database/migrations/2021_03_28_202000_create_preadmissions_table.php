<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreadmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preadmissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admission_number')->nullable(); //Form No
            $table->date('date')->nullable(); //Normal Date
            $table->integer('session')->nullable(); //Academic Year
            $table->string('student_name', 200)->nullable(); //Student Full name
            $table->string('fathers_name', 200)->nullable();
            $table->string('mothers_name', 200)->nullable();
            $table->string('surname', 200)->nullable(); //

            $table->string('contact_no_1', 200)->nullable();
            $table->string('contact_no_2', 200)->nullable();

            $table->string('student_email', 200)->nullable();
            $table->date('date_of_birth')->nullable(); //DOB
            $table->string('age', 200)->nullable();

            $table->integer('admited_to_class')->nullable(); //Id need to store
            $table->foreign('admited_to_class')->references('id')->on('sm_classes')->onDelete('cascade');

            $table->integer('gender_id')->nullable();
            $table->foreign('gender_id')->references('id')->on('sm_base_setups')->onDelete('cascade');

            $table->integer('no_of_sibling')->nullable();
            $table->string('permanent_address', 500)->nullable();
            $table->string('pin_code', 500)->nullable();
            $table->string('fathers_occupation', 200)->nullable();
            $table->string('mothers_occupation', 200)->nullable();
            $table->string('reference', 500)->nullable();
            $table->string('source', 500)->nullable();

            $table->string('previous_school_details', 500)->nullable();
            $table->string('descriptions', 500)->nullable();

            $table->string('full_name', 200)->nullable(); //Student Full name

            $table->tinyInteger('active_status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preadmissions');
    }
}
