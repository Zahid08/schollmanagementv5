<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdmissionformsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admissionforms', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('role_id')->nullable();
            $table->integer('admission_number')->nullable();
            $table->integer('session')->nullable(); //intiger Beacuse ID need to store
            $table->string('full_name', 200)->nullable();
            $table->string('divison', 200)->nullable();
            $table->integer('roll_no')->nullable();
            $table->date('admission_date')->nullable();

            $table->string('general_register_id', 200)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('age', 200)->nullable();
            $table->string('date_of_place', 250)->nullable();
            $table->integer('no_of_sibling')->nullable();
            $table->string('nationality')->nullable(); //extra
            $table->string('caste', 200)->nullable();
            $table->string('sub_caste', 200)->nullable(); //new
            $table->string('last_school_attend', 200)->nullable(); //new
            $table->string('last_class_attend', 200)->nullable(); //new
            $table->string('student_national_id_no', 200)->nullable();
            $table->string('student_rfid_no', 200)->nullable(); //RFID
            $table->string('student_uid_no', 200)->nullable(); //RFID
            $table->string('student_email', 200)->nullable();
            $table->string('student_height', 200)->nullable();
            $table->string('student_weight', 200)->nullable();
            $table->string('student_photo', 800)->nullable();

            //Parent Informations
            $table->string('fathers_name', 200)->nullable();
            $table->string('fathers_occupation', 200)->nullable();
            $table->string('fathers_email_id', 200)->nullable(); //pend
            $table->string('fathers_educations', 200)->nullable(); //pend
            $table->string('fathers_anual_income', 200)->nullable();
            $table->string('fathers_office_no', 200)->nullable();
            $table->string('fathers_office_address', 500)->nullable();
            $table->string('fathers_contact_no', 200)->nullable();


            $table->string('mothers_name', 200)->nullable();
            $table->string('mothers_occupation', 200)->nullable();
            $table->string('mothers_email_id', 200)->nullable();
            $table->string('mothers_educations', 200)->nullable();
            $table->string('mothers_anual_income', 200)->nullable();
            $table->string('mothers_office_no', 200)->nullable();
            $table->string('mothers_office_address', 500)->nullable();
            $table->string('mothers_contact_no', 200)->nullable();

            $table->string('route', 200)->nullable();
            $table->string('bus_no', 200)->nullable();


            $table->string('recedentials_address', 500)->nullable();
            $table->string('permanent_address', 500)->nullable();

            $table->string('city', 500)->nullable();
            $table->string('state', 500)->nullable();
            $table->string('district', 500)->nullable();

            $table->string('trnasport', 500)->nullable();

            //Parent Informations End


            $table->string('student_details_handicapped', 500)->nullable();
            $table->string('student_medical_issue', 500)->nullable();


            $table->string('document_file_1', 200)->nullable();
            $table->string('document_title_1', 200)->nullable();

            $table->string('document_title_2', 200)->nullable();
            $table->string('document_file_2', 200)->nullable();

            $table->string('document_title_3', 200)->nullable();
            $table->string('document_file_3', 200)->nullable();
            $table->string('document_title_4', 200)->nullable();
            $table->string('document_file_4', 200)->nullable();


            //$table->integer('af_student_group_id')->nullable();

            $table->string('student_group', 200)->nullable();

           // $table->foreign('af_religion_id')->references('id')->on('sm_base_setups')->onDelete('cascade');

            $table->integer('religion_id')->nullable();
            $table->foreign('religion_id')->references('id')->on('sm_base_setups')->onDelete('cascade');

            $table->integer('bloodgroup_id')->nullable();
            $table->foreign('bloodgroup_id')->references('id')->on('sm_base_setups')->onDelete('cascade');

            $table->integer('gender_id')->nullable();
            $table->foreign('gender_id')->references('id')->on('sm_base_setups')->onDelete('cascade');

            $table->integer('admited_to_class')->nullable(); //Id need to store
            $table->foreign('admited_to_class')->references('id')->on('sm_classes')->onDelete('cascade');

            $table->integer('fees_package_id')->nullable(); //Id need to store
            //$table->foreign('fees_package_id')->references('id')->on('sm_classes')->onDelete('cascade');

            $table->tinyInteger('active_status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admissionforms');
    }
}
